//
//  UIImageExtentions.swift
//  VoiceWords
//
//  Created by Georg Romas on 20/03/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

extension UIImage {
    /// Returns UIImage with 1x1 pixel size with required color
    class func imagePixelFromColor(color: UIColor) -> UIImage {
        let rect = CGRectMake(0.0, 0.0, 1.0, 1.0)
        return imageFromColorForRect(color, rect: rect)
    }
    
    /// Returns UIImage with required color and size
    class func imageFromColorForRect(color: UIColor, rect: CGRect) -> UIImage {
        UIGraphicsBeginImageContext(rect.size);
        let context = UIGraphicsGetCurrentContext();
        
        CGContextSetFillColorWithColor(context, color.CGColor);
        CGContextFillRect(context, rect);
        
        let image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return image;
    }
}