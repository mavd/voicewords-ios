//
//  UILabelExtentions.swift
//  VoiceWords
//
//  Created by Georg Romas on 13/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

extension UILabel {
    
    /**
     
     Text typing animation. Text appear characters one by one next each other with required interval
     
     - Parameter typedText: `String` text to animate
     - Parameter characterInterval: interval in seconds, f.e.: 0.4. **Default 0.1**
     
     */
    func textTypeAnimated(typedText: String, characterInterval: NSTimeInterval = 0.10) {
        text = ""
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0)) {
            for character in typedText.characters {
                dispatch_async(dispatch_get_main_queue()) {
                    self.text = self.text! + String(character)
                }
                NSThread.sleepForTimeInterval(characterInterval)
            }
        }
    }
    
    /**
     
     Increment/Decrement number animation. Number grow up or go down with required interval and steps
     
     - Parameter from: `Int` start value, f.e.: 0
     - Parameter to: `Int` end value, f.e.: 100
     - Parameter steps: `Int` steps number, f.e.: 10. **Default 5**
     - Parameter format: `String` format as for String(format:) function, f.e.: %d. **Default %05d**
     - Parameter interval: `Double` interval in seconds, f.e.: 0.4. **Default 0.05**
     
     */
    func changeNumberAnimated(from: Int, to: Int, steps: Int = 5, format: String = "%05d", interval: NSTimeInterval = 0.05) {
        var current = from
        
        let step = (to-from)/steps
        
        func isComplete() -> Bool {
            if (from > to) { return current > to }
            if (from < to) { return current < to }
            return false
        }
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0)) {
            while isComplete() {
                current += step
                if (!isComplete() || current == 0 || step == 0) {current = to}
                ui{ self.text = String(format: format, current) }
                NSThread.sleepForTimeInterval(interval)
            }
            if (current != to) {current = to}
            ui{ self.text = String(format: format, current) }
        }
        
    }
    
}