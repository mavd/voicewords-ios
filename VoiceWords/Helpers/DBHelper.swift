//
//  DBHelper.swift
//  VoiceWords
//
//  Created by Georg Romas on 15/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation
import GRDB


/// Database access helper based on GRDB library
class DBHelper {
    
    static let version = "4.3"
    
    private class func dbPath() -> String {
        let cachesPath = NSSearchPathForDirectoriesInDomains(.CachesDirectory, .UserDomainMask, true).first!
        //TODO: for debugging
        //print("cachesPath: \(cachesPath)")
        
        return "\(cachesPath)/db_\(version).sqlite"
    }
    
    private  class func dbBundlePath() -> String? {
        return NSBundle.mainBundle().pathForResource("db", ofType: "sqlite")
    }
    
    /// Application ships with local words databse. 
    /// On first launch it should be copied into caches directory if not exist.
    class func copyBundeledDbToCachesIfNotExist() {
        let fm = NSFileManager.defaultManager()
        do {
            if let bundleDBPath = DBHelper.dbBundlePath()
            {
                let dbPath = DBHelper.dbPath()
                if !fm.fileExistsAtPath(dbPath) {
                    try fm.copyItemAtPath(bundleDBPath, toPath: dbPath)
                }
            }
        } catch let e {
            print("ERROR: can not copy database from bundle to the Library/Caches \(e)")
        }
    }
    
    /// Returns Database Queue from GRDB library
    class func db() throws -> DatabaseQueue {
        return try DatabaseQueue(path: dbPath())
    }
    
    /// Update database with words loaded from remote server
    class func updateWordsFromText(words: String) {
        //print(words)
        
        let set = NSCharacterSet.whitespaceCharacterSet()
        let mset = NSMutableCharacterSet(charactersInString: "\r\n \r\n \r \n \t")
        mset.formUnionWithCharacterSet(set)
        
        let wordsArray = words.characters.split("\r")
        if wordsArray.count < 5 {
            return
        }
        
        do  {
            try DBHelper.db().inTransaction { db in
                print("DBHelper::updateWordsFromText: - db.transaction start")
                
                try db.execute("UPDATE words SET is_old = 1")
                
                try wordsArray.forEach { str in
                    NSCharacterSet.controlCharacterSet()
                    let preparedWord = String(str)
                        .stringByTrimmingCharactersInSet(mset)
                        .lowercaseString.stringByReplacingOccurrencesOfString("ё", withString: "е", options: [NSStringCompareOptions.CaseInsensitiveSearch], range: nil)
                    try db.execute("UPDATE words SET is_old = NULL WHERE word = ?", arguments: [preparedWord])
                    
                    if db.changesCount == 0 {
                        try db.execute("INSERT INTO words (word) VALUES (?)", arguments: [preparedWord])
                    }
                }
                
                try db.execute("DELETE FROM words WHERE is_old = 1")
                
                print("DBHelper::updateWordsFromText- db.transaction end")
                return .Commit
            }
            
        } catch let e {
            print("DBHelper::updateWordsFromText:ERROR: can not read database text file: \(e)")
        }
    }
}