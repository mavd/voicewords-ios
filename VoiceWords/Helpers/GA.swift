//
//  GA.swift
//  VoiceWords
//
//  Created by Georg Romas on 10/03/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//
// Google Analytics helper

import Foundation
import Crashlytics
import AppsFlyer

/// Events traking helper class
/// Aggregate Crashlytics and Answers
/// Shortcuts for events and screens
class GA {
    //MARK: Screens
    
    class func screenWelcome() {
        setScreen("Welcome")
    }
    
    class func screenMenu() {
        setScreen("Menu")
    }
    
    class func screenTutorial() {
        setScreen("Tutorial")
    }
    
    class func screenLevelLoading() {
        setScreen("LevelLoading")
    }
    
    class func screenGame() {
        setScreen("Game")
    }
    
    class func screenLevels() {
        setScreen("Levels")
    }
    
    class func screenLevelComplete() {
        setScreen("LevelComplete")
    }
    
    class func screenGameComplete() {
        setScreen("GameComplete")
    }
    
    class func screenMarket() {
        setScreen("Market")
    }
    
    class func screenAbout() {
        setScreen("About")
    }
    
    class func screenSettings() {
        setScreen("Settings")
    }
    
    private class func setScreen(name: String) {
        Answers.logCustomEventWithName("Open Screen", customAttributes: ["Screen" : name])
        AppsFlyerTracker.sharedTracker().trackEvent("Open Screen", withValue: name)
    }
    
    //MARK: Events
    
    class func gaEvUseChit(type: String, word: String) {
        eventCategoryGame("used_help_word_\(type)", label: word, value: 1)
    }
    
    class func gaEvUseChit(type: String, time: Float) {
        eventCategoryGame("used_help_time_\(type)", label: "\(time)", value: 1)
    }
    
    class func gaEvMakePurchase(purchaseName: String) {
        eventCategoryMarket("make_purchase", label: purchaseName, value: 1)
    }
    
    class func gaEvLevelFailed(level: Int, wordsSolved: Int, time: Float) {
        eventCategoryMarket("level_failed", label: "\(level)_\(wordsSolved)_\(time)", value: 1)
    }
    
    class func gaEvLevelSucceeded(level: Int, time: Float) {
        eventCategoryMarket("level_succeeded", label: "\(level)_\(time)", value: 1)
    }
    
    class func gaEvLevelReplay(level: Int) {
        eventCategoryMarket("level_complete_replay", label: "\(level)", value: 1)
    }
    
    class func gaEvPlayeNextLevel(level: Int) {
        eventCategoryMarket("level_complete_play_next", label: "\(level)", value: 1)
    }
    
    class func gaEvGoHomeAfterLevel(level: Int) {
        eventCategoryMarket("level_complete_go_home", label: "\(level)", value: 1)
    }
    
    class func gaEvAddForReward() {
        eventCategoryMarket("add", label: "for_reward", value: 1)
    }
    
    class func gaEvReachableWifi() {
        eventCategoryInfo("connection_success", label: "wifi", value: 1)
    }
    
    class func gaEvReachableGSM() {
        eventCategoryInfo("connection_success", label: "gsm", value: 1)
    }
    
    class func gaEvReachableNoInternet() {
        eventCategoryInfo("connection_error", label: "no_internet", value: 1)
    }
    
    class func gaWorngWord(word: String) {
        Answers.logCustomEventWithName("worng_word", customAttributes: ["word":word])
        AppsFlyerTracker.sharedTracker().trackEvent("worng_word", withValue: word)
    }
    
    //MARK: Events Info
    class func eventCategoryInfo(action: String, label: String, value: NSNumber) {
        sendEvent("Info", action: action, label: label, value: value)
    }
    
    //MARK: Events Market
    class func eventCategoryMarket(action: String, label: String, value: NSNumber) {
        sendEvent("Market", action: action, label: label, value: value)
    }
    
    //MARK: Events Game
    class func eventCategoryGame(action: String, label: String, value: NSNumber) {
        sendEvent("Game", action: action, label: label, value: value)
    }
    
    private class func sendEvent(categoty: String, action: String, label: String, value: NSNumber) {
        let dict = ["Category" : categoty, "Label" : label, "Value" : value]
        Answers.logCustomEventWithName(action, customAttributes: dict)
        AppsFlyerTracker.sharedTracker().trackEvent(action, withValues: dict)
    }
}