//
//  UIColorExtentions.swift
//  VoiceWords
//
//  Created by Georg Romas on 12/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    /**
     Retuns `UIColor` based on hex representaion
     
     - Parameter hex: `String` color hex representation. f.e.: "#ffffff"
     - Returns `UIColor`
     */
    class func colorWithHexString (hex:String) -> UIColor {
        var cString:String = hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).uppercaseString
        
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substringFromIndex(1)
        }
        
        if (cString.characters.count != 6) {
            return UIColor.grayColor()
        }
        
        let rString = (cString as NSString).substringToIndex(2)
        let gString = ((cString as NSString).substringFromIndex(2) as NSString).substringToIndex(2)
        let bString = ((cString as NSString).substringFromIndex(4) as NSString).substringToIndex(2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        NSScanner(string: rString).scanHexInt(&r)
        NSScanner(string: gString).scanHexInt(&g)
        NSScanner(string: bString).scanHexInt(&b)
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
    
    // Color shortcat for #FECDA6
    class func progressFill() -> UIColor {
        return UIColor.colorWithHexString("#FECDA6")
    }
    
    // Color shortcat for #0D6874
    class func nextLevelTimerStrocke() -> UIColor {
        return UIColor.colorWithHexString("#0D6874")
    }
    
    // Color shortcat for #FD5D08
    class func levelCellHightlightStrocke() -> UIColor {
        return UIColor.colorWithHexString("#FD5D08")
    }
    
    // Color shortcat for #e96f1c
    class func levelCellActiveBorder() -> UIColor {
        return UIColor.colorWithHexString("#e96f1c")
    }
    
    // Color shortcat for #f4c7a0
    class func levelCellDisabledBorder() -> UIColor {
        return UIColor.colorWithHexString("#f4c7a0")
    }
    
    // Color shortcat for #E7F1F7
    class func timerDecorativeCircleStroke() -> UIColor {
        return UIColor.colorWithHexString("#E7F1F7")
    }
    
    // Color shortcat for red: 51/255.0, green: 61/255.0, blue: 63/255.0, alpha: 1.0
    class func timerStroke() -> UIColor {
        return UIColor(red: 51/255.0, green: 61/255.0, blue: 63/255.0, alpha: 1.0)
    }
    
    // Color shortcat for red: 233/255.0, green: 63/255.0, blue: 80/255.0, alpha: 1.0
    class func timerWarningStroke() -> UIColor {
        return UIColor(red: 233/255.0, green: 63/255.0, blue: 80/255.0, alpha: 1.0)
    }
    
    // Color shortcat for #D6E3EC
    class func viceIdicator() -> UIColor {
        return UIColor.colorWithHexString("#D6E3EC")
    }
    
    // Color shortcat for #EAF9FF
    class func deckBackground() -> UIColor {
        return UIColor.colorWithHexString("#EAF9FF")
    }
    
    // Color shortcat for red: 51/255.0, green: 61/255.0, blue: 63/255.0, alpha: 1.0
    class func charcoal() -> UIColor {
        return UIColor(red: 51/255.0, green: 61/255.0, blue: 63/255.0, alpha: 1.0)
    }
}