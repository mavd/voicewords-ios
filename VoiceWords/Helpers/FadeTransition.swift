//
//  FadeTransition.swift
//  VoiceWords
//
//  Created by Georg Romas on 10/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

/**
 Custom fade transition controller for UIViewControllers
 */
class FadeTransition: NSObject, UIViewControllerAnimatedTransitioning {

    var isDississs = false
    
    init(isDississs: Bool) {
        super.init()
        
        self.isDississs = isDississs
    }
    
    /// This is used for percent driven interactive transitions, as well as for container controllers that have companion animations that might need to
    /// synchronize with the main animation.
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.4
    }
    
    /// This method can only  be a nop if the transition is interactive and not a percentDriven interactive transition.
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        let fromVK = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)
        let toVK = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)
        
        transitionContext.containerView()!.addSubview(toVK!.view)
        transitionContext.containerView()!.addSubview(fromVK!.view)
        
        if isDississs {
            toVK!.view.alpha = 1
            fromVK!.view.alpha = 0
            transitionContext.completeTransition(true)
        } else {
            UIView.animateWithDuration(0.4, animations: {
                fromVK!.view.alpha = 0
                }) { complete in
                    transitionContext.completeTransition(true)
            }
        }
    }
    
    /// This is a convenience and if implemented will be invoked by the system when the transition context's completeTransition: method is invoked.
    func animationEnded(transitionCompleted: Bool) {
        //
    }
    
}
