//
//  GameCenterHelper.swift
//  VoiceWords
//
//  Created by Georg Romas on 21/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation
import GameKit
import XCGLogger

class GameCenterHelper {
    
    /// Save new score into Game Center general leaderboard 
    class func reportScore(score: Int) {
        let gkScore = GKScore(leaderboardIdentifier: Constants.LeaderboardIDGeneral, player: GKLocalPlayer.localPlayer())
        gkScore.value = Int64(score)
        
        GKScore.reportScores([gkScore]) { error in
            if let error = error {
                log.error("GC: report score error: \(error)")
            } else {
                log.info("GC: report score without error")
            }
        }
    }
    
}