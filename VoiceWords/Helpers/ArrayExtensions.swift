//
//  ArrayExtensions.swift
//  SpeechDemo
//
//  Created by Georg Romas on 06/02/16.
//  Copyright © 2016 Georg. All rights reserved.
//

import Foundation

extension Array {
    /// Returns random element
    func randomItem() -> Element {
        let index = Int(arc4random_uniform(UInt32(self.count)))
        return self[index]
    }
}