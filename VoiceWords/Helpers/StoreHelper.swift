//
//  StoreHelper.swift
//  VoiceWords
//
//  Created by Georg Romas on 16/05/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation
import StoreKit

/**
 Helper to preload purchases information
 
 **Note:** Clients shoud use single instance
 */
class StoreHelper: NSObject, SKProductsRequestDelegate {
    
    /// Required accessor to single object
    static let sharedInstance = StoreHelper()
    
    private var request:SKProductsRequest!
   
    /**
     Do not use this cunstructor. Use `sharedInstance` var
     */
    override init() {
        super.init()
        
        request = SKProductsRequest(productIdentifiers: [
            Constants.PurchasePack1,
            Constants.PurchasePack2,
            Constants.PurchasePack3,
            Constants.PurchasePack4,
            Constants.PurchasePack5
        ])
    }
    
    private var requestCompletion: ((products: [SKProduct])->Void)?
    
    /**
     Load purchases information with callback
     
     - Parameter complete: response callback with access to SKProduct array
     */
    func preLoadPurchasesInfo(complete: ((products: [SKProduct])->Void)? = nil) {
        requestCompletion = complete
        request.delegate = self
        
        request.start()
    }
    
    // MARK: SKProductsRequestDelegate
    func productsRequest(request: SKProductsRequest, didReceiveResponse response: SKProductsResponse) {
        requestCompletion?(products: response.products)
    }
}