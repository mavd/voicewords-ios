//
//  SoundsBoss.swift
//  VoiceWords
//
//  Created by Georg Romas on 13/05/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation

/// Global sound manager
let soundsBoss = SoundsBoss()

/**
 Sound manager helper
 
 **Note:** Should should be initialized only one instance. Use **soundsBoss** global variable.
 */
class SoundsBoss {
    
    private static var instancesCount = 0
    
    private var backMusicTheme:AudioPlayer!
    private var brainIndicatorProgress: AudioPlayer!
    private var gameCompleteTheme: AudioPlayer!
    private var purchaseSuccessEffect: AudioPlayer!
    private var startRoundEffect: AudioPlayer!
    
    // Level complete screen
    private var newStatusEffect:AudioPlayer!
    private var scoreGrowProgressEffect: AudioPlayer!
    private var looseEffect: AudioPlayer!
    
    // Game
    private var roundEndEffect: AudioPlayer!
    private var errorEffect: AudioPlayer!
    
    private var playerAnswerEffect: AudioPlayer!
    
    // This is trick to play different robot sounds. 
    // Sound is very long and have robot speech variations.
    // Game toggle play/pause for this infinity sound.
    private var robotPreparedLoop: AudioPlayer!
    
    // Input controller
    private var chitTimeEffect: AudioPlayer!
    private var chitChangeEffect: AudioPlayer!
    private var chitSolveEffect: AudioPlayer!
    
    /// Required initializer
    required init() {
        if SoundsBoss.instancesCount > 1 {
            assert(false, "FATAL: more then one instance of SoundsBoss created. Use soundsBoss global variable.")
        }
        SoundsBoss.instancesCount += 1
        
        
        backMusicTheme = try! AudioPlayer(fileName: "1a_main_theme.mp3")
        backMusicTheme.volume = 0.1
        backMusicTheme.volume = 0.1
        
        brainIndicatorProgress = try! AudioPlayer(fileName: "progress.wav")
        brainIndicatorProgress.volume = 0.6
        
        gameCompleteTheme = try! AudioPlayer(fileName: "end_of_the_game.wav")
        gameCompleteTheme.numberOfLoops = -1
        
        roundEndEffect = try! AudioPlayer(fileName: "end_round.wav")
        errorEffect = try! AudioPlayer(fileName: "word_error.wav")
        
        playerAnswerEffect = try! AudioPlayer(fileName: "send.wav")
        playerAnswerEffect.volume = 0.2
        robotPreparedLoop = try! AudioPlayer(fileName: "robot_talking.wav")
        robotPreparedLoop.numberOfLoops = -1
        robotPreparedLoop.volume = 0.2
        
        chitTimeEffect = try! AudioPlayer(fileName: "chit_clock.wav")
        chitChangeEffect = try! AudioPlayer(fileName: "chit_change.wav")
        chitSolveEffect = try! AudioPlayer(fileName: "chit_solve.wav")
        
        purchaseSuccessEffect = try! AudioPlayer(fileName: "purchase_coins.wav")
        
        startRoundEffect = try! AudioPlayer(fileName: "begin.wav")
        startRoundEffect.volume = 0.2
        
        newStatusEffect = try! AudioPlayer(fileName: "new_status.wav")
        scoreGrowProgressEffect = try! AudioPlayer(fileName: "final_score.wav")
        looseEffect = try! AudioPlayer(fileName: "round_loose.wav")
    }
    
    /// Indicate whether main theme is played or not
    func isMainThemePlayed() -> Bool {
        return backMusicTheme.playing
    }
    
    /// Play main background music with fade from 0.0 to 0.8 with duration 1 sec.
    func playLoopMainTheme() {
        backMusicTheme.play()
        backMusicTheme.fadeTo(0.8, duration: 1.0)
    }
    
    /// Stop main background music with fade
    func stopMainTheme() {
        backMusicTheme.fadeOut()
    }
    
    /// Play once brain indicator progress effect
    func playOnceBrainProgress() {
        brainIndicatorProgress.play()
    }
    
    func playLoopGameCompleteTheme() {
        gameCompleteTheme.play()
    }
    
    /// Stop main background music with fade
    func stopGameCompleteTheme() {
        gameCompleteTheme.fadeOut()
    }
    
    // MARK: Game sounds
    
    /// Play error sound effect once
    func playOnceErrorEffect() {
        errorEffect.play()
    }
    
    /// Play round end sound once
    func playOnceRoundEndEffect() {
        roundEndEffect.play()
    }
    
    /// Play sound player answer once
    func playEffectPlayerAnwer() {
        playerAnswerEffect.play()
    }
    
    /// Stop sound player answer
    func stopPlayerAnwer() {
        playerAnswerEffect.stop()
    }
    
    /// Indicate whether robot voice is played or not
    func isRobotPreparedPlayed() -> Bool {
        return robotPreparedLoop.playing
    }
    
    /// Play sound robot thinking infinit
    func playLoopRobotPrepared() {
        robotPreparedLoop.play()
    }
    
    /// Pause sound robot thinking
    func stopRobotPrepared() {
        robotPreparedLoop.stop()
    }
    
    /// Play sound "Add time" once
    func playOnceChitAddTime() {
        chitTimeEffect.play()
    }
    
    /// Play sound "Solve word" once
    func playOnceChitSolveWord() {
        chitSolveEffect.play()
    }
    
    /// Play sound "Change word" once
    func playOnceChitChangeWord() {
        chitChangeEffect.play()
    }
    
    /// Play sound "Purchase success" once
    func playOncePurchaseSuccess() {
        purchaseSuccessEffect.play()
    }
    
    /// Play sound "Round begins" once
    func playOnceStartRound() {
        startRoundEffect.play()
    }
    
    /// Play sound "New status achieved" once
    func playOnceNewStatus() {
        newStatusEffect.play()
    }
    
    /// Play sound "Score grow up" once
    func playOnceScoreGrowProgress() {
        scoreGrowProgressEffect.play()
    }
    
    /// Play sound "Yuo are loose" once
    func playOnceYouAreLoose() {
        looseEffect.play()
    }
}