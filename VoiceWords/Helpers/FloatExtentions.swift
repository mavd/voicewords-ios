//
//  FloatExtentions.swift
//  VoiceWords
//
//  Created by Georg Romas on 23/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation


extension Float {
    
    /// Returns `String` representation of seconds as minutes. 
    ///
    /// **f.e.:** 300.minutesFormat() returns "3:20"
    func minutesFormat() -> String {
        let minutes = self/60.0
        let integer = floor(minutes)
        let fraction = (minutes - integer) * 60
        
        return String(format: "%02d:%02d", arguments: [Int(integer), Int(fraction)])
    }
    
}