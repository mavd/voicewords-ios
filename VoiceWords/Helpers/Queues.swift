//
//  Queues.swift
//  eatseasy
//
//  Created by Georg Romas on 29/08/15.
//  Copyright © 2015 Georg Romas. All rights reserved.
//


import Foundation

/**
 
 **Shortcut for running closures in background thread** through dispatch_get_global_queue
 
 **This is Global function**
 
 Used queue priority is **BACKGROUND**
 
 **Example:**
 
 ```
 future {
    performNetworkRequest()
 }
 ```
 
 - Parameter closure: What should be executed in background thread
 
 - SeeAlso:
   - `delayCall(_:closure:)`
   - `ui(_:)`
 
 */
func future(closure:()->()) {
    let backQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)
    dispatch_async(backQueue, closure)
}

/**
 
 **Shortcut for running closures with delay** through dispatch_after
 
 **This is Global function**
 
 **Note:** Closure will be executed in main thread
 
 - Parameter delay: `Double` value in seconds. for example 1.3
 - Parameter closure: What should be executed after delay
 
 */
func delayCall(delay:Double, closure:()->()) {
    dispatch_after(
        dispatch_time(
            DISPATCH_TIME_NOW,
            Int64(delay * Double(NSEC_PER_SEC))
        ),
        dispatch_get_main_queue(), closure)
}

/**
 
 **Shortcut for running closures in main thread** through dispatch_async
 
 **This is Global function**
 
 **Usage**: Offten required perform task with uikit elements when some background work done
 
 **Example:**
 
 ```
 doSomesingInbackground() { isComplete in
    ui {
        label.text = "Change IU safely"
    }
 }
 ```
 
 - Parameter closure: What should be executed in main thread
 
 */
func ui(closure:()->()){
    dispatch_async(dispatch_get_main_queue(), closure)
}