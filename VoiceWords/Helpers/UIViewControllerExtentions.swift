//
//  UIViewControllerExtentions.swift
//  VoiceWords
//
//  Created by Georg Romas on 12/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    /// Returns main screen bounds height
    private func screenHeight() -> CGFloat {
        return UIScreen.mainScreen().bounds.height
    }
    
    /// Returns main screen bounds width
    private func screenWidth() -> CGFloat {
        return UIScreen.mainScreen().bounds.width
    }
    
    /*
    WARNING: works only for portrait orientation
    for complete solution should check width also
    */
    
    // iPhone 5.5 inch
    /// Is it 5.5 inch screen?
    func is55Inch() -> Bool {
        return screenHeight() == 736
    }
    
    // iPhone 4.7 inch
    /// Is it 4.7 inch screen?
    func is47Inch() -> Bool {
        return screenHeight() == 667
    }
    
    // iPhone 4 inch
    /// Is it 4 inch screen?
    func is4Inch() -> Bool {
        return screenHeight() == 568
    }
    
    // iPhone 3.5 inch
    /// Is it 3.5 inch screen?
    func is35Inch() -> Bool {
        return screenHeight() == 480
    }
    
}