//
//  WatchRequestHandler.swift
//  VoiceWords
//
//  Created by Georg Romas on 05/03/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation

/**
 
 ApplicationDelegate delegate watch request from session to this object
 
 This class accomplish three tasks:
 
   - Answer info about current user status, such as name, score, coins balance and so on
   - Create new game task for watch app throug `WatchGame` object
   - Save result of watch game
 
 */
class WatchRequestHandler {
 
    /**
     
     Process companion application request
     
     - Parameter request: `Dictionary` describe task to perform from apple watch
     - Parameter replyHandler: Callback with `Dictionary` answer according to selected task
     
     */
    class func performRequest(request: [String:AnyObject], replyHandler: ([String : AnyObject]) -> Void) {
        
        if let userRequest = request[WatchRequestConstants.kRequestKey] as? String {
            
            switch userRequest {
                
            case WatchRequestConstants.kRequestUserInfo:
                replyHandler(
                    [
                        WatchRequestConstants.kResponseKeyUserStatus: ScoreStorage.currentStatusName(),
                        WatchRequestConstants.kResponseKeyUserScore: ScoreStorage.userTotalScore(),
                        WatchRequestConstants.kResponseKeyUserCoins : CoinsStorage.userCoins(),
                        WatchRequestConstants.kResponseKeyUserAvatarName : ScoreStorage.watchCurrentStatusImageName(),
                        WatchRequestConstants.kResponseKeyUserGameAvatarName : ScoreStorage.cellAvatarName(),
                        WatchRequestConstants.kResponseKeyCurrentLevel : LevelsCompletionStorage.lastNotCompleteLevel(),
                        WatchRequestConstants.kResponseKeyUserLevel : ScoreStorage.userLevel()
                    ]
                )
                
            case WatchRequestConstants.kRequestGame:
                let game = WatchGame()
                let question = game.selectNotUsedMask()
                
                replyHandler(
                    [
                        WatchRequestConstants.kResponseKeyGameWord: question.0,
                        WatchRequestConstants.kResponseKeyGameVariants: question.1
                    ]
                )
                
            case WatchRequestConstants.kRequestLevelComplete:
                if
                let selectedWord = request[WatchRequestConstants.kRequestKeySelectedWord] as? String,
                let mask = request[WatchRequestConstants.kRequestKeyPlayedMask] as? String,
                let rewardScore = request[WatchRequestConstants.kRequestKeyRewardScore] as? Int,
                let rewardCoins = request[WatchRequestConstants.kRequestKeyRewardCoins] as? Int
                {
                    let watchGame = WatchGame()
                    watchGame.saveUsedMask(selectedWord, mask: mask)
                    
                    ScoreStorage.incrementTotalScore(rewardScore)
                    CoinsStorage.refill(rewardCoins)
                
                    replyHandler(
                        [
                            WatchRequestConstants.kResponseKeyUserStatus: ScoreStorage.currentStatusName(),
                            WatchRequestConstants.kResponseKeyUserScore: ScoreStorage.userTotalScore(),
                            WatchRequestConstants.kResponseKeyUserCoins : CoinsStorage.userCoins(),
                            WatchRequestConstants.kResponseKeyUserAvatarName : ScoreStorage.watchCurrentStatusImageName(),
                            WatchRequestConstants.kResponseKeyUserGameAvatarName : ScoreStorage.cellAvatarName(),
                            WatchRequestConstants.kResponseKeyCurrentLevel : LevelsCompletionStorage.lastNotCompleteLevel(),
                            WatchRequestConstants.kResponseKeyUserLevel : ScoreStorage.userLevel()
                        ]
                    )
                }
                
            default: break
            }
            
        }
        
    }
    
}