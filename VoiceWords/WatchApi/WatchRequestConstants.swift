//
//  WatchRequestConstants.swift
//  VoiceWords
//
//  Created by Georg Romas on 05/03/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation

/**
 
 Apple Watch session transfer *Dictionary* object.
 In that reason this strings constants used mostly for autocompletion and safety
 
 This struct used both targets: main app and watch extention
 
 - Starts with `kRequest` mean incomin request from companion application
 - Starts with `kResponse` mean response key
 
 */
struct WatchRequestConstants {
    
    /// Main dictioanry key which used to indicate companion application requests
    static let kRequestKey = "com.voicewords.request";
 
    /// Success response shortcut
    static let ok = [String: AnyObject]()
    
    // MARK: Loading user info
    static let kRequestUserInfo = "com.voicewords.request.user.info";
    
    static let kResponseKeyUserStatus = "com.voicewords.response.user.status"
    static let kResponseKeyUserCoins = "com.voicewords.response.user.coins"
    static let kResponseKeyUserAvatarName = "com.voicewords.response.user.avatarName"
    static let kResponseKeyUserGameAvatarName = "com.voicewords.response.user.gameAvatarName"
    static let kResponseKeyUserScore = "com.voicewords.response.user.score"
    static let kResponseKeyUserLevel = "com.voicewords.response.userLevel"
    static let kResponseKeyCurrentLevel = "com.voicewords.response.currentLevel"
    
    // MARK: Loading game info
    static let kRequestGame = "com.voicewords.request.game.info"
    
    static let kResponseKeyGameWord = "com.voicewords.response.game.word"
    static let kResponseKeyGameVariants = "com.voicewords.response.game.variants"
    
    // MARK: Process level completion result
    static let kRequestLevelComplete = "com.voicewords.request.level.complete"
    
    static let kRequestKeyPlayedMask = "com.voicewords.request.game.mask"
    static let kRequestKeySelectedWord = "com.voicewords.request.game.answer"
    static let kRequestKeyRewardScore = "com.voicewords.request.game.reward.score"
    static let kRequestKeyRewardCoins = "com.voicewords.request.game.reward.coins"
    
}