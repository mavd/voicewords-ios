//
//  ScoreIndicator.swift
//  VoiceWords
//
//  Created by Georg Romas on 19/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

/// Level score indicator view
class ScoreIndicator: UILabel {
    private var score: Int = 0
    
    /// Increment score
    func scoreUP(addPoints: Int) {
        self.changeNumberAnimated(self.score, to: self.score + addPoints, steps: 6, format: "%05d", interval: 0.05)
        self.score += addPoints
    }
    
}
