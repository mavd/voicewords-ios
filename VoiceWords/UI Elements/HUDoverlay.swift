//
//  HUDoverlay.swift
//  VoiceWords
//
//  Created by Georg Romas on 21/03/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit
import SnapKit

class HUDoverlay: UIView {
    var label: UILabel!
    
    class func setupInView(parent: UIView) -> HUDoverlay {
        let overlay = HUDoverlay(frame: parent.frame)
        overlay.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.8)
        
        overlay.label = UILabel()
        
        overlay.addSubview(overlay.label)
        parent.addSubview(overlay)
        
        overlay.snp_makeConstraints { make in
            make.center.equalTo(parent)
            make.size.equalTo(parent)
        }
        
        overlay.label.textAlignment = NSTextAlignment.Center
        overlay.label.text = "Ожидание Game Center"
        overlay.label.font = UIFont.systemFontOfSize(15)
        overlay.label.textColor = UIColor.charcoal()
        overlay.label.numberOfLines = 10
        overlay.label.snp_makeConstraints { make in
            make.center.equalTo(overlay)
            make.width.equalTo(overlay)
        }
        
        overlay.hide()
        
        return overlay
    }
    
    func showWaitGC() {
        self.show("Ожидание Game Center")
    }
    
    func showNoInternet() {
        self.show("Извините!\nДля игры требуется\nинтернет-соединение")
    }
    
    func showSyncDB() {
        self.show("Проверяем обновления")
    }

    func show(message: String) {
        self.label.text = message
        self.hidden = false
    }
    
    func hide() {
        self.hidden = true
    }

}
