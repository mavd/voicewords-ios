//
//  BrainIndicator.swift
//  VoiceWords
//
//  Created by Georg Romas on 12/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

class BrainIndicator: UIView {

    private var label = UILabel()
    private var progress = 0.0
    private let progressView = UIView()
    private let progressFillView = UIView()
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor.clearColor()
        
        let imageView = UIImageView(image: UIImage(named: "img_brain"))
        imageView.bounds = self.bounds
        
        let mask = UIImage(named: "mask_brain")!
        let maskImageView = UIImageView(image: mask)
        
        self.progressView.bounds = self.bounds
        self.progressView.maskView = maskImageView
        
        self.addSubview(progressView)
        progressView.frame.origin = CGPointZero

        self.progressFillView.backgroundColor = UIColor.progressFill()
        
        self.progressView.addSubview(self.progressFillView)
        self.progressFillView.frame = self.progressView.frame
        
        self.addSubview(imageView)
        setProgress(0)
        
        label.bounds = self.bounds
        self.addSubview(label)
        label.frame.origin = CGPoint(x: 5, y: -15)
        label.text = "0"
        label.font = UIFont(name: Constants.Font_OpenSans_ExtraBold, size: 26)
        label.textAlignment = NSTextAlignment.Center
        label.textColor = UIColor.charcoal()
    }
    
    /*
    progress from 0 to 100
    */
    func setProgress(progress: Float) {
        let safeProgress = progress > 100.0 ? 100.0 : progress
        let tragetHeight = self.frame.size.height
        let fillHeight: CGFloat = tragetHeight/100.0 * CGFloat(safeProgress)
        var rect = self.progressFillView.frame
        rect.origin.y = tragetHeight - fillHeight
        rect.size.height = fillHeight
        self.progressFillView.frame = rect
    }
    
    func setProgressAnimated(progress: Float) {
        let speed:Float = 10.0/100.0
        var duration = progress * speed
        if duration < 0.5 { duration = 0.5 }
        
        if Settings.isMusicEnabled() { soundsBoss.playOnceBrainProgress() }
        
        UIView.animateWithDuration(Double(duration), delay: 0, usingSpringWithDamping: 1.2, initialSpringVelocity: 2.0,
            options: UIViewAnimationOptions.CurveEaseIn,
            animations: {
                self.setProgress(progress)
            },
            completion: {complite in})
    }
    
    var lastScore = 0
    var lastLevel = 1
    func updateScore() {
        let score = ScoreStorage.userTotalScore()
        let level = ScoreStorage.userLevel()
        
        if score > self.lastScore {
            self.label.changeNumberAnimated(self.lastScore, to: score, steps: 20, format: "%d")
            self.lastScore = score
        } else {
            self.label.text = "\(score)"
        }
        
        let progress = ScoreStorage.progressPercent()
        if level > lastLevel {
            self.setProgressAnimated(progress)
            self.lastLevel = level
        } else {
            if level == 0 {
                self.setProgress(100.0)
                self.setProgressAnimated(1.0)
            } else {
                self.setProgress(progress)
            }
        }
    }

}
