//
//  GameFeildView.swift
//  VoiceWords
//
//  Created by Georg Romas on 13/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit
import SnapKit

/// Where messages from player and AI appear and move from bottom to top animated
class GameFeildView: UIView {
    
    private var lastMessageY:CGFloat = 0
    private var lastBottomConstraint: Constraint?
    private var lastCell: UIView?
    private var cellViews = [UIView]()
    
    /// Setup view
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    /// Add new cell with player word
    func pushWordFromPlayer(word: String) {
        let cell = PlayerWordCell.loadFromNib(word, avatar: ScoreStorage.cellAvatar())
        pushWord(word, cell: cell)
        cell.playAnimation()
        
        if Settings.isMusicEnabled() { soundsBoss.playEffectPlayerAnwer() }
    }
    
    /// Add new cell with AI word
    func pushWordFromOpponent(word: String) -> WordCell {
        let cell = OpponentWordCell.loadFromNib(word)
        
        //TODO: uncomment on release. this is for screenshots
        if Settings.isMusicEnabled() { soundsBoss.playLoopRobotPrepared() }
        cell.playAIPreparesAnimation()
        pushWord(word, cell: cell)
        
        //TODO: comment on release. this is for screenshots
//        pushWord(word, cell: cell)
//        cell.playAnimation()
        
        return cell
    }
    
    /// Cancel sound playing
    func stopAllSounds() {
        soundsBoss.stopPlayerAnwer()
        soundsBoss.stopRobotPrepared()
    }
    
    private func pushWord(word: String, cell: WordCell) {
        self.addSubview(cell)
        self.cellViews.append(cell)
        
        //remove old views
        if self.cellViews.count > 10 {
            self.cellViews.removeFirst().removeFromSuperview()
        }
        
        if let lastChain = self.lastBottomConstraint {
            lastChain.uninstall()
        }
        
        var boundsCompensation:CGFloat = 68
        if self.lastCell == nil {
            boundsCompensation = 60
        }
        
        var b = self.bounds
        b.origin.y -= boundsCompensation
        self.bounds = b
        
        cell.snp_makeConstraints { [unowned self] make in
            make.height.equalTo(60)
            make.left.equalTo(self.snp_left)
            make.right.equalTo(self.snp_right)
            if let bottomCell = self.lastCell {
                make.top.equalTo(bottomCell.snp_bottom).offset(8).constraint
                self.lastBottomConstraint = make.bottom.equalTo(self.snp_bottom).offset(-10).constraint
            } else {
                self.lastBottomConstraint = make.bottom.equalTo(self.snp_bottom).offset(-10).constraint
            }
            self.lastCell = cell
        }
        
        b = self.bounds
        b.origin.y += boundsCompensation
        
        UIView.animateWithDuration(1.2) {
            self.bounds = b
        }
    }

}
