//
//  PlayNextLevelButton.swift
//  VoiceWords
//
//  Created by Georg Romas on 13/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

class PlayNextLevelButton: UIButton {
    
    var timerEndCallback:(()->Void)?
    
    func showTimerProgress(interval: Float, timerEndCallback:(()->Void)) {
        self.timerEndCallback = timerEndCallback
        
        let timerLayer = CAShapeLayer()
        
        self.layer.insertSublayer(timerLayer, atIndex: 0)
        
        timerLayer.frame = CGRect(x: 0, y: 0, width: self.layer.frame.width, height: self.layer.frame.height)
        timerLayer.strokeColor = UIColor.nextLevelTimerStrocke().CGColor
        timerLayer.lineWidth = 1.5
        timerLayer.fillColor = UIColor.clearColor().CGColor
        let radius = timerLayer.frame.height/2
        timerLayer.path = UIBezierPath(roundedRect: timerLayer.frame, byRoundingCorners: UIRectCorner.AllCorners, cornerRadii: CGSizeMake(radius, radius)).CGPath
        timerLayer.strokeStart = 0.0
        timerLayer.strokeEnd = 0.0
        
        let animation = CABasicAnimation()
        animation.duration = CFTimeInterval(interval)
        animation.keyPath = "strokeEnd"
        animation.fromValue = 0.0
        animation.toValue = 1.0
        animation.delegate = self
        
        timerLayer.addAnimation(animation, forKey: "strocke_fill_animation")
        
    }
    
    override func animationDidStop(anim: CAAnimation, finished flag: Bool) {
        if let basicAnim = anim as? CABasicAnimation {
            if basicAnim.keyPath == "strokeEnd" {
                self.timerEndCallback!()
            }
        }
    }
}
