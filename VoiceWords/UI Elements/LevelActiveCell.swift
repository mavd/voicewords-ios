//
//  LevelActiveCell.swift
//  VoiceWords
//
//  Created by Georg Romas on 13/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

class LevelActiveCell: LevelCell {

    override func borderColor() -> UIColor {
        return UIColor.levelCellActiveBorder()
    }

}
