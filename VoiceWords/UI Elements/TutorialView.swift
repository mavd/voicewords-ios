//
//  TutorialView.swift
//  VoiceWords
//
//  Created by Georg Romas on 03/03/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

class TutorialView: UIView {

    @IBOutlet weak var label: UILabel!
    
    
    @IBOutlet weak var stepView_1: UIView!
    @IBOutlet weak var stepView_2: UIView!
    @IBOutlet weak var stepView_3: UIView!
    @IBOutlet weak var stepView_4: UIView!
    @IBOutlet weak var stepView_5: UIView!
    @IBOutlet weak var stepView_6: UIView!
    @IBOutlet weak var stepView_7: UIView!
    @IBOutlet weak var stepView_8: UIView!
    
    
    @IBOutlet weak var labelStep3: UILabel!
    @IBOutlet weak var labelStep4: UILabel!
    @IBOutlet weak var labelStep6: UILabel!
    
    var tapCallback: (()->Void)?
    var containerTapCallback: (()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.hideAll()
    }
    
    private func fadeIn(view: UIView) {
        view.hidden = false
        view.alpha = 0
        UIView.animateWithDuration(0.3) { view.alpha = 1 }
    }
    
    func hideAll() {
        self.stepView_1.hidden = true
        self.stepView_2.hidden = true
        self.stepView_3.hidden = true
        self.stepView_4.hidden = true
        self.stepView_5.hidden = true
        self.stepView_6.hidden = true
        self.stepView_7.hidden = true
        self.stepView_8.hidden = true
    }
    
    func showStepWelcome() {
        self.hideAll()
        self.fadeIn(self.stepView_1)
    }
    
    func showStepStartLetter() {
        self.hideAll()
        self.fadeIn(self.stepView_2)
    }
    
    func showStepTimer() {
        self.hideAll()
        self.labelStep3.text = "Это шкала времени. Если не успеешь назвать слово, игра закончится."
        self.fadeIn(self.stepView_3)
    }
    
    func showStepMicrophone() {
        self.hideAll()
        self.fadeIn(self.stepView_4)
    }
    
    func showStepTryToSay() {
        self.hideAll()
        self.fadeIn(self.stepView_5)
    }
    
    func showStepGoodBoy() {
        self.hideAll()
        self.labelStep4.text = "У тебя отлично получается! Продолжай в том же духе."
        self.fadeIn(self.stepView_4)
    }
    
    func showStepDonate(amount: Int) {
        self.hideAll()
        self.labelStep4.text = "Мы дарим вам \(amount) монет"
        self.fadeIn(self.stepView_4)
    }
    
    func showStepHelpInfo() {
        self.hideAll()
        self.labelStep4.text = "Если ты испытываешь трудности с новым словом, то можешь использовать подсказки. Давай попробуем."
        self.fadeIn(self.stepView_4)
    }
    
    func showStepHelpInfoTimer() {
        self.hideAll()
        self.labelStep4.text = "Допустим, у тебя\nкончается время.\nПросто скажи:\n\"Добавить время\""
        self.fadeIn(self.stepView_4)
    }
    
    func showStepHelpInfoChangeWord() {
        self.hideAll()
        self.labelStep4.text = "Используй другую подсказку:\n\"Сменить слово\""
        self.fadeIn(self.stepView_4)
    }
    
    func showStepHelpInfoSolveWord() {
        self.hideAll()
        self.labelStep4.text = "Используй подсказку:\n\"Подобрать слово\""
        self.fadeIn(self.stepView_4)
    }
    
    func showStepCoinsInfo() {
        self.hideAll()
        self.labelStep6.text = "Для подсказок нужны монеты,\nпо этому следи за тем, чтобы\nтебе всегда хватало монет в кошельке."
        self.fadeIn(self.stepView_6)
    }
    
    func showStepMarket() {
        self.hideAll()
        self.labelStep6.text = "Пока оппонент придумывает нужное слово, ты можешь докупить нужное количество монет. Посмотри магазин."
        self.fadeIn(self.stepView_6)
    }
    
    func showStepGoodGirl() {
        self.hideAll()
        self.labelStep4.text = "Отлично справляешься!\nМолодец!"
        self.fadeIn(self.stepView_4)
    }
    
    func showStepKeyboard() {
        self.hideAll()
        self.fadeIn(self.stepView_7)
    }
    
    func showStepKeyboardAndHelp() {
        self.hideAll()
        self.fadeIn(self.stepView_8)
    }
    
    func showStepCongrats() {
        self.hideAll()
        self.labelStep4.text = "Поздравляем!\nТы успешно прошел обучение!"
        self.fadeIn(self.stepView_4)
    }
    
    func showStepTimerLow() {
        self.hideAll()
        self.labelStep3.text = "Время вышло!\nНо пока идет тренировка, оно будет автоматически возобновлено."
        self.fadeIn(self.stepView_3)
    }

    
    @IBAction func tapAction(sender: UITapGestureRecognizer) {
        self.tapCallback?()
    }
    
    @IBAction func containerTapAction(sender: UITapGestureRecognizer) {
        self.containerTapCallback?()
    }
}
