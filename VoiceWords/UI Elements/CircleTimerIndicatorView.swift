//
//  CircleTimerIndicatorView.swift
//  VoiceWords
//
//  Created by Georg Romas on 13/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

class CircleTimerIndicatorView: UIView {
    
    private var timerLayer: CAShapeLayer!
    private var circleLayer: CAShapeLayer!
    private var interval:Float = 0.0
    private var isPaused = false
    private var timer: NSTimer?
    var label: UILabel?
    
    var timerEndCallback:(()->Void)?
    
    func createIndicator(strokeEnd _strockeEnd: CGFloat) {
        if self.timerLayer == nil {
            self.timerLayer = CAShapeLayer()
            self.layer.insertSublayer(timerLayer, atIndex: 1)
        }
        
        self.backgroundColor = UIColor.clearColor()
        
        self.timerLayer.frame = CGRect(x: 0, y: 0, width: self.layer.frame.width, height: self.layer.frame.height)
        self.timerLayer.strokeColor = UIColor.timerStroke().CGColor
        self.timerLayer.lineWidth = 1.5
        self.timerLayer.fillColor = UIColor.clearColor().CGColor
        self.timerLayer.path = UIBezierPath(roundedRect: timerLayer.frame, cornerRadius: timerLayer.frame.height/2.0).CGPath
        self.timerLayer.strokeStart = 0.0
        self.timerLayer.strokeEnd = _strockeEnd
        
        if self.label == nil {
            self.label = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
            self.addSubview(self.label!)
            self.label!.font = UIFont(name: Constants.Font_OpenSans_ExtraBold, size: 12)
            self.label!.textColor = UIColor.charcoal()
            self.label!.textAlignment = NSTextAlignment.Center
            self.label!.text = String(format: "%d", 0)
        }
    }
    
    func emptyTimer() {
        self.timerLayer.strokeEnd = 0
        self.label!.changeNumberAnimated(self.lastTimerValue, to: 0, steps: 6, format: "%d")
        self.label!.textColor = UIColor.lightGrayColor()
    }
    
    var lastTimerValue = 0
    func updateProgress(countDown: Float, totalTime: Int, level: Int) {
        let countDown = countDown > 0 ? countDown : 0
        let level = level > 0 ? level : 1
        self.timerLayer.strokeEnd = CGFloat(1.0/Float(totalTime) * countDown)
        self.label!.textColor = UIColor.charcoal()
        
        let score = ScoreConverter.scoreFromCountDown(countDown, totalTime: totalTime, level: level)
        if lastTimerValue != score {
            self.label!.text = String(format: "%d", score)
            self.lastTimerValue = score
        }
        
        if countDown > 6 {
            self.timerLayer.strokeColor = UIColor.timerStroke().CGColor
        } else {
            self.timerLayer.strokeColor = UIColor.timerWarningStroke().CGColor
            self.scaleBouncheAnimation()
        }
    }
    
    var bouncePlayed = false
    func scaleBouncheAnimation() {
        if !self.bouncePlayed {
            self.bouncePlayed = true
            
            let bounce = CABasicAnimation(keyPath: "transform.scale")
            bounce.delegate = self
            bounce.duration = 1.0
            bounce.fromValue = 0.95
            bounce.toValue = 1.0
            bounce.setValue("Bounce", forKey: "Name")
            bounce.timingFunction = CAMediaTimingFunction(controlPoints: 0.5, 1.8, 1.0, 1.0)
            
            self.timerLayer.addAnimation(bounce, forKey: "Bounce")
        }
    }
    
    override func animationDidStop(anim: CAAnimation, finished flag: Bool) {
        if let animation = anim as? CABasicAnimation {
            if let name = animation.valueForKey("Name") as? String {
                if name == "Bounce" {
                    self.bouncePlayed = false
                }
            }
        }
    }

}
