//
//  PurchaseItemCell.swift
//  VoiceWords
//
//  Created by Georg Romas on 11/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

class PurchaseItemCell: UITableViewCell {

    @IBOutlet weak var cellTitleLabel: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellPriceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        cellPriceLabel.layer.borderWidth = 1.0
        cellPriceLabel.layer.borderColor = UIColor.blackColor().CGColor
        cellPriceLabel.layer.cornerRadius = cellPriceLabel.frame.size.height/2
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
