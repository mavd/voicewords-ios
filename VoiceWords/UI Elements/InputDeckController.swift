//
//  InputDeckController.swift
//  VoiceWords
//
//  Created by Georg Romas on 13/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

class InputDeckController: UIStackView {

    @IBOutlet weak var timerIndicator: CircleTimerIndicatorView!
    @IBOutlet weak var timerIndicatorContainer: UIView!
    @IBOutlet weak var topBlockContainer: UIView!
    @IBOutlet weak var voiceIndicator: VoiceIndicatorView!
    @IBOutlet weak var recognizerStateIndicatorLabel: UILabel!
    @IBOutlet weak var modeButton: UIButton!
    @IBOutlet weak var warningLabel: UILabel!
    @IBOutlet weak var inputField: UITextField!
    @IBOutlet weak var wordsCountLabel: UILabel!
    @IBOutlet weak var microphoneImage: UIImageView!
    
    @IBOutlet weak var bottomBlockContainer: UIStackView!
    @IBOutlet weak var csResetTimerLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var csSolveWordLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var csChangeWordLabelHeight: NSLayoutConstraint!
    
    @IBOutlet weak var chitAddTimeLabel: UILabel!
    @IBOutlet weak var chitSolveWordLabel: UILabel!
    @IBOutlet weak var chitChangeWordLabel: UILabel!
    
    @IBOutlet weak var addTimeChitButton: UIButton!
    @IBOutlet weak var solveWordChitButton: UIButton!
    @IBOutlet weak var changeWordChitButton: UIButton!
    
    
    @IBOutlet weak var addTimeChitIcon: UIImageView!
    @IBOutlet weak var solveWordChitIcon: UIImageView!
    @IBOutlet weak var changeWordChitIcon: UIImageView!
    
    var chitAddTimeCallback: (()->Void)?
    var chitSolveWordCallback: (()->Void)?
    var chitChangeWordCallback: (()->Void)?
    
    private var isInKeyboardMode = false
    
    class PathDemoView: UIView {
        let path = UIBezierPath()
        var startPoint = CGPoint(x: 0, y: 0)
        var endPoint = CGPoint(x: 150, y: 200)
        override func drawRect(rect: CGRect) {
            path.lineWidth = 3.0
            path.moveToPoint(startPoint)
            //path.addLineToPoint(endPoint)
            path.addCurveToPoint(endPoint, controlPoint1: CGPoint(x: startPoint.x, y: startPoint.y/2), controlPoint2: CGPoint(x: endPoint.x/2, y: endPoint.y))
            UIColor.blackColor().setStroke()
            path.stroke()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.inputField.font = UIFont(name: Constants.Font_OpenSans_Semibold, size: 18)
        
        self.topBlockContainer.layer.cornerRadius = self.topBlockContainer.frame.height/2.0
        self.hideWarning()
        self.clearText()
        self.hideChitsHints()
    }
    
    func setupPath() {
        let pathView = PathDemoView(frame: self.superview!.frame)
        pathView.backgroundColor = UIColor.clearColor()
        self.superview!.addSubview(pathView)
        
        pathView.startPoint = self.topBlockContainer.convertPoint(CGPoint(x: self.timerIndicator.frame.size.width/2, y: self.topBlockContainer.frame.size.height/2), toView: pathView)
        
        pathView.startPoint = self.topBlockContainer.convertPoint(self.timerIndicatorContainer.center, toView: self.superview!)
        pathView.endPoint = CGPoint(x: self.superview!.frame.size.width/1.8, y: 20)
        pathView.setNeedsDisplay()
        
        let rectView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        rectView.backgroundColor = UIColor.redColor()
        pathView.addSubview(rectView)
        rectView.center = pathView.startPoint
        
        let path = UIBezierPath()
        let startPoint = self.topBlockContainer.convertPoint(self.timerIndicatorContainer.center, toView: self.superview!)
        let endPoint = CGPoint(x: self.superview!.frame.size.width/1.8, y: 20)
        path.moveToPoint(startPoint)
        path.addCurveToPoint(CGPoint(x: self.superview!.frame.size.width/1.8, y: 20), controlPoint1: CGPoint(x: startPoint.x, y: startPoint.y/2), controlPoint2: CGPoint(x: endPoint.x/2, y: endPoint.y))
        
        let pathAnimation = CAKeyframeAnimation(keyPath: "position")
        pathAnimation.calculationMode = kCAAnimationPaced
        pathAnimation.path = path.CGPath
        
        let alphaAnimation = CABasicAnimation(keyPath: "opacity")
        alphaAnimation.fromValue = 1.0
        alphaAnimation.toValue = 0.0
        
        let animations = CAAnimationGroup()
        animations.duration = 1.5
        animations.repeatCount = 3
        animations.animations = [pathAnimation, alphaAnimation]
        
        rectView.layer.addAnimation(animations, forKey: "pathAnimation")
    }
    
    override func animationDidStop(anim: CAAnimation, finished flag: Bool) {
        if let animation = anim as? CAAnimationGroup {
            if let view = animation.valueForKey("parentView") as? UIView {
                view.removeFromSuperview()
            }
        }
    }
    
    func showScoreAnimation(score: Int) {
        let scoreView = UIView(frame: self.timerIndicator.frame)
        scoreView.backgroundColor = UIColor.clearColor()
        self.superview!.addSubview(scoreView)
        scoreView.center = self.topBlockContainer.convertPoint(self.timerIndicatorContainer.center, toView: self.superview!)
        
        let label = UILabel(frame: scoreView.frame)
        label.textAlignment = NSTextAlignment.Center
        label.textColor = UIColor.timerStroke()
        label.font = UIFont.boldSystemFontOfSize(11)
        label.text = "\(score)"
        scoreView.addSubview(label)
        
        label.snp_makeConstraints { make in
            make.center.equalTo(scoreView)
            make.size.equalTo(scoreView)
        }
        
        let path = UIBezierPath()
        let startPoint = self.topBlockContainer.convertPoint(self.timerIndicatorContainer.center, toView: self.superview!)
        let endPoint = CGPoint(x: self.superview!.frame.size.width/1.8, y: 20)
        path.moveToPoint(startPoint)
        path.addCurveToPoint(CGPoint(x: self.superview!.frame.size.width/1.8, y: 20), controlPoint1: CGPoint(x: startPoint.x, y: startPoint.y/2), controlPoint2: CGPoint(x: endPoint.x/2, y: endPoint.y))
        
        let pathAnimation = CAKeyframeAnimation(keyPath: "position")
        pathAnimation.calculationMode = kCAAnimationPaced
        pathAnimation.path = path.CGPath
        
        let alphaAnimation = CABasicAnimation(keyPath: "opacity")
        alphaAnimation.fromValue = 1.0
        alphaAnimation.toValue = 0.0
        
        let animations = CAAnimationGroup()
        animations.duration = 1.5
        animations.repeatCount = 1
        animations.animations = [pathAnimation, alphaAnimation]
        animations.delegate = self
        animations.setValue(scoreView, forKey: "parentView")
        animations.removedOnCompletion = false
        animations.fillMode = kCAFillModeForwards
        
        scoreView.layer.addAnimation(animations, forKey: "pathAnimation")
    }
    
    /*
    MARK: Indicators >
    */
    func stateIdle() {
        self.textIndicatorWait()
    }
    
    func stateListen() {
        self.showMicrophoneIndicator()
        self.hideVoiceIndicator()
        self.textIndicatorSpeeak()
    }
    
    func stateSpeechDetected() {
        self.showVoiceIndicator()
        self.textIndicatorListen()
    }
    
    func stateRecognizing() {
        self.hideVoiceIndicator()
        self.textIndicatorRecognizing()
    }
    
    func stateComplete() {
        self.hideAllVoiceIndicators()
        self.hideTextIndicator()
    }
    
    func changeTextIndicatorWithMessage(text: String) {
        if !self.isInKeyboardMode {
            self.recognizerStateIndicatorLabel.text = text
        }
    }
    
    func hideTextIndicator() {
        self.recognizerStateIndicatorLabel.text = ""
    }
    
    func textIndicatorWait() {
        self.changeTextIndicatorWithMessage("Ждите")
    }
    
    func textIndicatorSpeeak() {
        self.changeTextIndicatorWithMessage("Говорите")
    }
    
    func textIndicatorListen() {
        self.changeTextIndicatorWithMessage("Слушаю")
    }
    
    func textIndicatorRecognizing() {
        self.changeTextIndicatorWithMessage("Распознаю")
    }
    
    func showMicrophoneIndicator() {
        self.microphoneImage.image = UIImage(named: "ic_microphone")
    }
    
    func hideMicrophoneIndicator() {
        self.microphoneImage.image = UIImage(named: "ic_microphone_disabled")
    }
    
    func showVoiceIndicator() {
        self.voiceIndicator.animate()
    }
    
    func hideVoiceIndicator() {
        self.voiceIndicator.stop()
    }
    
    func hideAllVoiceIndicators() {
        hideMicrophoneIndicator()
        hideVoiceIndicator()
        hideTextIndicator()
    }
    
    /*
    Indicators <
    */
    
    func switchToKeyboardNode() {
        self.isInKeyboardMode = true
        self.microphoneImage.stopAnimating()
        self.microphoneImage.image = UIImage(named: "ic_keyboard")
        
        self.hideTextIndicator()
        
        self.inputField.becomeFirstResponder()
    }
    
    func switchToAudioMode() {
        self.isInKeyboardMode = false
        self.microphoneImage.stopAnimating()
        self.hideMicrophoneIndicator()
    }
    
    func isKeyboardMode() -> Bool {
        return self.isInKeyboardMode
    }
    
    func lockInputField() {
        self.inputField.enabled = false
    }
    
    func unlockInputField() {
        self.inputField.enabled = true
    }
    
    func hideWarning() {
        self.warningLabel.text = ""
    }
    
    func showWarning(type: WarningType) {
        self.warningLabel.text = type.message()
    }
    
    func clearText() {
        self.inputField.text = ""
    }
    
    func showWord(word: String) {
        self.inputField.text = word.capitalizedString
    }
    
    func updateWordsCount(text: String) {
        self.wordsCountLabel.text = text
    }
    
    func unlockChitButtons() {
        self.addTimeChitButton.enabled = true
        self.solveWordChitButton.enabled = true
        self.changeWordChitButton.enabled = true
        
        self.addTimeChitIcon.image = UIImage(named: "ic_help_time_reset")
        self.solveWordChitIcon.image = UIImage(named: "ic_help_word")
        self.changeWordChitIcon.image = UIImage(named: "ic_help_change_word")
    }
    
    func lockChitButtons() {
        self.addTimeChitButton.enabled = false
        self.solveWordChitButton.enabled = false
        self.changeWordChitButton.enabled = false
        
        delayCall(1.0) {
            self.addTimeChitIcon.image = UIImage(named: "ic_help_time_reset_disabled")
            self.solveWordChitIcon.image = UIImage(named: "ic_help_word_disabled")
            self.changeWordChitIcon.image = UIImage(named: "ic_help_change_word_disabled")
        }
    }
    
    /*
    MARK: Chits activation animation >
    */
    
    var activationAnimationPlayed = false
    func chitAddTimePlayActivationAnimation() {
        if Settings.isMusicEnabled() { soundsBoss.playOnceChitAddTime() }
        
        let rotationY = CABasicAnimation(keyPath: "transform.rotation.y")
        rotationY.fromValue = 0
        rotationY.toValue = CGFloat(2.0*M_PI)
        rotationY.duration = 1.0
        
        self.addTimeChitIcon.layer.addAnimation(rotationY, forKey: "RotationY")
    }
    
    func chitSolveWordPlayActivationAnimation() {
        if Settings.isMusicEnabled() { soundsBoss.playOnceChitSolveWord() }
        
        let bounceAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        bounceAnimation.values = [1.0 ,1.4, 0.9, 1.15, 0.95, 1.02, 1.0]
        bounceAnimation.duration = NSTimeInterval(1.0)
        bounceAnimation.calculationMode = kCAAnimationCubic
        
        self.solveWordChitIcon.layer.addAnimation(bounceAnimation, forKey: nil)
    }
    
    func chitChangeWordPlayActivationAnimation() {
        if Settings.isMusicEnabled() { soundsBoss.playOnceChitChangeWord() }
        
        let rotationZ = CABasicAnimation(keyPath: "transform.rotation.z")
        rotationZ.fromValue = 0
        rotationZ.toValue = CGFloat(-2.0*M_PI)
        rotationZ.duration = 1.0
        
        self.changeWordChitIcon.layer.addAnimation(rotationZ, forKey: "RotationZ")
    }
    
    /*
    Chits activation animation <
    */
    
    
    /*
    MARK: Chits periodical animation >
    */
    
    var playChitAnimationAddTime = false
    var playChitAnimationSolveWord = false
    var playChitAnimationChangeWord = false
    
    private func disableAllChitAnimations() {
        self.playChitAnimationAddTime = false
        self.playChitAnimationSolveWord = false
        self.playChitAnimationChangeWord = false
    }
    
    func hideAllChitsDescriptions() {
        self.csResetTimerLabelHeight.constant = 0
        self.csSolveWordLabelHeight.constant = 0
        self.csChangeWordLabelHeight.constant = 0
    }
    func showDescriptionChitAddTime() {
        self.bottomBlockContainer.setNeedsUpdateConstraints()
        self.hideAllChitsDescriptions()
        self.chitAddTimeLabel.transform = CGAffineTransformMakeScale(1, 1)
        self.csResetTimerLabelHeight.constant = 24
        self.bottomBlockContainer.layoutIfNeeded()
    }
    func showDescriptionChitChangeWord() {
        self.bottomBlockContainer.setNeedsUpdateConstraints()
        self.hideAllChitsDescriptions()
        self.chitChangeWordLabel.transform = CGAffineTransformMakeScale(1, 1)
        self.csChangeWordLabelHeight.constant = 24
        self.bottomBlockContainer.layoutIfNeeded()
    }
    func showDescriptionChitSolveWord() {
        self.bottomBlockContainer.setNeedsUpdateConstraints()
        self.hideAllChitsDescriptions()
        self.chitSolveWordLabel.transform = CGAffineTransformMakeScale(1, 1)
        self.csSolveWordLabelHeight.constant = 24
        self.bottomBlockContainer.layoutIfNeeded()
    }
    
    func checkAnimations(countDown: Float, levelTime: Float, isOpponentTurn: Bool) {
        if isOpponentTurn {
            self.disableAllChitAnimations()
            return
        }
        
        let percent = countDown/levelTime * 100.0
        
        if percent <= 70 && percent > 65 {
            self.animateChiChangeWord()
        }
        if percent <= 50 && percent > 35 {
            self.animateChitSolveWord()
        }
        if percent <= 20 {
            self.animateChitAddTimer()
        }
    }
    
    private func animateChitAddTimer() {
        if !self.playChitAnimationAddTime {
            self.playChitAnimationAddTime = true
            self.animateChit(self.chitAddTimeLabel, constrain: self.csResetTimerLabelHeight)
        }
    }
    
    private func animateChitSolveWord() {
        if !self.playChitAnimationSolveWord {
            self.playChitAnimationSolveWord = true
            self.animateChit(self.chitSolveWordLabel, constrain: self.csSolveWordLabelHeight)
        }
    }
    
    private func animateChiChangeWord() {
        if !self.playChitAnimationChangeWord {
            self.playChitAnimationChangeWord = true
            self.animateChit(self.chitChangeWordLabel, constrain: self.csChangeWordLabelHeight)
        }
    }
    
    private func animateChit(label: UILabel, constrain: NSLayoutConstraint) {
        label.transform = CGAffineTransformMakeScale(0.0, 0.0)
        self.bottomBlockContainer.setNeedsUpdateConstraints()
        UIView.animateWithDuration(2.0, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.05, options: [],
            animations: {
                label.transform = CGAffineTransformMakeScale(1, 1)
                constrain.constant = 24
                self.bottomBlockContainer.layoutIfNeeded()
            },
            completion: { completeSuccess in
                
                self.bottomBlockContainer.setNeedsUpdateConstraints()
                UIView.animateWithDuration(1.0, delay: 2.0, options: [],
                    animations: {
                        label.transform = CGAffineTransformMakeScale(0.05, 0.05)
                        constrain.constant = 0
                        self.bottomBlockContainer.layoutIfNeeded()
                    },
                    completion: { completeSuccess in
                        //
                })
                
        })
    }
    
    private func hideChitsHints() {
        self.chitAddTimeLabel.transform = CGAffineTransformMakeScale(0, 0)
        self.chitSolveWordLabel.transform = CGAffineTransformMakeScale(0, 0)
        self.chitChangeWordLabel.transform = CGAffineTransformMakeScale(0, 0)
        
        self.csResetTimerLabelHeight.constant = 0
        self.csChangeWordLabelHeight.constant = 0
        self.csSolveWordLabelHeight.constant = 0
    }
    
    /*
    Chits periodical animation <
    */
    
    @IBAction func addTimeChitAction(sender: AnyObject?) {
        self.chitAddTimeCallback?()
    }
    
    @IBAction func solveWordChitAction(sender: AnyObject?) {
        self.chitSolveWordCallback?()
    }
    
    @IBAction func changeWordChitAction(sender: AnyObject?) {
        self.chitChangeWordCallback?()
    }
}
