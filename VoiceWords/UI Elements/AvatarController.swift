//
//  AvatarController.swift
//  VoiceWords
//
//  Created by Georg Romas on 23/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

class AvatarController: UIStackView {
    
    @IBOutlet var imageFrom: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    func updateAvatar() {
        self.label.font = UIFont(name: Constants.Font_OpenSans_Bold, size: 18)
        self.imageFrom.image = ScoreStorage.currentAvatar()
        self.label.text = ScoreStorage.currentStatusName()
    }
    
    func changeAvatar() {
        let imageTo = UIImageView(image: ScoreStorage.nextAvatar())
        self.imageFrom.superview!.addSubview(imageTo)
        UIView.transitionFromView(self.imageFrom, toView: imageTo, duration: 1.0, options: [.TransitionFlipFromLeft, .ShowHideTransitionViews], completion: {complete in
            imageTo.snp_makeConstraints { make in
                make.size.equalTo(self.imageFrom)
                make.center.equalTo(self.imageFrom)
                self.label.text = ScoreStorage.nextStatusName()
            }
        })
    }

}
