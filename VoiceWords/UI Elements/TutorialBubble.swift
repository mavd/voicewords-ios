//
//  TutorialBubble.swift
//  VoiceWords
//
//  Created by Georg Romas on 10/03/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

class TutorialBubble: UIView {
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.layer.cornerRadius = self.frame.height * 0.5
    }
}
