//
//  LevelDisabledCell.swift
//  VoiceWords
//
//  Created by Georg Romas on 12/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

class LevelDisabledCell: LevelCell {
    
    override func borderColor() -> UIColor {
        return UIColor.levelCellDisabledBorder()
    }
}
