//
//  CoinsController.swift
//  VoiceWords
//
//  Created by Georg Romas on 18/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation
import UIKit

/// Coins indicator view
class CoinsController: UIView {

    /// Text label
    @IBOutlet weak var label: UILabel!
    /// Icon
    @IBOutlet weak var image: UIImageView!
    
    private var coins: Int = 0
    
    /// Setup view
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.label.font = UIFont(name: Constants.Font_OpenSans_Semibold, size: 14)
        
        self.actualizeDisplayedData()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CoinsController.actualizeDisplayedData), name: Constants.PurchaseCompleteEvent, object: nil)
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    /// Update current state
    @objc func actualizeDisplayedData() {
        self.coins = CoinsStorage.userCoins()
        self.label.text = "\(self.coins)"
    }
    
    /// Increment coins count
    func revenue(newCoins: Int) {
        self.animateCoinsRevenue(newCoins)
        self.coins += newCoins
        CoinsStorage.refill(newCoins)
    }
    
    /// Decrement coins count
    func withdraw(coinsMinus: Int) -> Bool {
        if self.coins >= coinsMinus {
            self.animateCoinsPayment(coinsMinus)
            CoinsStorage.withdraw(coinsMinus)
            self.coins -= coinsMinus
            
            return true
        } else {
            return false
        }
    }
    
    private func animateCoinsRevenue(newCoins: Int) {
        self.label.text = "\(self.coins + newCoins)"
    }
    
    private func animateCoinsPayment(newCoins: Int) {
        self.label.changeNumberAnimated(self.coins, to: self.coins - newCoins, steps: 6, format: "%d", interval: 0.07)
    }
    
}