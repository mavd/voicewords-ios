//
//  VoiceIndicatorView.swift
//  VoiceWords
//
//  Created by Georg Romas on 13/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

class VoiceIndicatorView: UIView {
    
    private var isStopped = false

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor.viceIdicator()
        self.layer.cornerRadius = self.frame.height/2
        self.hidden = true
    }
    
    func stop() {
        self.isStopped = true
        self.hidden = true
    }
    
    func animate() {
        self.hidden = false
        self.isStopped = false
        let randomDuration = [0.1, 0.2, 0.3, 0.4]
        
        func loop() {
            UIView.animateWithDuration(randomDuration.randomItem(),
                animations: { self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.5, 1.5) },
                completion: {complete in if self.isStopped { return }
                    
                    UIView.animateWithDuration(randomDuration.randomItem(),
                        animations: { self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0) },
                        completion: {complete in if self.isStopped { return }
                            
                            UIView.animateWithDuration(randomDuration.randomItem(),
                                animations: { self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.3, 1.3) },
                                completion: {complete in if self.isStopped { return }
                                    
                                    UIView.animateWithDuration(randomDuration.randomItem(),
                                        animations: { self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.5, 1.5) },
                                        completion: {complete in if self.isStopped { return }
                                            
                                            UIView.animateWithDuration(randomDuration.randomItem(),
                                                animations: { self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0) },
                                                completion: {complete in if self.isStopped { return }
                                                    
                                                    UIView.animateWithDuration(randomDuration.randomItem(),
                                                        animations: { self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.5, 0.5) },
                                                        completion: {complete in if self.isStopped { return }
                                                            
                                                            UIView.animateWithDuration(randomDuration.randomItem(),
                                                                animations: { self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.3, 0.3) },
                                                                completion: {complete in if self.isStopped { return }
                                                                    
                                                                    UIView.animateWithDuration(randomDuration.randomItem(),
                                                                        animations: { self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.5, 1.5) },
                                                                        completion: {complete in if self.isStopped { return }
                                                                            
                                                                            UIView.animateWithDuration(randomDuration.randomItem(),
                                                                                animations: { self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.2, 1.2) },
                                                                                completion: {complete in if self.isStopped { return }
                                                                                    
                                                                                    UIView.animateWithDuration(randomDuration.randomItem(),
                                                                                        animations: { self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.5, 1.5) },
                                                                                        completion: {complete in if self.isStopped { return }
                                                                                            
                                                                                            UIView.animateWithDuration(randomDuration.randomItem(),
                                                                                                animations: { self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0) },
                                                                                                completion: {complete in if self.isStopped { return }
                                                                                                    
                                                                                                    loop()
                                                                                                    
                                                                                            })
                                                                                            
                                                                                    })
                                                                                    
                                                                            })
                                                                            
                                                                    })
                                                            })
                                                            
                                                    })
                                                    
                                            })
                                    })
                            })
                    })
            })
        }
        loop()
    }

}
