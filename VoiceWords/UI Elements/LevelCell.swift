//
//  LevelCell.swift
//  VoiceWords
//
//  Created by Georg Romas on 12/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

class LevelCell: UICollectionViewCell {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var levelsLabel: UILabel!
    
    func borderColor() -> UIColor {
        return UIColor.colorWithHexString("#FD5D08")
    }
    
    private let strokeLayer = CAShapeLayer()
    private var archiveBackViewColor = UIColor.blackColor()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        archiveBackViewColor = backView.backgroundColor!
        adjustCell()
    }
    
    func adjustCell() {
        backView.layer.cornerRadius = self.frame.size.height/2
        backView.layer.borderWidth = 1
        backView.layer.borderColor = borderColor().CGColor
    }
    
    
    func animateSelection() {
// experimental animation
//        self.layer.addSublayer(strokeLayer)
//        
//        strokeLayer.frame = CGRect(x: 0.5, y: 0.5, width: self.layer.frame.width-2, height: self.layer.frame.height-2)
//        strokeLayer.strokeColor = UIColor.levelCellHightlightStrocke().CGColor
//        strokeLayer.lineWidth = 2.0
//        strokeLayer.fillColor = UIColor.clearColor().CGColor
//        let radius = strokeLayer.frame.height/2
//        strokeLayer.path = UIBezierPath(roundedRect: strokeLayer.frame, byRoundingCorners: UIRectCorner.AllCorners, cornerRadii: CGSizeMake(radius, radius)).CGPath
//        strokeLayer.strokeStart = 0.0
//        strokeLayer.strokeEnd = 0.0
//        
//        let animation = CABasicAnimation()
//        animation.duration = CFTimeInterval(0.3)
//        animation.keyPath = "strokeEnd"
//        animation.fromValue = 0.0
//        animation.toValue = 1.0
//        animation.fillMode = kCAFillModeForwards
//        animation.removedOnCompletion = false
//        
//        strokeLayer.addAnimation(animation, forKey: "strocke_fill_animation")
        
        
        backView.backgroundColor = borderColor()
    }
    
    func hideSelection() {
//        strokeLayer.removeAllAnimations()
        backView.backgroundColor = archiveBackViewColor
    }
    
}
