//
//  ConfettiView.swift
//  VoiceWords
//
//  Created by Georg Romas on 28/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

class ConfettiView: UIView {
    
    private let emitterLayer = CAEmitterLayer()
    
    func addConfettiInView(parent: UIView) {
        self.frame = parent.frame
        parent.addSubview(self)
        self.userInteractionEnabled = false
        
        var frame = parent.frame
        frame.origin.x += 150
        frame.origin.y -= 200
        self.emitterLayer.frame = frame
        
        self.emitterLayer.renderMode = kCAEmitterLayerOldestFirst
        self.emitterLayer.emitterShape = kCAEmitterLayerLine
        self.emitterLayer.emitterMode = kCAEmitterLayerAdditive
        
        let cell = CAEmitterCell()
        cell.contentsRect = CGRect(x: 0, y: 0, width: 2.0, height: 1.0)
        cell.contents = UIImage(named: "confetti")!.CGImage
        cell.birthRate = 10.0
        cell.lifetime = 10.0
        cell.scale = 0.2
        
        cell.color = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1.0).CGColor
        cell.redRange = 0.8
        cell.blueRange = 0.8
        cell.greenRange = 0.8
        
        
        cell.spin = CGFloat(M_PI)
        cell.spinRange = 10.0
        
        cell.velocity = 100
        cell.velocityRange = 50
        cell.yAcceleration = 0
        
        cell.emissionRange = CGFloat(M_PI_2)
        cell.emissionLongitude = CGFloat(M_PI)
        
        self.emitterLayer.emitterCells = [cell]
        
        self.layer.addSublayer(self.emitterLayer)
    }
    
    func stop() {
        self.emitterLayer.birthRate = 0
    }
    
    
}
