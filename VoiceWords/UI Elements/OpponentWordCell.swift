//
//  OpponentPhraseCell.swift
//  VoiceWords
//
//  Created by Georg Romas on 13/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

class OpponentWordCell: WordCell {

    class func loadFromNib(word: String) -> OpponentWordCell {
        let view = NSBundle.mainBundle().loadNibNamed("OpponentWordCell", owner: self, options: nil)[0] as! OpponentWordCell
        view.wordText = word
        view.titleText = "Искусственный интеллект"
        return view
    }

}
