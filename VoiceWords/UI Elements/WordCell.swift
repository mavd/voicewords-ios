//
//  PhraseCell.swift
//  VoiceWords
//
//  Created by Georg Romas on 13/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

class WordCell: UIView {
    
    @IBOutlet weak var bubble: UIView!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!

    var wordText = ""
    var titleText = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.bubble.layer.cornerRadius = self.bubble.frame.height/2
        self.avatar.layer.cornerRadius = self.avatar.frame.height/2
        
        
        self.titleLabel.font = UIFont(name: Constants.Font_OpenSans_Semibold, size: 9)
        
        self.messageLabel.textColor = UIColor.charcoal()
        self.messageLabel.font = UIFont(name: Constants.Font_OpenSans_Bold, size: 18)
    }
    
    var prepareAnimationShouldStop = false
    func playAIPreparesAnimation() {
        let firstChar = String(self.wordText.characters.first!).capitalizedString
        self.messageLabel.text = firstChar
        
        var count = 0
        func dotsAnimation() {
            if self.prepareAnimationShouldStop { return }
            delayCall(0.6) {
                if self.prepareAnimationShouldStop { return }
                
                count += 1
                if count >= 4 {
                    count = 0
                    self.messageLabel.text = firstChar
                } else {
                    self.messageLabel.text! += "."
                }
                dotsAnimation()
            }
        }
        dotsAnimation()
    }
    
    func playAIAnimation() {
        self.prepareAnimationShouldStop = true
        self.messageLabel.text = self.wordText.capitalizedString
    }
    
    func playAnimation() {
        self.messageLabel.text = ""
        self.titleLabel.text = ""
        self.needsUpdateConstraints()
        self.layoutIfNeeded()
        
        self.titleLabel.text = titleText
        self.messageLabel.text = self.wordText.capitalizedString
        self.bubble.alpha = 0
        UIView.animateWithDuration(1.2) {
            self.bubble.alpha = 1.0
        }
    }
}
