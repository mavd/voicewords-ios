//
//  PlayerPhraseCell.swift
//  VoiceWords
//
//  Created by Georg Romas on 13/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

class PlayerWordCell: WordCell {

    class func loadFromNib(word: String, avatar: UIImage) -> PlayerWordCell {
        let view = NSBundle.mainBundle().loadNibNamed("PlayerWordCell", owner: self, options: nil)[0] as! PlayerWordCell
        view.wordText = word
        view.titleText = "Ваш ответ"
        view.avatar.image = avatar
        return view
    }

}
