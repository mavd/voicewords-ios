//
//  YandexVocalizer.swift
//  VoiceWords
//
//  Created by Georg Romas on 17/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation

/**
 Wrapper for `YSKVocalizer` from Yandeks Speech Kit
 */
class YandexVocalizer: NSObject, VocalizerProtocol, YSKVocalizerDelegate {
    
    /// YSKVocalizer holder property for ability to cancel speech
    private var speecher = YSKVocalizer(text: "Тест", language: "ru-RU")
    
    private var speechCompleteCallback:((success: Bool) -> Void)?
    private var speechStartCallback:(() -> Void)?
    
    override init() {
        super.init()
    }
    
    // MARK: VocalizerProtocol
    
    func textToVoice(text: String, complete: (success: Bool) -> Void, speechStart: (() -> Void)?) {
        speecher = YSKVocalizer(text: text, language: "ru-RU", autoPlay: true, voice: GameSettings.YSKVocalizerVoice())
        self.speechCompleteCallback = complete
        self.speechStartCallback = speechStart
        speecher.delegate = self
        
        speecher.start()
    }
    
    func cancel() {
        self.speecher.cancel()
    }
    
    // MARK: YSKVocalizerDelegate
    
    func vocalizer(vocalizer: YSKVocalizer!, didFailWithError error: NSError!) {
        log.debug("Y_SPEECH -- didFailWithError \(error)")
        self.speechCompleteCallback?(success: false)
    }
    
    func vocalizer(vocalizer: YSKVocalizer!, didFinishSynthesisWithResult result: NSData!, markerIds: [AnyObject]!, markerMsecs: [AnyObject]!) {
        log.debug("Y_SPEECH -- didFinishSynthesisWithResult")
    }
    
    func vocalizerDidBeginSynthesis(vocalizer: YSKVocalizer!) {
        log.debug("Y_SPEECH -- vocalizerDidBeginSynthesis")
    }
    
    func vocalizerDidFinishPlaying(vocalizer: YSKVocalizer!) {
        log.debug("Y_SPEECH -- vocalizerDidFinishPlaying")
        self.speechCompleteCallback?(success: true)
    }
    
    func vocalizerDidStartPlaying(vocalizer: YSKVocalizer!) {
        log.debug("Y_SPEECH -- vocalizerDidStartPlaying")
        self.speechStartCallback?()
    }
}
