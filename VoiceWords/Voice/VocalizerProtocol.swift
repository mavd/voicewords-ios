//
//  VocalizerProtocol.swift
//  VoiceWords
//
//  Created by Georg Romas on 23/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation

/// All vocalizers (yandex and siri) conform to this protocol in reason of polymorphism
protocol VocalizerProtocol {
    
    /// Syntesize text to speech
    func textToVoice(text: String, complete: (success: Bool) -> Void, speechStart:(() -> Void)?)
    
    /// Cancel speech
    func cancel()
}