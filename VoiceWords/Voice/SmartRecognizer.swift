//
//  SmartRecognizer.swift
//  SpeechDemo
//
//  Created by Georg Romas on 05/02/16.
//  Copyright © 2016 Georg. All rights reserved.
//

import Foundation

/**
 Wrapper for `YSKRecognizer` from Yandeks Speech Kit
 
 *Smart* mean additional improvements, such as api response validation and 
 handy interface with callbacks instead of delegation pattern
 
 **Note:** All task should be executed in main thread
 */
class SmartRecognizer: NSObject, YSKRecognizerDelegate {
    
    private var speechRecognizer: YSKRecognizer!
    
    /// `Boolean` recognizer state indicator. Is it busy or not.
    private(set) var isRecognizing = false
    
    /// Organize reconizer states into one object
    struct RecognizerCallback {
        private let startListen:(()->Void)?
        private let startRecognize:(()->Void)?
        private let speechDetect:(()->Void)?
        private let complete:((hypothesis: [YSKRecognitionHypothesis]?, mostRelevant: String?)->Void)?
        
        init(startListen:(()->Void)?, startRecognize:(()->Void)?, speechDetect:(()->Void)?, complete:((hypothesis: [YSKRecognitionHypothesis]?, mostRelevant: String?)->Void)?) {
            self.startListen = startListen
            self.startRecognize = startRecognize
            self.speechDetect = speechDetect
            self.complete = complete
        }
    }
    
    private var listenCallback:RecognizerCallback!
    
    override init() {
        super.init()
    }
    
    /// Start recognition and execute `RecognizerCallback` when voice detected end recognized
    ///
    /// Previous recognition tasks will be canceled
    func listen(callBack:RecognizerCallback) {
        self.listenCallback = callBack
        
        if let r = self.speechRecognizer {
            r.cancel()
        }
        
        self.speechRecognizer = YSKRecognizer(language: "ru-RU", model: "general")
        self.speechRecognizer.delegate = self
        self.speechRecognizer.start()
    }
    
    /// Stop recognition and cancels the recognition request
    func cancel() {
        self.speechRecognizer?.cancel()
    }
    
    //MARK: YSKRecognizerDelegate
    
    func recognizer(recognizer: YSKRecognizer!, didFailWithError error: NSError!) {
        log.error("RECOGNIZER FATAL: \(error.description)")
        
        // - 7 Networking communication error
        // - 9 forget error code
        if ![7,9].contains(error.code) {
            self.listen(self.listenCallback)
            log.info("restart recognizer")
        } else {
            self.isRecognizing = false
        }
    }
    
    func recognizerDidStartRecording(recognizer: YSKRecognizer!) {
        log.info("RECOGNIZER Did Start Recording")
        self.isRecognizing = true
        self.listenCallback.startListen?()
    }
    
    func recognizerDidDetectSpeech(recognizer: YSKRecognizer!) {
        log.info("RECOGNIZER Did Detect Speech")
        self.listenCallback.speechDetect?()
    }
    
    func recognizerDidFinishRecording(recognizer: YSKRecognizer!) {
        log.info("RECOGNIZER Did Finish Recording")
        self.listenCallback.startRecognize?()
    }
    
    func recognizer(recognizer: YSKRecognizer!, didUpdatePower power: Float) {
        //log.info("RECOGNIZER update power level: \(power)")
        //TODO: check low power huppen
    }
    
    func recognizer(recognizer: YSKRecognizer!, didCompleteWithResults results: YSKRecognition!) {
        log.info("RECOGNIZER complete with result \(results.hypotheses)")
        self.isRecognizing = false
        
        guard results.hypotheses.count > 0, let hyps = results.hypotheses as? [YSKRecognitionHypothesis] else {
            print("listener return empty hypotheses list! Restart recognizer.")
            self.listen(self.listenCallback)
            return
        }
        
        let hypothesisOrdered = hyps.sort({$0.confidence > $1.confidence })

        let mostRelevantHypothise = hypothesisOrdered.first!
        
        log.info("mostRelevantHypothise.confidence : \(mostRelevantHypothise.confidence)")

        if mostRelevantHypothise.normalized.isEmpty {
            log.warning("RECOGNIZER recognize empty word! Restart recognizer.")
            self.listen(self.listenCallback)
            return
        }
        
        // Check if word is not exist in database, try next from hypotheses >
        
        var validWord = ""
        func wordExistInDB(word: String) -> String? {
            do {
                
                var count = 0
                try DBHelper.db().inDatabase { db in
                    count = Int.fetchOne(db, "SELECT COUNT(*) FROM words WHERE word = ?", arguments: [word])!
                }
                
                if count == 0 {
                    log.info("recognized hypothise \(word) not found in local words db")
                } else {
                    return word
                }
                
            } catch let e {
                print("ERROR: sql executoin: \(e)")
            }
            
            return nil
        }
        
        for hypothise in hypothesisOrdered {
            let normalized = hypothise.normalized.lowercaseString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            if !normalized.isEmpty {
                log.info("Check hypothise \(normalized) in db")
                if let exist = wordExistInDB(normalized) {
                    validWord = exist
                    log.info("YEP! hypothise \(normalized) EXIST in db")
                    break
                }
            }
        }
        
        // Check if word is not exist in database, try next from hypotheses <
        
        //If validation fails, show first recognized, tho show user error feedback
        if validWord.isEmpty {
            validWord = mostRelevantHypothise.normalized
        }
        
        self.listenCallback.complete?(hypothesis: hyps, mostRelevant: validWord)
    }
    
}