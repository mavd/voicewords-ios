//
//  SmartVocalizer.swift
//  SpeechDemo
//
//  Created by Georg Romas on 05/02/16.
//  Copyright © 2016 Georg. All rights reserved.
//

import Foundation
import AVFoundation
import XCGLogger

/**
 Wrapper for `AVSpeechSynthesizer` (Siri) from AVFoundation
 */
class SmartVocalizer: NSObject, VocalizerProtocol, AVSpeechSynthesizerDelegate  {
    
    /// AVSpeechSynthesizer holder property for ability to cancel speech
    private var speecher: AVSpeechSynthesizer!
    private let voice = AVSpeechSynthesisVoice(language: "ru-RU")!
    private var speechCallback:((success: Bool) -> Void)!
    private var speechStartCallback:(() -> Void)?
    
    override init() {
        super.init()
        
        speecher = AVSpeechSynthesizer()
        speecher.delegate = self
    }
    
    /// Actualy not used now. Previously used for debugging.
    func textToVoice(text: String) -> AVSpeechSynthesizer {
        let voice = AVSpeechSynthesisVoice(language: "ru-RU")!
        let speechUtterance = AVSpeechUtterance(string: text)
        speechUtterance.rate = 0.40
        speechUtterance.volume = 1.0
        speechUtterance.voice = voice
        
        speecher.speakUtterance(speechUtterance)
        return speecher
    }
    
    // MARK: VocalizerProtocol
    
    func textToVoice(text: String, complete: (success: Bool) -> Void, speechStart:(() -> Void)? = nil) {
        let speechUtterance = AVSpeechUtterance(string: text)
        speechUtterance.rate = 0.40
        speechUtterance.volume = 1.0
        speechUtterance.voice = voice
        
        self.speechCallback = complete
        self.speechStartCallback = speechStart
        
        speecher.stopSpeakingAtBoundary(AVSpeechBoundary.Immediate)
        speecher.speakUtterance(speechUtterance)
    }
    
    func cancel() {
        speecher.stopSpeakingAtBoundary(AVSpeechBoundary.Immediate)
    }
    
    //MARK: AVSpeechSynthesizerDelegate
    
    func speechSynthesizer(synthesizer: AVSpeechSynthesizer, didStartSpeechUtterance utterance: AVSpeechUtterance) {
        log.info("SPEECH -- didStart")
        self.speechStartCallback?()
    }
    
    func speechSynthesizer(synthesizer: AVSpeechSynthesizer, didCancelSpeechUtterance utterance: AVSpeechUtterance) {
        log.info("SPEECH -- didCancel")
        self.speechCallback?(success: false)
    }
    
    func speechSynthesizer(synthesizer: AVSpeechSynthesizer, didFinishSpeechUtterance utterance: AVSpeechUtterance) {
        log.info("SPEECH -- didFinish")
        self.speechCallback?(success: true)
    }
}