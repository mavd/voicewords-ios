//
//  LevelsCompletionStorage.swift
//  VoiceWords
//
//  Created by Georg Romas on 28/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation

/// Wrapper for NSUserDefaults to trak complete levels and solved words per level
class LevelsCompletionStorage {    
    static private let kLevelBaseKey = "kLevelBaseKey"
    static private let kLastLevelKey = "kLastLevelKey"
    
    static private let settings = NSUserDefaults.standardUserDefaults()
    
    // MARK: Access
    
    /// Returns `Int` number of solved words words
    class func solvedWordsForLevel(level: Int) -> Int {
        let count = settings.integerForKey(keyForLevel(level))
        return count
    }
    
    /// Returns `Int` last not complete level number
    class func lastNotCompleteLevel() -> Int {
        let level =  settings.integerForKey(kLastLevelKey)
        var result = level
        if result > 8 || result <= 0 {
            result = 1
        }
        return result
    }
    
    // MARK: Change
    
    /**
     
     Save number of solved words for level
     
     - Parameter count: `Int` number of solved words
     - Parameter level: `Int` level number
     
     */
    class func saveSolvedWords(count: Int, level: Int) {
        settings.setInteger(count, forKey: keyForLevel(level))
        settings.synchronize()
    }
    
    /// Save last not complete level number
    class func saveLastNotCompleteLevel(level: Int) {
        settings.setInteger(level, forKey: kLastLevelKey)
        settings.synchronize()
    }
    
    class private func keyForLevel(level: Int) -> String {
        return "\(kLevelBaseKey)_\(level)"
    }
}