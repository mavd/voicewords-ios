//
//  GameSettings.swift
//  VoiceWords
//
//  Created by Georg Romas on 19/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation

/// In reason Yandex Speech Kit is freezed some times, we can use Siri as vocalizer.
///
/// **Note:** Why it is global?
/// By analogy with Yandex Speech Kit constant variabes like `YSKVocalizerVoiceJane`, `YSKVocalizerVoiceAlyss`
let YSKVocalizerVoiceSiri = "YSKVocalizerVoiceSiri"

/**
 Wrapper for NSUserDefaults for additional game settings:
 
 - Which vocalizet to use Yandex/Siri
 - Target words cound per level
 - Target word timout per level
 - Cost for each help
 - Score number for max user level (actually not used after last changes as planning)
 - Pause words timer when voice recognizer in progress or not
 - Coins reward for solved word
 - Animation settings for level loading screen
 */
class GameSettings {
    
    static private let kGSettingsYandexVoiceKey = "kGSettingsYandexVoiceKey"
    static private let kGSettingsWordsCountKey = "kGSettingsWordsCountKey"
    static private let kGSettingsGameIntervalKey = "kGSettingsGameIntervalKey"
    static private let kGSettingsChitCostKey = "kGSettingsChitCostKey"
    static private let kGSettingsTopScoreKey = "kGSettingsTopScoreKey"
    static private let kGSettingsPauseTimerOnRecognitionKey = "kGSettingsPauseTimerOnRecognitionKey"
    static private let kGSettingsCoinsPerWordKey = "kGSettingsCoinsPerWordKey"
    
    //animations on level loading screen
    static private let kGSettings_A_ChitMaxScale = "kGSettings_A_ChitMaxScale"
    static private let kGSettings_A_ChitTimeUp = "kGSettings_A_ChitTimeUp"
    static private let kGSettings_A_ChitTimeDown = "kGSettings_A_ChitTimeDown"
    
    static private let settings = NSUserDefaults.standardUserDefaults()
    
    /// Chit type identifiator
    enum ChitID: String {
        case Change, Solve, Time
    }
    
    // MARK: Access
    
    class func isVocalizerSiri() -> Bool {
        return settings.stringForKey(kGSettingsYandexVoiceKey) == YSKVocalizerVoiceSiri
    }

    class func isYandexVoiceZahar() -> Bool {
        return settings.stringForKey(kGSettingsYandexVoiceKey) == YSKVocalizerVoiceZahar
    }
    
    class func isYandexVoiceErmil() -> Bool {
        return settings.stringForKey(kGSettingsYandexVoiceKey) == YSKVocalizerVoiceErmil
    }
    
    class func isYandexVoiceJane() -> Bool {
        return settings.stringForKey(kGSettingsYandexVoiceKey) == YSKVocalizerVoiceJane
    }
    
    class func isYandexVoiceAlyss() -> Bool {
        return settings.stringForKey(kGSettingsYandexVoiceKey) == YSKVocalizerVoiceAlyss
    }
    
    class func YSKVocalizerVoice() -> String {
        guard let voice = settings.stringForKey(kGSettingsYandexVoiceKey) else {
            return YSKVocalizerVoiceAlyss
        }
        
        return voice
    }
    
    class func wordsCountForLevel(level: Int) -> Int {
        return settings.integerForKey("\(kGSettingsWordsCountKey)_\(level)")
    }
    
    class func gameIntervalForLevel(level: Int) -> Int {
        return settings.integerForKey("\(kGSettingsGameIntervalKey)_\(level)")
    }
    
    class func chitCostInCoins(type: ChitID) -> Int {
        return settings.integerForKey(kGSettingsChitCostKey + type.rawValue)
    }
    
    class func topScore() -> Int {
        return settings.integerForKey(kGSettingsTopScoreKey)
    }
    
    class func pauseTimerOnRecognition() -> Bool {
        return settings.boolForKey(kGSettingsPauseTimerOnRecognitionKey)
    }
    
    class func coinsPerWordDonation() -> Int {
        return settings.integerForKey(kGSettingsCoinsPerWordKey)
    }
    
    class func chitAnimationMaxScale() -> Float {
        return settings.floatForKey(kGSettings_A_ChitMaxScale)
    }
    
    class func chitAnimationTimeUp() -> Float {
        return settings.floatForKey(kGSettings_A_ChitTimeUp)
    }
    
    class func chitAnimationTimeDown() -> Float {
        return settings.floatForKey(kGSettings_A_ChitTimeDown)
    }
    
    
    // MARK: Change
    
    class func saveVoice(voice: String) {
        settings.setObject(voice, forKey: kGSettingsYandexVoiceKey)
        settings.synchronize()
    }
    
    class func setWordsCountForLevel(level: Int, wordsCount: Int) {
        settings.setInteger(wordsCount, forKey: "\(kGSettingsWordsCountKey)_\(level)")
        settings.synchronize()
    }
    
    class func setGameIntervalForLevel(level: Int, interval: Int) {
        settings.setInteger(interval, forKey: "\(kGSettingsGameIntervalKey)_\(level)")
        settings.synchronize()
    }
    
    class func setChitCostInCoins(coins: Int, type: ChitID) {
        settings.setInteger(coins, forKey: kGSettingsChitCostKey + type.rawValue)
        settings.synchronize()
    }
    
    class func setTopScore(score: Int) {
        settings.setInteger(score, forKey: kGSettingsTopScoreKey)
    }
    
    class func setPauseTimerOnRecognition(pause: Bool) {
        settings.setBool(pause, forKey: kGSettingsPauseTimerOnRecognitionKey)
        settings.synchronize()
    }
    
    class func setCoinsPerWordDonation(coins: Int) {
        settings.setInteger(coins, forKey: kGSettingsCoinsPerWordKey)
        settings.synchronize()
    }
    
    // animations
    
    /**
     Set chit icon animation maximum scale
     
     Used in development setting to ajust icons animations on level loading screen
     
     - Parameter scale: `Float` scale, f.e.: 1.2
     */
    class func setChitAnimationMaxScale(scale: Float) {
        settings.setFloat(scale, forKey: kGSettings_A_ChitMaxScale)
        settings.synchronize()
    }
    
    /**
     Set chit icon animation time to scale up
     
     Used in development setting to ajust icons animations on level loading screen
     
     - Parameter scale: `Float` scale, f.e.: 1.2
     */
    class func setChitAnimationTimeUp(scale: Float) {
        settings.setFloat(scale, forKey: kGSettings_A_ChitTimeUp)
        settings.synchronize()
    }
    
    /**
     Set chit icon animation time to scale down
     
     Used in development setting to ajust icons animations on level loading screen
     
     - Parameter scale: `Float` scale, f.e.: 1.2
     */
    class func setChitAnimationTimeDown(scale: Float) {
        settings.setFloat(scale, forKey: kGSettings_A_ChitTimeDown)
        settings.synchronize()
    }
    
}