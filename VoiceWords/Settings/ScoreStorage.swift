//
//  Settings.swift
//  VoiceWords
//
//  Created by Georg Romas on 11/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation

/// Wrapper for NSUserDefaults for user score, level, avatar and status name
class ScoreStorage {
    static private let kScoreTotalKey = "kScoreTotalKey"
    static private let kUserLevelKey = "kUserLevelKey"
    
    static private let settings = NSUserDefaults.standardUserDefaults()
    
    // MARK: Access
    
    /// Returns `Int` user total score
    class func userTotalScore() -> Int {
        return settings.integerForKey(kScoreTotalKey)
    }
    
    /// Returns `Int` user level (from 1 to 5)
    class func userLevel() -> Int {
        var level = settings.integerForKey(kUserLevelKey)
        if level == 0 { level = 1}
        if level > 5 { level = 5 }
        return level
    }
    
    /// Returns `Float` progress percentage for user level from 1 to 5
    ///
    /// Used as helper for progress indicator
    class func progressPercent() -> Float {
        let current = Float(userLevel()) - 1
        let target:Float = 4.0
        return current/target*100.0
    }
    
    /// Returns `UIImage` user avatar according to level
    class func currentAvatar() -> UIImage {
        let level = self.userLevel()
        return avatarForLevel(level)
    }
    
    /// Returns `UIImage` user avatar according to level used in watch extention
    class func currentWatchAvatar() -> UIImage {
        let level = self.userLevel()
        return avatarForLevel(level)
    }
    
    /// Returns `UIImage` user avatar for next level
    class func nextAvatar() -> UIImage {
        var level = self.userLevel()
        if level > 5 { level = 5 }
        
        return avatarForLevel(level)
    }
    
    /// Returns `String` user status name to level
    class func currentStatusName() -> String {
        return self.statusNameForLevel(self.userLevel())
    }
    
    /// Returns `UIImage` user status name for next level
    class func nextStatusName() -> String {
        var level = self.userLevel()
        if level > 5 { level = 5 }
        
        return self.statusNameForLevel(level)
    }
    
    /// Returns `String` status name converted from Integer (from 1 to 5)
    class private func statusNameForLevel(level: Int) -> String {
        switch level {
        case 1: return "Новичок"
        case 2: return "Продвинутый"
        case 3: return "Эксперт"
        case 4: return "Мастер"
        default: return "Эрудит"
        }
    }
    
    /// Returns `String` user status image name accordin to user level used in watch extention
    class func watchCurrentStatusImageName() -> String {
        let genderString =  Settings.userGender() == Settings.Gender.Female ? "female" : "male"
        return "w_img_status_\(genderString)_user\(self.userLevel())"
    }
    
    class private func avatarForLevel(level: Int) -> UIImage {
        let genderString =  Settings.userGender() == Settings.Gender.Female ? "female" : "male"
        let imageName = "img_status_\(genderString)_user\(level)"
        return UIImage(named: imageName)!
    }
    
    /// Returns `String` user avatar image name accordin to user level
    class func cellAvatarName() -> String {
        let genderString =  Settings.userGender() == Settings.Gender.Female ? "female" : "male"
        return "img_ava_\(genderString)_user\(self.userLevel())"
    }
    
    /// Returns `UIImage` user avatar according to level used in game player word cell
    class func cellAvatar() -> UIImage {
        return UIImage(named: self.cellAvatarName())!
    }
    
    // MARK: Change
    
    /// Increment total user score with new value
    class func incrementTotalScore(score: Int) {
        let current = userTotalScore()
        let result = current + score
        settings.setInteger(result, forKey: kScoreTotalKey)
        settings.synchronize()
    }
    
    /// Overide total user score with new value
    class func setTotalScore(score: Int) {
        settings.setInteger(score, forKey: kScoreTotalKey)
        settings.synchronize()
    }
    
    /// Save user level
    class func saveUserLevel(level: Int) {
        settings.setInteger(level, forKey: kUserLevelKey)
        settings.synchronize()
    }
}