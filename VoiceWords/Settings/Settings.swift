//
//  Settings.swift
//  VoiceWords
//
//  Created by Georg Romas on 11/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation

/**
 Wrapper for NSUserDefaults for common game settings:
 
 - Play sounds or not
 - Play music or not
 - Syntesize word with voice or not
 - Player gender male or female
 - Is tutorial complete or not
 - Is welcome screen was shown or not
 - Store default settings version number (used on app start in ApplicatinDelegate)
 - Show ad or not
 - Words database server version number
 */
class Settings {
    static let instance = Settings()
    
    /**
     
     User gender
     
     - **Unknown:** in most cases not used
     - **Male:** Male
     - **Female:** Female
     
     */
    enum Gender {
        case Unknown, Male, Female
        
        /// Create `Gender` from Integer
        static func fromInt(i: Int) -> Gender {
            switch i {
            case 1: return Male
            case 2: return Female
            default: return Unknown
            }
        }
    }
    
    static private let kSettingsSoundsKey = "kSettingsSoundsKey"
    static private let kSettingsMusicKey = "kSettingsMusicKey"
    static private let kSettingsVoiceKey = "kSettingsVoiceKey"
    static private let kSettingsGenderKey = "kSettingsGenderKey"
    static private let kSettingsTutorialCompleteKey = "kSettingsTutorialCompleteKey"
    static private let kSettingsWelcomeWasShownKey = "kSettingsWelcomeWasShownKey"
    static private let kInitialSetupWasCompleteKey = "kInitialSetupWasCompleteKey"
    static private let kAllowShowAdForRewardKey = "kAllowShowAdForRewardKey"
    static private let kLastWordsVersionKey = "kLastWordsVersionKey"
    static private let kDevelopmentModeKey = "kDevelopmentModeKey"
    
    static private let settings = NSUserDefaults.standardUserDefaults()
    
    // MARK: Access
    
    /*
    IMPORTANT:
    for sounds, music, voice stored 0 mean enabled
    this is for default true on instalation
    */
    
    /**
     Play sounds or not
     - Returns `Bool`
     
     - SeeAlso:
       - `isMusicEnabled()`
       - `isVoiceEnabled()`
       - `isVoiceDisabled()`
     */
    class func isSoundsEnabled() -> Bool {
        return !settings.boolForKey(kSettingsSoundsKey)
    }
    
    /**
     Play music or not
     - Returns `Bool`
     
     - SeeAlso:
     - `isSoundsEnabled()`
     - `isVoiceEnabled()`
     - `isVoiceDisabled()`
     */
    class func isMusicEnabled() -> Bool {
        return !settings.boolForKey(kSettingsMusicKey)
    }
    
    /**
     User wants use voice input or not
     - Returns `Bool`
     
     - SeeAlso:
     - `isSoundsEnabled()`
     - `isMusicEnabled()`
     - `isVoiceDisabled()`
     */
    class func isVoiceEnabled() -> Bool {
        return !settings.boolForKey(kSettingsVoiceKey)
    }
    
    /**
     User wants use voice input or not
     - Returns `Bool`
     
     - SeeAlso:
     - `isSoundsEnabled()`
     - `isMusicEnabled()`
     - `isVoiceEnabled()`
     */
    class func isVoiceDisabled() -> Bool {
        return !isVoiceEnabled()
    }
    
    /**
     Is tutorial complete or not
     - Returns `Bool`
     */
    class func isTutorialComplete() -> Bool {
        return settings.boolForKey(kSettingsTutorialCompleteKey)
    }
    
    /**
     Player gender male or female
     - Returns `Gender`
     */
    class func userGender() -> Gender {
        return Gender.fromInt(settings.integerForKey(kSettingsGenderKey))
    }
    
    /**
     Is welcome screen was shown or not
     - Returns `Bool`
     */
    class func isWelcomeShown() -> Bool {
        return settings.boolForKey(kSettingsWelcomeWasShownKey)
    }
    
    /**
     Check if current settings version number is not equal stored version
     - Returns `Bool`
     */
    class func isInitialiSettingUp(version: Int) -> Bool {
        return settings.boolForKey("\(kInitialSetupWasCompleteKey)_\(version)")
    }
    
    /**
     Show ads or not
     - Returns `Bool`
     */
    class func canShowAdForReward() -> Bool {
        return settings.boolForKey(kAllowShowAdForRewardKey)
    }
    
    /**
     Words database version number which is synced with remote server
     - Returns `Int`
     */
    class func currentWordsVersion() -> Int {
        return settings.integerForKey(kLastWordsVersionKey)
    }
    
    class func isDevelopmentMode() -> Bool {
        return settings.boolForKey(kDevelopmentModeKey)
    }
    
    
    // MARK: Change
    
    /// Toggle game sounds (like buttons touch)
    class func toggleSounds(isOn: Bool) {
        settings.setBool(!isOn, forKey: kSettingsSoundsKey)
        settings.synchronize()
    }
    
    /// Toggle game background music
    class func toggleMusic(isOn: Bool) {
        settings.setBool(!isOn, forKey: kSettingsMusicKey)
        settings.synchronize()
    }
    
    /// Toggle voice sintesizing
    class func toggleVoice(isOn: Bool) {
        settings.setBool(!isOn, forKey: kSettingsVoiceKey)
        settings.synchronize()
    }
    
    /**
     Save selected user gender
     - Parameter gender: `Gender` enum
     */
    class func setGender(gender: Gender) {
        settings.setInteger(gender.hashValue, forKey: kSettingsGenderKey)
        settings.synchronize()
    }
    
    /// Mark tutorial as comlete
    class func setTutoralComplete() {
        settings.setBool(true, forKey: kSettingsTutorialCompleteKey)
        settings.synchronize()
    }
    
    /// Mark tutorial as not comlete
    class func setTutoralNotComplete() {
        settings.setBool(false, forKey: kSettingsTutorialCompleteKey)
        settings.synchronize()
    }
    
    /**
     Mark welcome screen as shown
     
     Welcome screen should be shown only once on first game launch
     */
    class func setWelcomeShown() {
        settings.setBool(true, forKey: kSettingsWelcomeWasShownKey)
        settings.synchronize()
    }
    
    /// Save current settings version number
    class func setInitialSetupsComplete(version: Int) {
        settings.setBool(true, forKey: "\(kInitialSetupWasCompleteKey)_\(version)")
        settings.synchronize()
    }
    
    /// Allow show ad for reward
    class func allowShowAdForReward() {
        settings.setBool(true, forKey: kAllowShowAdForRewardKey)
    }
    
    /// Do allow show ad for reward
    class func doNotAllowShowAdForReward() {
        settings.setBool(false, forKey: kAllowShowAdForRewardKey)
    }
    
    /// Save current words database version number on remote server
    class func setCurrentWordsVersion(version: Int) {
        settings.setInteger(version, forKey: kLastWordsVersionKey)
    }
    
    class func enableDevelopmentMode() {
        settings.setBool(true, forKey: kDevelopmentModeKey)
    }
}