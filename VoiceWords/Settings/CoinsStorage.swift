//
//  Settings.swift
//  VoiceWords
//
//  Created by Georg Romas on 11/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation

/// Wrapper for NSUserDefaults for user coins
class CoinsStorage {    
    static private let kCoinsTotalKey = "kCoinsTotalKey"
    
    static private let settings = NSUserDefaults.standardUserDefaults()
    
    // MARK: Access
    
    /// Returns number of stored user coins
    class func userCoins() -> Int {
        let coins = settings.integerForKey(kCoinsTotalKey)
        return coins
    }
    
    // MARK: Change
    
    /// Increment current coins number with new value
    class func refill(coins: Int) {
        let current = userCoins()
        let result = current + coins
        settings.setInteger(result, forKey: kCoinsTotalKey)
        settings.synchronize()
    }
    
    /// Decrement current coins number with new value
    ///
    /// Can be used unsafe. It check to prevent coins go to negative number.
    class func withdraw(coins: Int) {
        let current = userCoins()
        let result = current - coins
        let safeResult = result < 0 ? 0 : result
        settings.setInteger(safeResult, forKey: kCoinsTotalKey)
        settings.synchronize()
    }
    
    /// Directly set number of coins 
    ///
    /// Warning: Overide current coins number with new
    class func setCoins(coins: Int) {
        settings.setInteger(coins, forKey: kCoinsTotalKey)
        settings.synchronize()
    }
}