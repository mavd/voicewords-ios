//
//  TutorialController.swift
//  VoiceWords
//
//  Created by Georg Romas on 03/03/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation
import AVFoundation

/// Controll tutorial flow step by step
class TutorialController {
    
    /**
     Tutorial steps
     
     - Init
     - AIStart
     - GameRuleWord
     - GameRuleTimer
     - RequsetMicrophone
     - MicrophoneAccessSuccess
     - TrySay
     - FirstWord
     - SecondWord
     - DonateCoins
     - UseHelp
     - PrepareWordForHelpDemonstrationAddTime
     - HelpAddTime
     - PrepareWordForHelpDemonstrationChangeWord
     - HelpChangeWord
     - PrepareWordForHelpDemonstrationSolveWord
     - HelpSolveWord
     - MarketInfo
     - AIThink
     - MarketOpenRequest
     - MarketClosed
     - Compliment
     - KeyboardHelp
     - KeyboardOpen
     - LastPlay
     - TutorialComplete
     - TimeRunOut
     */
    enum Step {
        case Init
        case AIStart
        case GameRuleWord
        case GameRuleTimer
        case RequsetMicrophone
        case MicrophoneAccessSuccess
        case TrySay
        case FirstWord
        case SecondWord
        case DonateCoins
        case UseHelp
        case PrepareWordForHelpDemonstrationAddTime
        case HelpAddTime
        case PrepareWordForHelpDemonstrationChangeWord
        case HelpChangeWord
        case PrepareWordForHelpDemonstrationSolveWord
        case HelpSolveWord
        case MarketInfo
        case AIThink
        case MarketOpenRequest
        case MarketClosed
        case Compliment
        case KeyboardHelp
        case KeyboardOpen
        case LastPlay
        case TutorialComplete
        case TimeRunOut
        
        /// Create Step from `Integer`
        static func fromInt(intValue: Int) -> Step {
            switch intValue {
            case 1: return .AIStart
            case 2: return .GameRuleWord
            case 3: return .GameRuleTimer
            case 4: return .RequsetMicrophone
            case 5: return .MicrophoneAccessSuccess
            case 6: return .TrySay
            case 7: return .FirstWord
            case 8: return .SecondWord
            case 9: return .DonateCoins
            case 10: return .UseHelp
            case 11: return .PrepareWordForHelpDemonstrationAddTime
            case 12: return .HelpAddTime
            case 13: return .PrepareWordForHelpDemonstrationChangeWord
            case 14: return .HelpChangeWord
            case 15: return .PrepareWordForHelpDemonstrationSolveWord
            case 16: return .HelpSolveWord
            case 17: return .MarketInfo
            case 18: return .AIThink
            case 19: return .MarketOpenRequest
            case 20: return .MarketClosed
            case 21: return .Compliment
            case 22: return .KeyboardHelp
            case 23: return .KeyboardOpen
            case 24: return .LastPlay
            case 25: return .TutorialComplete
            case 26: return .TimeRunOut
            default: return .Init
            }
        }
    }
    
    private var currentStep = Step.Init
    private var rememberStep = Step.Init
    private weak var tutorialView: TutorialView!
    private unowned var gameController: GameController
    private var screenController: MainCtrl!
    
    /// Required constructor
    init(gameController: GameController, screenController: MainCtrl, tutorialView: TutorialView) {
        self.gameController = gameController
        self.tutorialView = tutorialView
        self.screenController = screenController
        
        self.gameController.levelCompleteCallback = { isWin, levelScore, levelTimeInSeconds, solvedWords in
            //
        }
    }
    
    /// Start tutorial flow
    func start() {
        self.gameController.prepareGame()
        self.tutorialView.tapCallback = {
            self.nextStep()
        }
        self.gameController.timerLowCallback = {
            self.rememberStep = Step.fromInt(self.currentStep.hashValue)
            self.currentStep = .TimeRunOut
            self.blockNext = false
            self.nextStep()
        }
        self.nextStep()
    }
    
    private var microphoneNoAccessTimes = 0
    
    private var blockNext = false
    
    private func nextStep() {
        
        if self.blockNext { return }
        self.blockNext = true
        
        self.gameController.aiQuestionCompleteCallback = nil
        self.gameController.playerAnswerCompleteCallback = nil
        
        switch self.currentStep {
            
        case .Init:
            self.currentStep = .AIStart
            self.gameController.unpauseGame()
            
            self.gameController.levelTargetTime = 8
            
            self.gameController.aiQuestionCompleteCallback = { [unowned self] in
                delayCall(0.5) {
                    self.tutorialView.showStepWelcome()
                    self.blockNext = false
                }
            }
            self.gameController.aiSayWord()
            
        case .AIStart:
            self.currentStep = .GameRuleWord
            
            self.gameController.showStartLetter()
            
            self.tutorialView.showStepStartLetter()
            self.blockNext = false
            
        case .GameRuleWord:
            self.currentStep = .RequsetMicrophone
            self.tutorialView.showStepMicrophone()
            self.blockNext = false
            
        case .RequsetMicrophone:
            self.tutorialView.hideAll()
            AVAudioSession.sharedInstance().requestRecordPermission{ [unowned self] granted in
                if granted {
                    self.currentStep = .MicrophoneAccessSuccess
                } else {
                    self.microphoneNoAccessTimes += 1
                }
                
                if self.microphoneNoAccessTimes > 2 {
                    let message = "Для обучения необходим доступ к микрофону. Включите доступ в Настройках телефона > Микрофон > Словолюб."
                    let alert = UIAlertController(title: "Нет доступа к микрофону", message: message, preferredStyle: .Alert)
                    self.screenController.navigationController!.presentViewController(alert, animated: true, completion: nil)
                    
                    alert.addAction(UIAlertAction(title: "Закрыть", style: .Cancel) { a in
                        self.screenController.closeAction(nil)
                    })
                    return
                }
                
                self.blockNext = false
                self.nextStep()
            }
            
        case .MicrophoneAccessSuccess:
            self.currentStep = .TrySay
            self.tutorialView.showStepTryToSay()
            self.blockNext = false
            
        case .TrySay:
            self.tutorialView.hideAll()
            self.gameController.levelTargetTime = 20
            self.gameController.refillTimerInUserTurn(true)
            
            self.gameController.playerAnswerCompleteCallback = { [unowned self] in
                self.currentStep = .GameRuleTimer
                self.blockNext = false
                self.tutorialView.showStepGoodBoy()
                self.gameController.refillTimer()
            }
            
            self.gameController.playerTurn()
            
        case .FirstWord:
            self.currentStep = .GameRuleTimer
            self.tutorialView.showStepTimer()
            self.blockNext = false
            
        case .GameRuleTimer:
            self.currentStep = .SecondWord
            self.tutorialView.showStepTimer()
            self.blockNext = false
            
        case .SecondWord:
            self.currentStep = .DonateCoins
            self.tutorialView.hideAll()
            self.gameController.playerAnswerCompleteCallback = { [unowned self] in
                self.blockNext = false
                self.nextStep()
            }
            self.gameController.aiSayWord()
            
        case .DonateCoins:
            self.currentStep = .UseHelp
            self.blockNext = false
            let coinsDifference = 300 - CoinsStorage.userCoins()
            if coinsDifference > 0 {
                self.tutorialView.showStepDonate(coinsDifference)
                self.gameController.gameUIGroup!.coinsController.revenue(coinsDifference)
            } else {
                self.nextStep()
            }
            
        case .UseHelp:
            self.currentStep = .PrepareWordForHelpDemonstrationAddTime
            self.tutorialView.hideAll()
            self.gameController.aiQuestionCompleteCallback = { [unowned self] in
                self.tutorialView.showStepHelpInfo()
                self.blockNext = false
            }
            self.gameController.deckController.lockInputField()
            delayCall(1.0) {
                self.gameController.aiSayWord()
            }
            
        case .PrepareWordForHelpDemonstrationAddTime:
            self.gameController.levelTargetTime = 20
            self.gameController.refillTimerInUserTurn(false)
            self.gameController.showStartLetter()
            delayCall(0.5) {
                self.gameController.countDown = 5
                self.gameController.resumeTimer()
            }
            delayCall(1.0) {
                self.gameController.pauseTimer()
                self.tutorialView.showStepHelpInfoTimer()
                self.gameController.deckController.showDescriptionChitAddTime()
                self.currentStep = .HelpAddTime
                self.blockNext = false
                self.nextStep()
            }
            
        case .HelpAddTime:
            self.blockNext = true
            self.listen{ [unowned self] result in
                switch result {
                case .Some(let relevant) where self.gameController.isPhraseAddTime(relevant):
                    self.currentStep = .PrepareWordForHelpDemonstrationChangeWord
                    self.gameController.chitAddTime()
                    self.gameController.gameUIGroup!.coinsController.withdraw(GameSettings.chitCostInCoins(GameSettings.ChitID.Time))
                    self.tutorialView.hideAll()
                    delayCall(2.5) {
                        self.blockNext = false
                        self.nextStep()
                    }
                    
                default:
                    if let word = result {
                        self.gameController.deckController.showWord(word)
                        delayCall(0.7) {
                            if let word = self.gameController.startLetter {
                                self.gameController.deckController.showWord(word)
                            }
                        }
                    }
                    self.blockNext = false
                    self.nextStep()
                }
            }
            
        case .PrepareWordForHelpDemonstrationChangeWord:
            self.gameController.playerAnswerCompleteCallback = nil
            self.gameController.playerTurn()
            
            self.gameController.aiQuestionCompleteCallback = {[unowned self] in
                self.gameController.pauseTimer()
                self.currentStep = .HelpChangeWord
                self.tutorialView.showStepHelpInfoChangeWord()
                self.gameController.deckController.showDescriptionChitChangeWord()
                self.blockNext = false
                self.nextStep()
            }
            
        case .HelpChangeWord:
            self.gameController.aiQuestionCompleteCallback = { [unowned self] in
                self.currentStep = .PrepareWordForHelpDemonstrationSolveWord
                self.gameController.showStartLetter()
                self.blockNext = false
                self.nextStep()
            }
            
            self.listen{ [unowned self] result in
                switch result {
                case .Some(let relevant) where self.gameController.isPhraseChangeWord(relevant):
                    self.tutorialView.hideAll()
                    self.gameController.chitChangeWord()
                    self.gameController.gameUIGroup!.coinsController.withdraw(GameSettings.chitCostInCoins(GameSettings.ChitID.Change))
                default:
                    if let word = result {
                        self.gameController.deckController.showWord(word)
                        delayCall(0.7) {
                            if let word = self.gameController.startLetter {
                                self.gameController.deckController.showWord(word)
                            }
                        }
                    }
                    self.blockNext = false
                    self.nextStep()
                }
            }
            
        case .PrepareWordForHelpDemonstrationSolveWord:
            self.currentStep = .HelpSolveWord
            self.tutorialView.showStepHelpInfoSolveWord()
            self.gameController.deckController.showDescriptionChitSolveWord()
            self.gameController.deckController.unlockChitButtons()
            self.blockNext = false
            self.nextStep()
            
        case .HelpSolveWord:
            self.gameController.playerAnswerCompleteCallback = { [unowned self] in
                self.currentStep = .MarketInfo
                self.gameController.pauseTimer()
                delayCall(1.2) {
                    self.currentStep = .MarketInfo
                    self.blockNext = false
                    self.nextStep()
                }
            }
            
            self.listen{ [unowned self] result in
                switch result {
                case .Some(let relevant) where self.gameController.isPhraseSolveWord(relevant):
                    self.tutorialView.hideAll()
                    self.gameController.deckController.hideAllChitsDescriptions()
                    self.gameController.chitSolveWord()
                    self.gameController.gameUIGroup!.coinsController.withdraw(GameSettings.chitCostInCoins(GameSettings.ChitID.Solve))
                default:
                    if let word = result {
                        self.gameController.deckController.showWord(word)
                        delayCall(0.7) {
                            if let word =  self.gameController.startLetter {
                                self.gameController.deckController.showWord(word)
                            }
                        }
                    }
                    self.blockNext = false
                    self.nextStep()
                }
            }
            
        case .MarketInfo:
            self.currentStep = .AIThink
            self.tutorialView.showStepCoinsInfo()
            self.blockNext = false
            
        case .AIThink:
            self.tutorialView.hideAll()
            self.currentStep = .MarketOpenRequest
            self.gameController.pauseGame()
            self.gameController.aiSayWord()
            delayCall(1.5) {
                self.blockNext = false
                self.nextStep()
            }
            
        case .MarketOpenRequest:
            self.tutorialView.showStepMarket()
            
            self.screenController.closeMarketCallback = { [unowned self] in
                self.currentStep = .MarketClosed
                self.tutorialView.hideAll()
                self.blockNext = false
                self.nextStep()
            }
            
        case .MarketClosed:
            self.gameController.refillTimer()
            self.gameController.playerAnswerCompleteCallback = { [unowned self] in
                self.gameController.pauseTimer()
                self.currentStep = .Compliment
                self.blockNext = false
                self.nextStep()
            }
            self.gameController.aiQuestionCompleteCallback = { [unowned self] in
                self.gameController.playerTurn()
            }
            delayCall(1.0) {
                self.gameController.unpauseGame()
            }
            
        case .Compliment:
            self.currentStep = .KeyboardHelp
            self.tutorialView.showStepGoodGirl()
            self.blockNext = false
            
        case .KeyboardHelp:
            self.tutorialView.hideAll()
            self.gameController.aiSayWord()
            self.gameController.aiQuestionCompleteCallback = { [unowned self] in
                self.tutorialView.containerTapCallback = {
                    self.nextStep()
                }
                self.currentStep = .KeyboardOpen
                self.tutorialView.showStepKeyboard()
                self.blockNext = false
            }
            
        case .KeyboardOpen:
            self.tutorialView.containerTapCallback = nil
            self.tutorialView.hideAll()
            self.tutorialView.hidden = false
            self.screenController.deckController.unlockInputField()
            self.screenController.inputField.becomeFirstResponder()
            self.gameController.pauseTimer()
            self.gameController.aiQuestionCompleteCallback = { [unowned self] in
                self.currentStep = .LastPlay
                self.tutorialView.showStepKeyboardAndHelp()
                self.blockNext = false
            }
            self.gameController.refillTimer()
            delayCall(1.0) {
                self.gameController.playerTurn()
            }
            
        case .LastPlay:
            self.tutorialView.hideAll()
            self.tutorialView.hidden = true
            self.gameController.playerAnswerCompleteCallback = { [unowned self] in
                self.tutorialView.hidden = false
                self.currentStep = .TutorialComplete
                self.gameController.cancelGame()
                delayCall(1.0) {
                    self.tutorialView.showStepCongrats()
                    self.blockNext = false
                }
            }
            self.gameController.refillTimer()
            delayCall(1.0) {
                self.gameController.playerTurn()
            }
            
        case .TutorialComplete:
            Settings.setTutoralComplete()
            self.screenController.navigationController!.popViewControllerAnimated(true)

        case .TimeRunOut:
            self.tutorialView.hidden = false
            self.gameController.pauseTimer()
            self.gameController.refillTimerInUserTurn(false)
            self.currentStep = self.rememberStep
            self.tutorialView.showStepTimerLow()
            self.blockNext = false
        }
    }
    
    /// Main controller triger this callback when market screen opened
    func marketOpened() -> Bool {
        return self.currentStep != .MarketOpenRequest
    }
    
    /// When tutorial complete, change market in settings
    ///
    /// Main controller triger this callback when tutorial screen is closed
    func cancelTutorial() {
        Settings.setTutoralComplete()
    }
    
    private func listen(complete:(result: String?)->Void) {
        let recognizerCallback = SmartRecognizer.RecognizerCallback(
            startListen: {
                self.gameController.deckController.stateListen()
            },
            startRecognize: {
                self.gameController.deckController.stateRecognizing()
            },
            speechDetect: {
                self.gameController.deckController.stateSpeechDetected()
            },
            complete: { hypothesis, mostRelevant in
                log.info("recognizer listen callback: hypothesis: \(hypothesis) | mostRelevant: \(mostRelevant)")
                self.gameController.deckController.stateComplete()
                complete(result: mostRelevant)
        })
        self.gameController.recognizer.listen(recognizerCallback)
    }
}