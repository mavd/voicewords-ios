//
//  WatchGame.swift
//  VoiceWords
//
//  Created by Georg Romas on 25/03/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation
import GRDB

extension String {
    /// Returns `Character` at index
    ///
    /// O(n) should be used carefully
    func characterAtIndex(index: Int) -> Character? {
        var cur = 0
        for char in self.characters {
            if cur == index {
                return char
            }
            cur += 1
        }
        return nil
    }
}

class WatchGame {
    
    /**
     JFK
     
     mask: _ о _ _ _ _ а | variants: ["кошелка", "подачка", "воркута", "ночювка", "полочка", "долинка", "дочурка", "побудка", "горстка", "мозаика", "поклажа", "новелла", "ножовка", "воевода", "горчица", "подбора", "подвода", "горилла", "золушка", "солярка", "бороза", "золушка", "солярка", "борозда", "горница", "коммуна", "могилка", "вольера", "рогатка", "болячка", "фортуна", "мокрота", "похвала", "коронка", "родинка", "когорта", "копирка", "монашка", "побывка", "косынка", "котомка", "кочерга", "колючка", "сорочка", "водянка", "вотчина", "кобылка", "полтава", "соломка", "воронка", "сосенка", "кошечка", "розочка", "копилка", "мошкара", "попойка", "ромашка", "горелка", "поломка", "пошлина", "модница", "торпеда", "лозанна", "мошонка", "товарка", "подкоа", "мошонка", "товарка", "подкова", "возница", "ложбина", "соринка", "мошонка", "товарка", "подкова", "возница", "ложбина", "соринка", "дощечка", "розетка", "подмена", "потроха", "котлета", "поганка", "кончина", "получка", "воришка", "кожанка", "новизна", "поверка", "солянка", "козявка", "бойница", "коровка", "болонка", "пометка", "вологда", "лонка", "пометка", "вологда", "моторка", "лоханка", "молодка", "конница", "лошадка", "корочка", "колодка", "посылка", "полтина", "водичка", "волчица", "полнота", "покупка", "походка", "гондола", "горбуша", "добавка", "мокрица", "розница", "бородка", "головка", "мочалка", "сосиска", "кокарда", "голочалка", "сосиска", "кокарда", "голгофа", "коррида", "подлюга", "солочалка", "сосиска", "кокарда", "голгофа", "коррида", "подлюга", "солонка", "повадка", "лодочка", "косичка", "монетка", "полянка", "позюмка", "водочка", "госдума", "помойка", "лодыжка", "подагра", "порубка", "росинка", "ротонда", "горячка", "подмога", "воровка", "доплата", "кчка", "подмога", "воровка", "доплата", "короста", "косилка", "поделка", "кчка", "подмога", "воровка", "доплата", "короста", "косилка", "поделка", "команда", "ложечка", "повозка", "собачка", "толщина", "морщина", "тошнота", "ловушка", "коленка", "лопатка", "подошва", "догадка", "доброта", "коляска", "госпожа", "молитва", "конфета", "посадка", "полоска", "корзина", "формула", "повязка", "соседка", "колбаса", "копейка", "контора", "дорожка", "комната", "подушка", "коробка", "поездка", "полчаса", "коллега", "новинка", "подруга", "колонна", "хозяйка", "попытка"]
     */
    
    //    з а р е в о
    //    з а _ _ _ _
    //    з _ р _ _ _
    //    з _ _ е _ _
    //    з _ _ _ в _
    //    з _ _ _ _ о
    //    _ а р _ _ _
    //    _ а _ е _ _
    //    _ а _ _ в _
    //    _ а _ _ _ о
    //    _ _ р е _ _
    //    _ _ р _ в _
    //    _ _ р _ _ о
    //    _ _ _ е в _
    //    _ _ _ е _ о
    
    private func allMasksForWord(word: String) -> [String] {
        let string = word
        //var string = "зарево"
        let chars = string.characters
        var masks = [(String, Int)]()
        for currentStart in 0 ..< chars.count-2 {
            var mask = ""
            for i in 0 ... currentStart {
                if i == currentStart {
                    mask += "\(String(string.characterAtIndex(i)!)) "
                } else {
                    mask += "_ "
                }
            }
            
            masks.append((mask, chars.count - currentStart - 1))
        }
        
//        print(masks)
//        print("\n-------------------------\n")
        
        var finalMasks = [String]()
        
        for mask in masks {
            var finalMask = mask.0
            var offset = 0
            let tailLenght = mask.1
            for _ in 0 ..< tailLenght {
                for i in 0 ..< tailLenght {
                    let relativeIndex = chars.count - tailLenght + i
                    if i == offset {
                        finalMask += "\(String(string.characterAtIndex(relativeIndex)!)) "
                    } else {
                        finalMask += "_ "
                    }
                }
                
                finalMasks.append(finalMask.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()))
                finalMask = mask.0
                offset += 1
            }
        }
        
//        print("з а р е в о")
//        finalMasks.forEach { mask in
//            print(mask)
//        }
//        
//        print("\n-------------------------")
        
        return finalMasks

    }
    
    private func clearUsedMasks() {
        do {
            try DBHelper.db().inDatabase { db in
                try db.execute("UPDATE words SET mask = NULL")
            }
        } catch let e {
            log.error("FATAL: can not clear used masks \(e)")
            assert(true)
        }
    }
    
    private func randomWordWithoutMask() -> String {
        do {
            var expression = "SELECT * FROM words"
               expression += " WHERE mask IS NULL"
               expression += " AND length(word) >= 5"
               expression += " AND length(word) <= 7"
               expression += " ORDER BY random() LIMIT 1"
            
            var row: Row?
            
            try DBHelper.db().inDatabase { db in
                row = Row.fetchOne(db, expression)
            }
            
            if let row = row {
                return row.value(named: "word")
            } else {
                clearUsedMasks()
                return randomWordWithoutMask()
            }
        } catch let e {
            log.error("ERROR: AI can not get word from db \(e)")
            
            clearUsedMasks()
            return randomWordWithoutMask()
        }
    }
    
    /// Returns `String` mask for `String Array` of words matched to this mask
    ///
    /// **f.e.:** ("_ a _ _", ["кара", "тара", "сара"])
    func selectNotUsedMask() -> (String, [String]) {
        let word = self.randomWordWithoutMask()
        let allMasks = self.allMasksForWord(word)
        
        let preparedW = word.characters.map { ch in String(ch) }.joinWithSeparator(" ")
        print(preparedW)
        allMasks.forEach { m in
            print(m)
        }
        
        var mask = allMasks.randomItem()
        
        var wordVariants = [String]()
        
        func validateMask() {
            do {
                
                try DBHelper.db().inDatabase { db in
                    if Int.fetchOne(db, "SELECT COUNT(*) FROM words WHERE mask = ?", arguments: [mask]) > 0 {
                        log.warning("mask \(mask) already used. select next.")
                        mask = allMasks.randomItem()
                        validateMask()
                    } else {
                        
                        //catch all available answers for this mask
                        let likeMask = mask.stringByReplacingOccurrencesOfString(" ", withString: "")

                        for row in Row.fetch(db, "SELECT * FROM words WHERE mask IS NULL AND word LIKE '\(likeMask)'") {
                            let word: String = row.value(named: "word")
                            wordVariants.append(word)
                        }
                        log.info("mask: \(mask) | variants: \(wordVariants)")
                    }
                }
                
            } catch let e {
                log.error("ERROR: can not connect to db to validate selected mask \(e)")
                assert(true)
            }
        }
        validateMask()
        
        return (mask, wordVariants)
    }
    
    /// Save mask as used for selected word
    func saveUsedMask(word: String, mask: String) {
        do {
            try DBHelper.db().inDatabase { db in
                try db.execute("UPDATE words SET mask = ? WHERE word = ?", arguments: [mask, word])
            }
        } catch let e {
            log.error("ERROR: can not connect to db to save used mask \(e)")
            assert(true)
        }
    }
}