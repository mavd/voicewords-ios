//
//  GameValidator.swift
//  VoiceWords
//
//  Created by Georg Romas on 05/03/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation

/**
 
 Enumerate warning types for incorrect player word input or help usage during game
 
   - **DuplicatePlayer:** Word is used by player early
   - **DuplicateOpponent:** Word is used by AI early
   - **WrongPrefix:** Word start with incorrect character
   - **MoneyEnuff:** Can not activate help in reason of lack of money
   - **JustWrong:** Word is incorrect in reason of not exist in database mostly
 
 */

enum WarningType {
    /// Word is used by player early
    case DuplicatePlayer
    /// Word is used by AI early
    case DuplicateOpponent
    /// Word start with incorrect character
    case WrongPrefix
    /// Can not activate help in reason of lack of money
    case MoneyEnuff
    /// Word is incorrect in reason of not exist in database mostly
    case JustWrong;
    
    /// Create WarningType from integer
    static func fromInt(value: Int) -> WarningType {
        switch value {
        case 0: return .DuplicatePlayer
        case 1: return .DuplicateOpponent
        case 2: return .WrongPrefix
        case 3: return .MoneyEnuff
        default: return .JustWrong
        }
    }
    
    /// Human message representation of WarningType
    func message() -> String {
        switch self {
        case .DuplicatePlayer: return "Было использовано вами"
        case .DuplicateOpponent: return "Было использовано ИИ"
        case .JustWrong: return "Недопустимое слово"
        case .WrongPrefix: return "Не та буква"
        case .MoneyEnuff: return "Недостаточно монет"
        }
    }
}

/**
 This object store user input validation rules
 
 Used in Game controller
 */
class GameValidator {
    
    /// Current played level. Some rules depend on level number.
    private let level:Int!
    
    /// Required constructor
    required init(level: Int) {
        self.level = level
    }
    
    private var suffixesUsageCount: [String : Int] = ["а" : 0, "й" : 0]
    
    /**
     
     Increment suffixes usage for "а" and "й" characters
     
     - Parameter word: `String` AI word
     
     */
    func recordSuffixUsage(word: String) {
        if word.hasSuffix("а") { suffixesUsageCount["а"]! += 1 }
        if word.hasSuffix("й") { suffixesUsageCount["й"]! += 1 }
        
        print("restricted suffixes: [ \(self.suffixLimit().joinWithSeparator(", ")) ]")
    }
    
    /// ["а", "й"] suffixes limited for ai questions for different levels in game rules reason
    func suffixLimit() -> [String] {
        var limit = [String]()
        
        switch self.level {
        case 1:
            if suffixesUsageCount["а"] == 10 { limit.append("а") }
            if suffixesUsageCount["й"] == 1 { limit.append("й") }
        case 2:
            if suffixesUsageCount["а"] == 15 { limit.append("а") }
            if suffixesUsageCount["й"] == 2 { limit.append("й") }
        case 3:
            if suffixesUsageCount["а"] == 20 { limit.append("а") }
            if suffixesUsageCount["й"] == 2 { limit.append("й") }
        case 4:
            if suffixesUsageCount["а"] == 25 { limit.append("а") }
            if suffixesUsageCount["й"] == 3 { limit.append("й") }
        case 5:
            if suffixesUsageCount["а"] == 30 { limit.append("а") }
            if suffixesUsageCount["й"] == 3 { limit.append("й") }
        case 6:
            if suffixesUsageCount["а"] == 35 { limit.append("а") }
            if suffixesUsageCount["й"] == 4 { limit.append("й") }
        case 7:
            if suffixesUsageCount["а"] == 40 { limit.append("а") }
            if suffixesUsageCount["й"] == 4 { limit.append("й") }
        case 8:
            if suffixesUsageCount["а"] == 50 { limit.append("а") }
            if suffixesUsageCount["й"] == 5 { limit.append("й") }
        default: break
        }
        
        return limit
    }
    
    /// It is forbidden to use words started with "ь", "ъ", "ы"
    static let forbiddenSuffixes = ["ь", "ъ", "ы"]
    
    /**
     Next AI word should start from last player word, but not from "ь", "ъ" or "ы"
     
     - Parameter word: `Sring` player word
     */
    class func considerSuffixForNextWord(word: String?) -> String? {
        guard let word = word else {
            return nil
        }
        
        let index = word.endIndex.advancedBy(-1)
        var suffix: String? = word.substringFromIndex(index)
        
        if forbiddenSuffixes.contains(suffix!) {
            let index = word.endIndex.advancedBy(-1)
            suffix = considerSuffixForNextWord(word.substringToIndex(index))
        }
        
        return suffix
    }
    
}