//
//  GameController.swift
//  VoiceWords
//
//  Created by Georg Romas on 16/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation
import XCGLogger
import GRDB
import SVProgressHUD
import AVFoundation

/// Group for game UI
class GameUIGroup {
    /// Where all messages appear
    unowned var gameField: GameFeildView
    /// Coins indicator
    unowned var coinsController: CoinsController
    /// Score indicator
    unowned var scoreIndicator: ScoreIndicator
    /// Parent controller
    unowned var screen: MainCtrl
    
    /// Required constructor
    init(screen: MainCtrl, gameField: GameFeildView, coinsController: CoinsController, scoreIndicator: ScoreIndicator) {
        self.screen = screen
        self.gameField = gameField
        self.coinsController = coinsController
        self.scoreIndicator = scoreIndicator
    }
}

/// Where all game logic appear
class GameController: NSObject, UITextFieldDelegate {
    private let TAG = "GameController"
    
    /// Current played level number
    let level: Int!
    
    private var opponentTurn = true
    /// Is it AI turn?
    func isOpponentTurn() -> Bool {
        return self.opponentTurn
    }
    /// Is it player turn?
    func isPlayerTurn() -> Bool {
        return !self.opponentTurn
    }
    
    private var timerPaused = true
    /// Is word timer paused?
    func isTimerPaused() -> Bool {
        return self.timerPaused
    }
    /// Pause word timer
    func pauseTimer() {
        self.timerPaused = true
    }
    /// Resume word timer
    func resumeTimer() {
        self.timerPaused = false
    }
    
    private var gamePaused = true
    /// Is game timer paused?
    func isGamePaused() -> Bool {
        return self.gamePaused
    }
    /// Pause game timer
    func pauseGame() {
        self.gamePaused = true
    }
    /// Resume game timer
    func unpauseGame() {
        self.gamePaused = false
    }
    
    private let opponent = Opponent()
    
    private var playerWords = [String]()
    private var aiWords = [String]()
    
    /// Interface for screen UI
    var gameUIGroup: GameUIGroup?
    
    /// Voice recognizer object. Also accessed from tutorial controller
    let recognizer = SmartRecognizer()
    
    private let vocalizer: VocalizerProtocol!
    weak var deckController: InputDeckController!

    private var scoreCalculator: ScoreCalculator!
    private var gameValidator: GameValidator!
    
    private var timer = NSTimer()
    private var levelTime: Float = 0.0
    private var levelScore = 0
    
    /// Current turn start letter for player
    var startLetter: String?
    
    /// Callback for Main controller and tutorial controller
    var levelCompleteCallback: (isWin: Bool, levelScore: Int, levelTimeInSeconds: Float, solvedWords: Int) -> Void
    
    private var isVoiceMode = true
    private var isKeyboardMode: Bool {
        return !isVoiceMode
    }
    
    /// Timer interval for word in current level
    /// Each level can have different time limit for solve ford, f.e.: 30 sec on level 1, 20 sec on level 3.
    var levelTargetTime = 0
    
    /// Callback for tutorial game flow control
    var aiQuestionCompleteCallback: (()->Void)?
    /// Callback for tutorial game flow control
    var playerAnswerCompleteCallback: (()->Void)?
    /// Callback for tutorial game flow control
    var timerLowCallback: (()->Void)?
    
    /// Required constructor
    init(level: Int, gameUIGroup: GameUIGroup, deckController: InputDeckController, levelCompleteCallback: (isWin: Bool, levelScore: Int, levelTimeInSeconds: Float, solvedWords: Int) -> Void) {
        
        self.level = level
        self.gameUIGroup = gameUIGroup
        self.deckController = deckController
        self.levelCompleteCallback = levelCompleteCallback
        self.vocalizer = GameSettings.isVocalizerSiri() ? SmartVocalizer() : YandexVocalizer()

        super.init()

        self.subscribeToAppEvents()
        
        self.levelTargetTime = self.timeoutForLevel()
        self.scoreCalculator = ScoreCalculator(level: level, levelTime: self.levelTargetTime)
        self.gameValidator = GameValidator(level: level)
        
        self.setupGameLoopAndTimer()

        self.deckController.timerIndicator.emptyTimer()
        self.deckController.lockChitButtons()        
        self.deckController.updateWordsCount("\(self.playerWords.count)/\(self.wordsCountForLevel())")
        
        self.gameUIGroup?.screen.closeMarketCallback = self.marketClosed
        
        AVAudioSession.sharedInstance().requestRecordPermission{ [unowned self] granted in
            self.isVoiceMode = granted
        }
    }
    
    private func subscribeToAppEvents() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(GameController.applicationDidBecomeActiveNotification), name: UIApplicationDidBecomeActiveNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(GameController.applicationDidEnterBackgroundNotification), name: UIApplicationDidEnterBackgroundNotification, object: nil)
    }
    
    
    private var gameGoToBackgroundDate: NSDate?
    private var timerBackgroundCorrection: Float = 0
    /// Resume game when application become active
    func applicationDidBecomeActiveNotification() {
        if let startDate = self.gameGoToBackgroundDate {
            self.timerBackgroundCorrection = Float(NSDate().timeIntervalSinceDate(startDate))
            self.gameGoToBackgroundDate = nil
            log.warning("Game was in background \(self.timerBackgroundCorrection) seconds")
            
            if self.isVoiceMode && self.isPlayerTurn() {
                //restart player turn to enable voice recognizer
                self.playerTurn()
            }
        }
    }
    
    /// Pause game when application become inactive
    func applicationDidEnterBackgroundNotification() {
        self.timerBackgroundCorrection = 0
        self.vocalizer.cancel()
        if self.isPlayerTurn() {
            self.gameGoToBackgroundDate = NSDate()
            if self.isVoiceMode {
                self.recognizer.cancel()
            }
        }
    }
    
    /// Main controller toggle this method when keyboard presented
    func switchToVoiceMode() {
        AVAudioSession.sharedInstance().requestRecordPermission{ [unowned self] granted in
            if granted {
                self.isVoiceMode = true
                self.deckController.switchToAudioMode()
                
                if self.isPlayerTurn() {
                    //restart player turn to enable voice recognizer
                    self.playerTurn()
                }
            }
        }
    }
    
    /// Main controller toggle this method when keyboard hidden
    func switchToKeyboardMode() {
        self.isVoiceMode = false
        self.recognizer.cancel()
        self.vocalizer.cancel()
        self.deckController.switchToKeyboardNode()
        
        if self.isPlayerTurn() && self.isTimerPaused() {
            self.resumeTimer()
        }
    }
    
    /// Temporary fix for duplicate chit usage on quick multiple button taps
    var lockChitDuplicate = false
    private func canUseChit(type: GameSettings.ChitID) -> Bool {
        if self.isGamePaused() { return false }
        
        if self.isOpponentTurn() { return false }
        
        let chitCost = GameSettings.chitCostInCoins(type)
        let canPay = self.gameUIGroup!.coinsController.withdraw(chitCost)
        
        if !canPay {
            self.deckController.showWarning(WarningType.MoneyEnuff)
            self.delayedJob(1.0, pauseOnGamePause: false) {  [weak self] in self!.deckController.hideWarning() }
        }
        
        if self.lockChitDuplicate {
            return false
        }
        
        self.lockChitDuplicate = true
        self.delayedJob(1.0, pauseOnGamePause: false) {  [weak self] in self!.lockChitDuplicate = false }
        
        return canPay
    }
    
    /// Activate help Solve word
    ///
    /// Used from tutorial controller as well
    func chitSolveWord() {
        
        log.info("player use chit SOLVE WORD for \(self.aiWords.last)")
        
        GA.gaEvUseChit("solve", word: self.startLetter!)
        GA.gaEvUseChit("solve", time: self.countDown)
        
        self.recognizer.cancel()
        self.deckController.hideAllVoiceIndicators()
        
        var word: String? = nil
        let restrictedWords = self.playerWords + self.aiWords
        do {
            let joinedString = restrictedWords.map { str in "'\(str)'"}.joinWithSeparator(", ")
            var expression = " SELECT * FROM words WHERE word NOT IN (\(joinedString))"
            
            assert(self.startLetter != nil, "Start letter should not be empty")
            if let prefix = self.startLetter {
                expression += " AND word LIKE '\(prefix)%'"
            }
            
            expression += " ORDER BY random() LIMIT 1"
            
            try DBHelper.db().inDatabase { db in
                let row = Row.fetchOne(db, expression)!
                word = row.value(named: "word")
            }
        } catch let e {
            log.error("ERROR: AI can not solve word start with \(self.startLetter) for user \(e)")
            assert(true)
        }
        
        if let word = word {
            // This is not allowed for now because of delegate callbacks
            //self.vocalizer.textToVoice(word)
            self.delayedJob(0.3, pauseOnGamePause: false) {
                self.deckController.chitSolveWordPlayActivationAnimation()
                self.processPlayerInput(word)
            }
        } else {
            log.error("ERROR: AI can not solve word start with \(self.startLetter) for user")
            assert(true)
        }
    }
    
    
    /// Activate help Change word
    ///
    /// Used from tutorial controller as well
    func chitChangeWord() {
        log.info("player use chit CHANGE WORD for \(self.aiWords.last)")
        
        GA.gaEvUseChit("change", word: self.startLetter!)
        GA.gaEvUseChit("change", time: self.countDown)
        
        self.recognizer.cancel()
        self.deckController.hideAllVoiceIndicators()
        self.delayedJob(0.3, pauseOnGamePause: false) { [unowned self] in
            self.deckController.chitChangeWordPlayActivationAnimation()
            self.aiTurn(witgThinkDelay: false)
            self.deckController.clearText()
        }
    }
    
    
    /// Activate help Add time to timer
    ///
    /// Used from tutorial controller as well
    func chitAddTime() {
        log.info("player use chit ADD TIME for \(self.aiWords.last)")
        
        GA.gaEvUseChit("time", word: self.startLetter!)
        GA.gaEvUseChit("time", time: self.countDown)
        
        self.refillTimerInUserTurn(false)
        
        self.recognizer.cancel()
        self.delayedJob(0.3, pauseOnGamePause: false) { [unowned self] in
            
            self.deckController.chitAddTimePlayActivationAnimation()
            self.deckController.hideAllVoiceIndicators()
            self.deckController.clearText()
        }
        self.delayedJob(2.5, pauseOnGamePause: false) { [unowned self] in
            self.showStartLetter()
            
            if self.isVoiceMode {
                self.listen()
            }
        }
    }
    
    /// Prepare controller for new game
    ///
    /// Used from tutorial controller as well
    func prepareGame() {
        self.deckController.chitAddTimeCallback = { [weak self] in
            if self!.canUseChit(GameSettings.ChitID.Time) {
                self!.chitAddTime()
            } else {
                if self!.isPlayerTurn() { self!.playerTurn() }
            }
        }
        self.deckController.chitSolveWordCallback = { [weak self] in
            if self!.canUseChit(GameSettings.ChitID.Solve) {
                self!.chitSolveWord()
            } else {
                if self!.isPlayerTurn() { self!.playerTurn() }
            }
        }
        self.deckController.chitChangeWordCallback = { [weak self] in
            if self!.canUseChit(GameSettings.ChitID.Change) {
                self!.chitChangeWord()
            } else {
                if self!.isPlayerTurn() { self!.playerTurn() }
            }
        }
        self.deckController.lockChitButtons()
        self.deckController.timerIndicator.emptyTimer()
    }
    
    /// Start game loop
    func startGame() {
        self.prepareGame()
        
        if playerWords.count == 0 && aiWords.count == 0 {
            firstTurn()
            unpauseGame()
        }
        
        if isKeyboardMode {
            switchToKeyboardMode()
        }
        
        log.info("start level \(self.level)")
    }

    /// Main controller trigger this callback when user opened market screen
    func marketOpened() -> Bool {
        //ture mean should block market to open
        if self.isPlayerTurn() {
            return true
        } else {
            self.pauseGame()
            return false
        }
    }
    
    /// Main controller trigger this callback when user closed market screen
    func marketClosed() {
        self.unpauseGame()
        self.gameUIGroup!.coinsController.actualizeDisplayedData()
    }
    
    /**
    Stop all timers and regular actions
    Clear as more as possible when user close game
    */
    func cancelGame() {
        NSNotificationCenter.defaultCenter().removeObserver(self)
        self.gameUIGroup?.gameField.stopAllSounds()
        self.opponentTurn = true
        self.jobs = []
        self.timer.invalidate()
        self.deckController?.lockInputField()
        self.pauseGame()
        self.pauseTimer()
        self.recognizer.cancel()
        self.vocalizer.cancel()
    }
    
    private func completeGame(isWin win: Bool) {
        self.cancelGame()
        
        self.levelCompleteCallback(isWin: win, levelScore: self.levelScore, levelTimeInSeconds: Float(levelTime), solvedWords: self.playerWords.count)
    }
    
    private func setupGameLoopAndTimer() {
        self.timer = NSTimer.scheduledTimerWithTimeInterval(1.0/100.0, target: self, selector: #selector(GameController.updateLoop), userInfo: nil, repeats: true)
        self.deckController.timerIndicator.createIndicator(strokeEnd: 0.0)
    }
    
    /// Fullfill timer when word timer is not paused
    func refillTimerInUserTurn(resume: Bool = true) {
        self.pauseTimer()
        self.refillTimer()
        
        if resume { self.delayedJob(0.5, pauseOnGamePause: false) { [weak self] in self!.resumeTimer() } }
    }
    
    /// Game loop timer countdown
    var countDown:Float = -1.0
    /// Fullfill timer
    func refillTimer() {
        self.countDown = Float(self.levelTargetTime)
        self.scoreCalculator = ScoreCalculator(level: level, levelTime: self.levelTargetTime)
        self.updateTimerIndicator()
    }
    
    private enum CompletionType {
        case Won, Lose
    }
    private func isGameComplete() -> CompletionType? {
        if self.playerWords.count == self.wordsCountForLevel() {
            return .Won
        }
        
        if self.countDown <= 0 {
            return .Lose
        }
        
        return nil
    }
    
    
    /*
    Put delayed closures into level main loop
    * * * * 2 >
    */
    private var commonLoopTime = 0.0
    
    private class Job {
        let time: Double!
        let job: (()->Void)!
        let pauseOnGamePause: Bool!
        var complete = false
        
        init(time: Double, pauseOnGamePause: Bool, job: (()->Void)) {
            self.time = time
            self.job = job
            self.pauseOnGamePause = pauseOnGamePause
        }
    }
    private var jobs = [Job]()
    private func delayedJob(delay: Double, pauseOnGamePause: Bool, job: ()->Void) {
        self.jobs.append(Job(time: self.commonLoopTime + (delay), pauseOnGamePause: pauseOnGamePause, job: job))
    }
    
    /* * * * * 2 < */
    
    /// Update timer UI element with current cowntdown value
    func updateTimerIndicator() {
        self.deckController.timerIndicator.updateProgress(self.countDown, totalTime: self.levelTargetTime, level: self.level)
    }
    
    private let hundredthOfAsecond:Float =  1.0/100.0
    
    /// Main game timer loop
    @objc func updateLoop() {
        
        self.commonLoopTime += Double(self.timerBackgroundCorrection)
        self.commonLoopTime += Double(hundredthOfAsecond)
        
        // this is timer initialisation
        if self.countDown < 0 {
            self.countDown = Float(self.levelTargetTime)
        }
        
        if self.countDown < 3 { self.timerLowCallback?() }
        
        //print(String(format: "%f", self.countDown))
        if !self.isGamePaused() {
            self.levelTime += self.timerBackgroundCorrection
            self.levelTime += hundredthOfAsecond
        }
        
        if !self.isTimerPaused() {
            self.countDown -= self.timerBackgroundCorrection
            self.countDown -= hundredthOfAsecond
            self.updateTimerIndicator()
        }
        
        self.deckController.checkAnimations(self.countDown, levelTime: Float(self.levelTargetTime), isOpponentTurn: self.isOpponentTurn())
        
        if let complete = self.isGameComplete() {
            switch complete {
            case .Lose: self.completeGame(isWin: false)
            case .Won: self.completeGame(isWin: true)
            }
            return
        }
        
        self.jobs.forEach { job in
            if job.pauseOnGamePause! {
                if self.isGamePaused() { return }
            }
            if !job.complete && job.time <= self.commonLoopTime {
                job.complete = true
                job.job()
            }
        }
        if let firstJob = self.jobs.first {
            if firstJob.complete {
                self.jobs.removeFirst()
            }
        }
        
        //Apply correction only once
        if self.timerBackgroundCorrection > 0 {
            self.timerBackgroundCorrection = 0
        }
    }
    
    private func firstTurn() {
        log.info("first turn")
        
        self.aiSayWord()
    }
    
    private func listen() {
        if Settings.isVoiceDisabled() { return }
        
        func startTimer() {
            self.opponentTurn = false
            self.resumeTimer()
        }
        
        let recognizerCallback = SmartRecognizer.RecognizerCallback(
            startListen: {
                self.deckController.stateListen()
                startTimer()
            },
            startRecognize: {
                self.deckController.stateRecognizing()
            },
            speechDetect: {
                if GameSettings.pauseTimerOnRecognition() {
                    self.pauseTimer()
                }
                
                self.deckController.stateSpeechDetected()
            },
            complete: { hypothesis, mostRelevant in
                log.info("recognizer listen callback: hypothesis: \(hypothesis) | mostRelevant: \(mostRelevant)")
                self.deckController.stateComplete()
                if let relevant = mostRelevant {
                    self.processPlayerInput(relevant)
                } else {
                    self.resumeTimer()
                }
        })
        
        self.recognizer.listen(recognizerCallback)
        log.info("start listen player")
    }
    
    /// Show current start letter in input player field
    func showStartLetter() {
        if let letter = GameValidator.considerSuffixForNextWord(self.aiWords.last) {
            
            self.startLetter = letter.lowercaseString
            self.deckController.showWord(self.startLetter!)
            
            log.info("player should start with \(self.startLetter)")
            
        } else {
            log.error("this is CRITICAL. can not determine suffix for player next turn.")
            assert(true)
        }
    }
    
    /// Run into player turn
    func playerTurn() {
        log.info("player turn")

        if self.isGameComplete() != nil {
            log.info("game is complete")
            return
        }
        
        self.deckController.unlockInputField()
        self.deckController.unlockChitButtons()
        
        self.showStartLetter()
        
        if self.isVoiceMode {
            self.listen()
        } else {
            self.opponentTurn = false
            self.resumeTimer()
        }
    }
    
    /// Run into AI turn
    ///
    /// If thinkDelay set to `false`, AI should create new word without delay
    private func aiTurn(witgThinkDelay delay: Bool = true) {
        self.deckController.lockChitButtons()
        
        if self.isGameComplete() != nil {
            log.info("game is complete")
            return
        }
        
        log.info("ai turn")
        
        self.opponentTurn = true

        self.pauseTimer()
        
        if self.isVoiceMode {
            self.deckController.stateIdle()
        }
        
        if delay {
            self.aiThink {
                self.aiSayWord()
            }
        } else {
            self.aiSayWord()
        }
    }
    
    private func aiThink(complite: ()->Void) {
        self.delayedJob(2.0, pauseOnGamePause: false) { complite() }
    }

    /**
     
     Select next word and perform AI turn
     
     **Note:**
     If there are no words in database that meets all conditions,
     for example words with lenght < 4 all used, this condition will be ommited.
     
     */
    func aiSayWord() {
        
        let wordMaxLength = wordLengthForLevel()
        
        let startWith = GameValidator.considerSuffixForNextWord(self.playerWords.last)
        
        log.info("AI should start with \(startWith)")
        
        var preparedWord = opponent.prepareWordWithCondition(startWith, maxLenght: wordMaxLength, restrictedWords:self.playerWords, restrictedSuffixes: self.gameValidator.suffixLimit())
        if preparedWord == nil {
            log.warning("ai can not find word start with and max length\(wordMaxLength)")
            preparedWord = opponent.prepareWordWithCondition(startWith, maxLenght: nil, restrictedWords:self.playerWords, restrictedSuffixes: self.gameValidator.suffixLimit())
        }
        
        func processAIQuestion (word: String?) {
            if let tutorialCallback = self.aiQuestionCompleteCallback {
                tutorialCallback()
            } else {
                self.opponentTurn = false
                self.refillTimerInUserTurn(false)
                self.delayedJob(0.5, pauseOnGamePause: false) { [weak self] in self!.playerTurn() }
            }
        }

        if let word = preparedWord {
            log.info("AI prepare \(word)")
            
            self.gameValidator.recordSuffixUsage(word)
            
            self.aiWords.append(word)

            let cell = self.gameUIGroup!.gameField.pushWordFromOpponent(word)
            let delay = [2.0, 2.5, 3.0, 3.5, 4.0].randomItem()
            
            self.delayedJob(delay, pauseOnGamePause: true) { [weak self] in
                if Settings.isSoundsEnabled() {
                    soundsBoss.stopRobotPrepared()
                    self!.vocalizer.textToVoice(word,
                        complete: {isSuccess in processAIQuestion(word) },
                        speechStart: {
                            log.info("AI say \(word)")
                            cell.playAIAnimation()
                    })
                } else {
                    log.info("AI say \(word)")
                    cell.playAIAnimation()
                    processAIQuestion(word)
                    self!.delayedJob(0.3, pauseOnGamePause: false) {
                        soundsBoss.stopRobotPrepared()
                    }
                }
            }
            
        } else {
            log.error("this is CRITICAL event. AI have no not used words")
            
            SVProgressHUD.setMinimumDismissTimeInterval(6)
            SVProgressHUD.setDefaultStyle(.Dark)
            SVProgressHUD.setDefaultMaskType(.Gradient)
            SVProgressHUD.showErrorWithStatus("Искуственный интелект не может поборать слово для игры. Пожалуйста, обратитесь в поддержку.\nПриносим извинения.")
            self.gameUIGroup?.screen.closeAction(nil)
        }
    }
    
    private class func userInputInvalid(word: String, startLetter: String, playerWords: [String], aiWords: [String]) -> WarningType? {
        
        if !word.lowercaseString.hasPrefix(startLetter) {
            log.info("player word \(word) not start with \(startLetter)")
            return WarningType.WrongPrefix
        }
        
        if playerWords.contains(word) {
            log.info("player word \(word) exist in player previous words \(playerWords)")
            return WarningType.DuplicatePlayer
        }
        
        if aiWords.contains(word) {
            log.info("player word \(word) exist in ai previous words \(aiWords)")
            return WarningType.DuplicateOpponent
        }
        
        /*
        Check word exist in local database
        */
        do {
            
            var count = 0
            try DBHelper.db().inDatabase { db in
                count = Int.fetchOne(db, "SELECT COUNT(*) FROM words WHERE word = ?", arguments: [word])!
            }
            
            if count == 0 {
                log.info("player word \(word) not found in local words db")
                return WarningType.JustWrong
            }
            
        } catch let e {
            print("ERROR: AI can not get word from db \(e)")
            return nil //is valid
        }
        
        return nil //is valid
    }
    
    /// Current game phase indicator
    func isPhraseAddTime(word: String) -> Bool {
        let trimmedString = word.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet()
        ).lowercaseString
        return ["добавить время", "сменить время", "обновить время", "сбросить время", "сбросить таймер", "сбросить часы", "обнулить часы", "обнулить таймер", "обновить таймер"].contains(trimmedString)
    }
    
    /// Current game phase indicator
    func isPhraseChangeWord(word: String) -> Bool {
        let trimmedString = word.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet()
        ).lowercaseString
        return ["сменить слово", "сменить слова", "смени слово", "смени слова", "заменить слово", "заменить слова", "замени слово", "замени слова", "другое слова", "другое слово", "разрули ситуацию", "разрули ситуация", "поменять слово", "поменять слова"].contains(trimmedString)
    }
    
    /// Current game phase indicator
    func isPhraseSolveWord(word: String) -> Bool {
        let trimmedString = word.stringByTrimmingCharactersInSet(
            NSCharacterSet.whitespaceAndNewlineCharacterSet()
        ).lowercaseString
        return ["подобрать слово", "подобрать слова", "решить слова", "решить слово", "найти слово", "найти слово"].contains(trimmedString)
    }
    
    /// Current game phase indicator
    func isChitCommands(word: String) -> Bool {
        if self.isPhraseChangeWord(word)  {
            if self.canUseChit(GameSettings.ChitID.Change) {
                self.chitChangeWord()
            } else {
                if self.isPlayerTurn() { self.playerTurn() }
            }
            return true
        }
        
        if self.isPhraseSolveWord(word)   {
            if self.canUseChit(GameSettings.ChitID.Solve) {
                self.chitSolveWord()
            } else {
                if self.isPlayerTurn() { self.playerTurn() }
            }
            return true
        }
        
        if self.isPhraseAddTime(word)   {
            if self.canUseChit(GameSettings.ChitID.Time) {
                self.chitAddTime()
            } else {
                if self.isPlayerTurn() { self.playerTurn() }
            }
            return true
        }
        
        return false
    }
    
    /// Process player word input and run AI turn if word is valid
    func processPlayerInput(word: String) {
        self.deckController.showWord(word)
        
        let word = word.lowercaseString
            .stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            .stringByReplacingOccurrencesOfString("ё", withString: "е", options: [NSStringCompareOptions.CaseInsensitiveSearch], range: nil)
        
        let delay = self.isVoiceMode ? 0.7 : 0.0
        
        if self.isChitCommands(word) {
            return
        }

        let score = self.scoreCalculator.measureWordReserchValue(self.countDown, isKeyboardMode: self.isKeyboardMode)
        log.info("player input is \(word). answered with \(score) points process to validate...")
        
        if let errorType = GameController.userInputInvalid(word, startLetter: self.startLetter!.lowercaseString, playerWords: self.playerWords, aiWords: self.aiWords) {
            
            log.info("player input is invalid. \(errorType)")
            GA.gaWorngWord(word)           
            
            self.deckController.showWarning(errorType)
            
            if Settings.isMusicEnabled() { soundsBoss.playOnceErrorEffect() }

            if self.isVoiceMode {
                self.delayedJob(delay, pauseOnGamePause: true) { [weak self] in
                    self!.deckController.showWord(self!.startLetter!)
                    self!.deckController.hideWarning()
                    self!.playerTurn()
                }
            } else {
                self.playerTurn()
            }
            
        } else {
            
            self.delayedJob(delay, pauseOnGamePause: true) { [weak self] in
                self!.deckController.clearText()
                
                self!.levelScore += score
                self!.deckController.showScoreAnimation(score)
                delayCall(1.3) { self!.gameUIGroup!.scoreIndicator.scoreUP(score) }
                
                self!.playerWords.append(word)
                self!.gameUIGroup!.gameField.pushWordFromPlayer(word)
                
                self!.deckController.timerIndicator.emptyTimer()
                
                self!.deckController.updateWordsCount("\(self!.playerWords.count)/\(self!.wordsCountForLevel())")
                
                if let tutorialCallback = self!.playerAnswerCompleteCallback {
                    tutorialCallback()
                } else {
                    self!.aiTurn()
                }
            }
            
        }
    }
    
    private func timeoutForLevel() -> Int {
        return GameSettings.gameIntervalForLevel(level)
    }
    
    private  func wordsCountForLevel() -> Int {
        let c = GameSettings.wordsCountForLevel(level)
        return c
    }
    
    private  func wordLengthForLevel() -> Int? {
        switch level {
        case 1: return 4
        case 2: return 5
        case 3: return 6
        case 4: return 7
        case 5: return 8
        case 6: return 9
        default: return nil
        }
    }
    
    //MARK: UITextFieldDelegate
    /// Called when 'return' key pressed. return NO to ignore.
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if textField.text == nil {
            textField.text = ""
        }
      
        let word = textField.text!
        
        if word.characters.count == 0 {
            return false
        }
        
        if self.isPlayerTurn() {
            self.processPlayerInput(word)
        }
        
        return false
    }
    
    /// Called when text field changed
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        self.deckController.hideWarning()
        return true
    }
    
    private class ScoreCalculator {
        private let levelTime: Int!
        private let level: Int!
        
        private var thinkSpeed: Float = 0
        
        init(level: Int, levelTime: Int) {
            self.level = level > 0 ? level : 1
            self.levelTime = levelTime
        }
        
        private var wordScore = 0
        func lastScore() -> Int {
            return self.wordScore
        }
        
        func measureWordReserchValue(currentCountDown: Float, isKeyboardMode: Bool) -> Int {
            return ScoreConverter.scoreFromCountDown(currentCountDown, totalTime: self.levelTime, level: self.level)
        }
    }
    
}