//
//  ScoreConverter.swift
//  VoiceWords
//
//  Created by Georg Romas on 28/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation

/// Helper to convert score from current timer with certain conditions
class ScoreConverter {
    /// Returns `Int` score based on current timer countdown, total time per word and current played level number 
    class func scoreFromCountDown(countDown: Float, totalTime: Int, level: Int) -> Int {
        return Int(countDown * Float(level * 10))
    }
}