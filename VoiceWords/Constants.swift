//
//  Constants.swift
//  VoiceWords
//
//  Created by Georg Romas on 10/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation

/// Strings constants mostly for autocompletion and safety
///
/// - Starts with `Screen` mean storyboard identifier
/// - Starts with `Font` mean font name
/// - Starts with `PurchasePack` mean in-app purchase identifier
/// - Starts with `Leaderboard` mean leaderboard identifier
struct Constants {
    // MARK: Storyboard controllers identifiers
    static let ScreenWelcome = "Welcome"
    static let ScreenInfo = "InfoController"
    static let ScreenMarket = "MarketController"
    static let ScreenGame = "GameScreen"
    static let ScreenMenu = "MenuScreen"
    static let ScreenLevelComplete = "LevelCompleteScreen"
    static let ScreenLevelLoading = "LevelLoadingScreen"
    static let ScreenLevels = "LevelsScreen"
    static let ScreenGameComplete = "GameCompleteScreen"
    
    // MARK: Custom font names
    static let Font_OpenSans_Regular = "OpenSans"
    static let Font_OpenSans_Semibold = "OpenSans-Semibold"
    static let Font_OpenSans_Bold = "OpenSans-Bold"
    static let Font_OpenSans_ExtraBold = "OpenSans-Extrabold"

    // MARK: Leaderboards identifiers
    static let LeaderboardIDGeneral = "com.spaceshipapps.a.VoiceWords.general"
    
    // MARK: In-app purchase identifiers
    static let PurchasePack1 = "com.spaceshipapps.a.VoiceWords.pack.1"
    static let PurchasePack2 = "com.spaceshipapps.a.VoiceWords.pack.2"
    static let PurchasePack3 = "com.spaceshipapps.a.VoiceWords.pack.3"
    static let PurchasePack4 = "com.spaceshipapps.a.VoiceWords.pack.4"
    static let PurchasePack5 = "com.spaceshipapps.a.VoiceWords.pack.5"
    static let PurchasePack6 = "com.spaceshipapps.a.VoiceWords.pack.6"
    
    // MARK: Notifications events
    static let PurchaseCompleteEvent = "com.VoiceWords.event.purchasecomplete"
}