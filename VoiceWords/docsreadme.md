# VoiceWords Game

### Release publishing

Search through the project for all `TODO:` marks and follow instructions.

General idea is to check database version, hide development settings, switch in-app purchase into production, etc.

### Developmetn environment

#### Git

After release 1.0.0 repository swithced to Git flow with default configuration.
All changes should be implemented throug feature branches and merged into development.

#### Dependencies

Cocoapods used as general dependencies manager

- SVProgressHUD [Progress and messaging HUD overlay]
- SnapKit [Handy autolayout managment from code]
- XCGLogger [Logging]
- YandexSpeechKit [Voice to text and back]
- Google/Analytics 
- GRDB [Lightweight sqlite database access and orm]

Part of third party lybraries iported directly as sources into `Thirdparty` directory

- [Alamofire](https://github.com/Alamofire/Alamofire)
- Apple Reachability helper overriden in swift 2.2 by Ashley Mills
- AudioPlayer helper [Copyright Recisio](http://www.recisio.com)
- Appodeal (Ads provider framework, library and assets) [integrated follow instructions](http://www.appodeal.ru/sdk/documentation?utf8=%E2%9C%93&framework=21&integration=1&ads_type%5B%5D=6&to_generate=1)

### General flow

Initial view controller is `LaunchCtrl`. It present developers logos and decides which controller should be open next.
If `WelcomeCtrl` (with short game instructions) never presented (Check record in user defaults) 
it will be open, in other case `MenuCtrl` will be presented.

`MenuCtrl` check on each *appeare* event whether tutorial should be launched.

### The Hard parts

#### Game

MainCtrl just a container for UI. Game logic handled by `GameController` object which is stored in MainCtrl as property. 
MainCtrl triger callbacks on game controller when required (buttons actions, close game screen, etc).

The hard parts:

- Custom game loop. Scheduled with NSTimer in main thread with interval *1.0/100.0* of second.
Interval value picked up with compromise of cpu usage. F.e.: 1/500 make cpu load up to 80%.
- GameController.Job class used to perform any task with delay in game loop. 
This is adds complexity, but allow pause such tasks when game paused.
- Voice synthesizing and sounds overlaping.
- Voice recognition and sounds overlaping.


#### Tutorial

MainCtrl also have property `TutorialController`. Tutorial controller have access to game controller in reason to
manually controll game flow step by step.

Tutorial controller logic is just a waterflow throug each case in `TutorialController.Step` enum.

The hard parts are complex relations between tutorial, main controller, game controller and voice recognizer.
Tutorial should know too much about each component and make a lot of assumptions.

#### Voice

[Yandex Speech Kit](https://tech.yandex.ru/speechkit) used to process voice recognision.

Yandex Vocalizer have random freezes and was replaced by AVSpeechSynthesizer (Siri). 
In that case ability to chage voice is lost (Yandex SDK ships with 2 male and 2 femail voices).

#### In-app purchases

In last update purchase for disable ads is commented, but reserved for future updates. 
If ads model changed, it can be uncommented back. 

#### Database synchronization

General flow: 
Send request to server, compare words database version number with stored in user preferences, if not match – prefrom wirds file uploading, parsing and update words.

The hard parts:

Database structure not optimal for such synchronization. Each word should be verified during synchronization process. 
In that case action to save or update words into databse covers most of the time.

`Menu screen` try to accomplish multiple tasks every time it opens: Sync Game center, Sync words database, launch tutorial if it never launched or if user launched it manually from settings, and so on. 
It is possible to reach conflict like "Open Game Center  authentication dialog when tutorial started". This screen try to resolve all conflicts like described.


#### Game Center synchronization 

The hard part is game chenter authorisation is too intrusive. 
On other side, if user reject GC authentication more than three times, application can not initiate authentication process anymore.

#### Developmetn settings

Development settings allow ajust application behavior during developmetn and testing. 

Access it from `Game Settings` screen bottom button. **Should be hidden on release.**