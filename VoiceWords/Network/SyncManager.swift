//
//  SyncManager.swift
//  VoiceWords
//
//  Created by Georg Romas on 27/04/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation
import XCGLogger

/**
 
 Load new database from server
 
 If new database version available, perform download file action, 
 parse content of this file and delegate task to store words to `DBHelper.updateWordsFromText(_:)`
 
 In reason default words databse exist, fail silently
 
 */
class SyncManager {
    
    /// Server base url
    //static let apiBase = "http://www.alpacadev.com/" // development
    static let apiBase = "http://vw.mobissa.ru/web/"
    
    /**
     
     Update words database if new version vailable. 
     Executed in *background*, but **completion** closure run in **main thread**
     
     In reason default words databse exist, fail silently
     
     - Parameter complete: Closure to execute on completion in main thread
     
     */
    class func syncWords(processing processCallback:()->Void, complete:()->Void) {
        //let checkVersionUrl = apiBase + "words/web/getFile" // development
        let checkVersionUrl = apiBase + "getFile"
        request(.GET, checkVersionUrl)
            .responseJSON() { response in
                log.info("sync words response received with code: \(response.response?.statusCode)")
                if let resp = response.result.value as? [String: AnyObject] {

                    if  let id = resp["id"] as? Int,
                        let url = resp["url"] as? String
                    {
                        print("current words version", Settings.currentWordsVersion(), "server words version", id)
                        if Settings.currentWordsVersion() == id {
                            complete()
                            return
                        }
                        
                        processCallback()
                        //let fileUrl = apiBase + "words/web/uploads/files/" + url // development
                        let fileUrl = apiBase + "uploads/files/" + url
                        
                        future {
                            //let data = NSData(contentsOfURL: NSURL(string: fileUrl)!)
                            if let stringData = try? NSString(contentsOfURL: NSURL(string: fileUrl)!, encoding: NSUTF8StringEncoding) as String {
                                DBHelper.updateWordsFromText(stringData)
                            }
                            ui {
                                Settings.setCurrentWordsVersion(id)
                                complete()
                            }
                        }
                    }
                } else {
                    ui { complete() }
                }
        }
    }
}