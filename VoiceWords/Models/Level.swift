//
//  Level.swift
//  VoiceWords
//
//  Created by Georg Romas on 12/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation

/**
 
 Level object representation. 
 Mostly for levels list screen.
 
 Holds level state (locked/unlocked), target words to complete and count of solved words in last game.
 
 */
class Level {
    /// Level identificator
    let id: Int!
    /// Level state indicator
    var unlocked = false
    /// Number of solved words in last game
    var complete = 0
    /// Target number of words for current level (30 is default, but can be changed for each level during object initialisation)
    var total = 30
    
    /// Default constructor
    init(id: Int, total: Int, complete: Int, unlocked: Bool = false) {
        self.id = id
        self.unlocked = unlocked
        self.total = total
        self.complete = complete
    }
}