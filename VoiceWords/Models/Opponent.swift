//
//  Opponent.swift
//  VoiceWords
//
//  Created by Georg Romas on 16/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation
import GRDB
import XCGLogger

/**
 Hold logic for AI words
 */
class Opponent {
   
    // Count attempts to find word for new turn
    private var tryCount = 0
    
    /**
     Select next word for AI turn based on certain conditions
     
     - Parameter startWith: `String` result word should start with
     - Parameter maxLenght: `Integer` result word maximum lenght
     - Parameter restrictedWords: `Array of Strings` result word should not starts with
     - Parameter restrictedSuffixes: `Array of Strings` result word should not ends on
     - Returns the `String` word for next AI turn
     */
    func prepareWordWithCondition(startWith: String?, maxLenght: Int?, restrictedWords:[String], restrictedSuffixes:[String]) -> String? {
        var word: String? = nil
        
        tryCount += 1
        print("------------------- - - - - - prepareWordWithCondition", tryCount)
        if tryCount > 10 { return nil }
        
        do {
            
            var expression = "SELECT * FROM words WHERE ai_used_at IS NULL"

            let joinedString = restrictedWords.map { str in "'\(str)'"}.joinWithSeparator(", ")
            expression += " AND word NOT IN (\(joinedString))"
            
            if let prefix = startWith {
                expression += " AND word LIKE '\(prefix)%'"
            }
            
            if let lenghtLimit = maxLenght {
                expression += " AND length(word) <= \(lenghtLimit)"
            }
            
            restrictedSuffixes.forEach { suffix in
                expression += " AND word NOT LIKE '%\(suffix)'"
            }

            expression += " ORDER BY random() LIMIT 1"
            
            log.info(expression)
            
            try DBHelper.db().inDatabase { db in
                
                // debug
                //if let row = Row.fetchOne(db, "SELECT * FROM words ORDER BY random() LIMIT 1") {
                //    log.info("\(row)")
                //}
                
                if let row = Row.fetchOne(db, expression) {
                    log.info("\(row)")
                    word = row.value(named: "word")
                    
                    if let suffix = GameValidator.considerSuffixForNextWord(word) {
                        //let availableWordsCount = db.scalar(t_words.filter(c_word.like("\(suffix)%")).filter(!restrictedWords.contains(c_word)).count)
                        let joinedString = restrictedWords.map { str in "'\(str)'"}.joinWithSeparator(", ")
                        let availableWordsCount = Int.fetchOne(db, "SELECT COUNT(*) FROM words WHERE word LIKE '\(suffix)%' AND word NOT IN (?)", arguments: [joinedString])!
                        log.info("available words: \(availableWordsCount)")
                        if availableWordsCount == 0 {
                            var restrictedSuffixesUpdated = restrictedSuffixes
                            restrictedSuffixesUpdated.append(suffix)
                            word = self.prepareWordWithCondition(startWith, maxLenght: maxLenght, restrictedWords: restrictedWords, restrictedSuffixes: restrictedSuffixesUpdated)
                        }
                    }
                    
                    let now = NSDate().timeIntervalSince1970
                    try db.execute("UPDATE words SET ai_used_at = ? WHERE word = ?", arguments: [now, word])
                }
            }
            
        } catch let e {
            log.error("ERROR: AI can not get word from db \(e)")
            assert(false)
        }
        
        if word != nil { tryCount = 0 }
        
        return word
    }
    
}