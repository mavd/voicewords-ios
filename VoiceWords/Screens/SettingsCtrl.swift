//
//  SettingsCtrl.swift
//  VoiceWords
//
//  Created by Georg Romas on 11/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit
import SnapKit

/// Settings screen
class SettingsCtrl: UIViewController {
    
    static let kMusicToggleNotificationKey = "kMusicToggleNotificationKey"

    @IBOutlet weak var toggleSoundsButton: UIButton!
    @IBOutlet weak var toggleMusicButton: UIButton!
    @IBOutlet weak var toggleVoiceButton: UIButton!
    @IBOutlet weak var femaleGenderButton: UIButton!
    @IBOutlet weak var maleGenderButton: UIButton!
    @IBOutlet weak var devSettingsBtn: UIButton!
    
    private var shakeCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.loadSavedSettings()
        
        devSettingsBtn.hidden = !Settings.isDevelopmentMode()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        GA.screenMarket()
        
        //blackAndWhiteEffect()
    }
    
//    override func viewDidAppear(animated: Bool) {
//        super.viewDidAppear(animated)
//        
//        blackAndWhiteEffect()
//    }
    
    override func motionEnded(motion: UIEventSubtype, withEvent event: UIEvent?) {
        if motion == .MotionShake {
            shakeCount += 1
            
            if shakeCount >= 2 {
                blackAndWhiteEffect()
            }
            
            if shakeCount >= 4 {
                Settings.enableDevelopmentMode()
                devSettingsBtn.hidden = false
            }
        }
    }
    
    func blackAndWhiteEffect() {
        let scale = UIScreen.mainScreen().scale
        let targetView = view
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, scale)
        let context = UIGraphicsGetCurrentContext()!
        targetView.layer.renderInContext(context)
        
        let originalImage: CGImageRef = CGBitmapContextCreateImage(context)!
        let ciImage: CIImage = CIImage(CGImage: originalImage)
        let bwFilter = CIFilter(name: "CIColorControls")!
        bwFilter.setValue(ciImage, forKey: kCIInputImageKey)
        bwFilter.setValue(NSNumber(float: 0.0), forKey: kCIInputBrightnessKey)
        bwFilter.setValue(NSNumber(float: 1.1), forKey: kCIInputContrastKey)
        bwFilter.setValue(NSNumber(float: 0.0), forKey: kCIInputSaturationKey)
        
        let bwFilterOutput = bwFilter.outputImage!
        
        let outputImage: UIImage = UIImage(CIImage: bwFilterOutput)
        
        UIGraphicsEndImageContext()
        
        let image = UIImageView(image: outputImage)
        view.addSubview(image)
        image.snp_makeConstraints { [unowned self] make in
            make.left.equalTo(self.view.snp_left)
            make.right.equalTo(self.view.snp_right)
            make.top.equalTo(self.view.snp_top)
            make.bottom.equalTo(self.view.snp_bottom)
        }
        
        image.alpha = 0
        UIView.animateWithDuration(0.5) {
            image.alpha = 1
        }
        UIView.animateWithDuration(0.5, delay: 0.8, options: [],
            animations: {
                image.alpha = 0
            },
            completion: { complete in
                image.removeFromSuperview()
        })
    }
    
    func loadSavedSettings() {
        toggleSoundsButton.selected = Settings.isSoundsEnabled()
        toggleMusicButton.selected = Settings.isMusicEnabled()
        toggleVoiceButton.selected = Settings.isVoiceEnabled()
        
        let gender = Settings.userGender()
        self.maleGenderButton.enabled = !(gender == Settings.Gender.Male)
        self.femaleGenderButton.enabled = !(gender == Settings.Gender.Female)
    }
    
    @IBAction func toggleSoundsAction(sender: AnyObject) {
        toggleSoundsButton.selected = !toggleSoundsButton.selected
        Settings.toggleSounds(toggleSoundsButton.selected)
    }

    @IBAction func toggleMusicAction(sender: AnyObject) {
        toggleMusicButton.selected = !toggleMusicButton.selected
        Settings.toggleMusic(toggleMusicButton.selected)
        
        NSNotificationCenter.defaultCenter().postNotificationName(SettingsCtrl.kMusicToggleNotificationKey, object: Int(toggleMusicButton.selected))
    }
    
    @IBAction func toggleVoiceAction(sender: AnyObject) {
        toggleVoiceButton.selected = !toggleVoiceButton.selected
        Settings.toggleVoice(toggleVoiceButton.selected)
    }
    
    @IBAction func genderToFemaleAtion(sender: AnyObject) {
        maleGenderButton.enabled = true
        femaleGenderButton.enabled = false
        Settings.setGender(Settings.Gender.Female)
    }
    
    @IBAction func genderToMaleAtion(sender: AnyObject) {
        maleGenderButton.enabled = false
        femaleGenderButton.enabled = true
        Settings.setGender(Settings.Gender.Male)
    }
    
    @IBAction func openAgreementAction(sender: AnyObject) {
        let vk = self.storyboard!.instantiateViewControllerWithIdentifier(Constants.ScreenInfo) as! InfoController
        vk.title = "СОГЛАШЕНИЕ"
        vk.isAgreement = true
        self.presentViewController(vk, animated: true, completion: nil)
    }
    
    @IBAction func repeatTutorialAction(sender: AnyObject) {
        Settings.setTutoralNotComplete()
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    @IBAction func closeAction(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
