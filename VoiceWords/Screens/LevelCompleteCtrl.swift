//
//  LevelCompleteCtrl.swift
//  VoiceWords
//
//  Created by Georg Romas on 13/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit
import GameKit

/// Level completion screen
class LevelCompleteCtrl: UIViewController {

    @IBOutlet weak var nextLevelButton: PlayNextLevelButton!
    @IBOutlet weak var replayButton: UIButton!
    @IBOutlet weak var homeButton: UIButton!
    
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var levelScoreLabel: UILabel!
    @IBOutlet weak var coinsLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var avatarController: AvatarController!
    
    @IBOutlet weak var csLogoToTop: NSLayoutConstraint!
    @IBOutlet weak var bottomBottonsToBottom: NSLayoutConstraint!
    @IBOutlet weak var scoreToScoreLabelVertical: NSLayoutConstraint!
    
    let confettiView = ConfettiView()
    
    class LevelInfo {
        var isWin = false
        var levelScore = 0
        var levelTimeInSeconds:Float = 0
        var level = 1
        var solvedWords = 0
        
        init(level: Int, isWin: Bool, levelScore: Int, levelTimeInSeconds:Float, solvedWords: Int) {
            self.isWin = isWin
            self.levelScore = levelScore
            self.levelTimeInSeconds = levelTimeInSeconds
            self.level = level
            self.solvedWords = solvedWords
        }
    }
    
    var levelInfo: LevelInfo!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.screensAdoptation()
        self.avatarController.updateAvatar()
        
        if self.levelInfo.isWin {
            self.topLabel.text = "Отличная работа!"
            GA.gaEvLevelSucceeded(self.levelInfo.level, time: self.levelInfo.levelTimeInSeconds)
        } else {
            self.topLabel.text = "Поражение. Ещё разок?"
            GA.gaEvLevelFailed(self.levelInfo.level, wordsSolved: self.levelInfo.solvedWords, time: self.levelInfo.levelTimeInSeconds)
        }
        
        self.hideAllButtons()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        //TODO: uncomment on release. this is for screenshots
        GA.screenLevelComplete()

        self.presentProgress()
        
        LevelsCompletionStorage.saveSolvedWords(self.levelInfo.solvedWords, level: self.levelInfo.level)
        if self.levelInfo.isWin {
            LevelsCompletionStorage.saveLastNotCompleteLevel(self.levelInfo.level + 1)
        }
        
        func shouldEnableNextLevelAutoStart() -> Bool {
            let isNotLastLevel = self.levelInfo.level < 8
            return self.levelInfo.isWin && Settings.isVoiceEnabled() && isNotLastLevel
        }
        
        delayCall(3.0) {
            if self.levelInfo.level == 8 {
                self.openGameComplete()
            } else {
                self.showAllButtons()
                self.nextLevelButton.hidden = !self.levelInfo.isWin
                self.replayButton.hidden = !self.nextLevelButton.hidden
                
                if shouldEnableNextLevelAutoStart() {
                    self.startNextLevelTimer()
                }
            }
        }
        
        reportScore()
        
        //TODO: comment on release. this is for screenshots
//        self.showAllButtons()
//        self.replayButton.hidden = true
//        self.scoreLabel.text = "27075"
//        self.timeLabel.text = "00:19"
//        self.levelScoreLabel.text = "653"
//        self.coinsLabel.text = "10"
//        self.confetti()
    }
    
    var isPaused = false
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.isPaused = true
        self.confettiView.stop()
    }
    
    private func hideAllButtons() {
        self.nextLevelButton.alpha = 0
        self.replayButton.alpha = 0
        self.homeButton.alpha = 0
    }
    
    private func showAllButtons() {
        self.nextLevelButton.alpha = 1
        self.replayButton.alpha = 1
        self.homeButton.alpha = 1
    }
    
    private func presentProgress() {
        let levelScoreFormat = "%03d"
        let totalScoreFormat = "%d"
        let coinsFormat = "%d"
        let lastScore = ScoreStorage.userTotalScore()
        let newScore = lastScore + self.levelInfo.levelScore
        
        self.coinsLabel.text = "0"
        self.timeLabel.text = self.levelInfo.levelTimeInSeconds.minutesFormat()
        self.scoreLabel.text = String(format: totalScoreFormat, lastScore)
        self.levelScoreLabel.text = String(format: levelScoreFormat, self.levelInfo.levelScore)
        
        let coinsDonate = self.levelInfo.solvedWords * GameSettings.coinsPerWordDonation()
        CoinsStorage.refill(coinsDonate)
        
        func animate() {
            if Settings.isMusicEnabled() {
                if self.levelInfo.isWin {
                    soundsBoss.playOnceScoreGrowProgress()
                } else {
                    soundsBoss.playOnceYouAreLoose()
                }
            }
            self.scoreLabel.changeNumberAnimated(lastScore, to: newScore, steps: 6, format: totalScoreFormat, interval: 0.1)
            self.levelScoreLabel.changeNumberAnimated(self.levelInfo.levelScore, to: 0, steps: 6, format: levelScoreFormat, interval: 0.1)
            self.coinsLabel.changeNumberAnimated(0, to: coinsDonate, steps: 5, format: coinsFormat, interval: 0.1)
        }
        
        delayCall(1.2) { animate() }
        
        let currentUserLevel = ScoreStorage.userLevel()
        ScoreStorage.incrementTotalScore(self.levelInfo.levelScore)

        var newUserLevel = currentUserLevel
        if self.levelInfo.isWin {
            if newUserLevel < 5 {
                switch LevelsCompletionStorage.lastNotCompleteLevel() {
                case 7: newUserLevel = 5
                case 5: newUserLevel = 4
                case 2: newUserLevel = 3
                case 1: newUserLevel = 2
                default: break
                }
            }
        }
        if currentUserLevel < newUserLevel {
            ScoreStorage.saveUserLevel(newUserLevel)
            delayCall(2.0) { self.evaluate() }
        }
    }
    
    private func evaluate() {
        if Settings.isMusicEnabled() { soundsBoss.playOnceNewStatus() }
        self.avatarController.changeAvatar()
        self.confetti()
    }
    
    private func confetti() {
        self.confettiView.addConfettiInView(self.view)
    }
    
    private func screensAdoptation() {
        self.topLabel.font = UIFont(name: Constants.Font_OpenSans_ExtraBold, size: 22)
        self.scoreLabel.font = UIFont(name: Constants.Font_OpenSans_ExtraBold, size: 45)
        
        if is35Inch() {
            csLogoToTop.constant = 5
            bottomBottonsToBottom.constant = 10
        }
    }
    
    private func reportScore() {
        let player = GKLocalPlayer.localPlayer()
        if player.authenticated {
            GameCenterHelper.reportScore(ScoreStorage.userTotalScore())
        }
    }
    
    private func startNextLevelTimer() {
        nextLevelButton.showTimerProgress(10.0) {
            if self.isPaused { return }
            self.nextLevelAction(nil)
        }
    }
    
    private func openGameComplete() {
        let vk = self.storyboard!.instantiateViewControllerWithIdentifier(Constants.ScreenGameComplete) as! GameCompleteCtrl
        self.navigationController!.pushViewController(vk, animated: true)
        delayCall(0.7) { self.removeFromParentViewController() }
    }
    
    private func openGame(level: Int) {
        let vk = self.storyboard!.instantiateViewControllerWithIdentifier(Constants.ScreenLevelLoading) as! LevelLoadingCtrl
        vk.level = level
        self.navigationController!.pushViewController(vk, animated: true)
        
        delayCall(0.7) { self.removeFromParentViewController() }
    }

    @IBAction func nextLevelAction(sender: AnyObject?) {
        var next = self.levelInfo.level + 1
        if next > 8 { next = 8 }
        self.openGame(next)
        
        GA.gaEvPlayeNextLevel(self.levelInfo.level)
    }
    
    @IBAction func replayAction(sender: AnyObject?) {
        self.openGame(self.levelInfo.level)
        
        GA.gaEvLevelReplay(self.levelInfo.level)
    }
    
    @IBAction func menuAction(sender: AnyObject?) {
        self.navigationController!.popViewControllerAnimated(true)
        
        GA.gaEvGoHomeAfterLevel(self.levelInfo.level)
    }
}
