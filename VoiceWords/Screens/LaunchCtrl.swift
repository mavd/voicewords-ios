//
//  LaunchCtrl.swift
//  VoiceWords
//
//  Created by Georg Romas on 10/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

/// Present fake launchscreens with company logo 
/// and open Welcome controller after delay using fade effect
class LaunchCtrl: UIViewController, UIViewControllerTransitioningDelegate {

    @IBOutlet weak var firstLaunchView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Settings.isMusicEnabled(){
            soundsBoss.playLoopMainTheme()
        }
        
        let reachability = try! Reachability(hostname: "google.com")
        if reachability.isReachable() {
            processLoading()
        } else {
            openWelocmeScreen()
        }
    }
    
    private func processLoading() {
        delayCall(1.0) { [weak self] in
            UIView.animateWithDuration(0.6,
                animations: { self?.firstLaunchView.alpha = 0 },
                completion: { complete in
                    delayCall(1.0) {
                        
                        let startDate = NSDate()
                        SyncManager.syncWords(
                            processing: {},
                            complete: {
                                let interval = abs(startDate.timeIntervalSinceNow)
                                print("sync words seconds: ", interval)
                                let intervalDifference = 1.0 - interval
                                print("intervalDifference", intervalDifference)
                                let delay = intervalDifference > 0 ? intervalDifference : 0
                                delayCall(delay) {
                                    ui {
                                        self?.openWelocmeScreen()
                                    }
                                }
                        })
                    }
            })
        }
    }
    
    private func openWelocmeScreen() {
        if Settings.isWelcomeShown() {
            self.openMenu()
        } else {
            let vk = self.storyboard!.instantiateViewControllerWithIdentifier(Constants.ScreenWelcome) as! WelcomeCtrl
            
            vk.dismissCallback = {
                Settings.setWelcomeShown()
                self.openMenu()
            }
            
            vk.modalPresentationStyle = UIModalPresentationStyle.Custom
            vk.transitioningDelegate = self
            self.navigationController!.presentViewController(vk, animated: true, completion: nil)
        }
    }
    
    private func openMenu() {
        let vk = self.storyboard!.instantiateViewControllerWithIdentifier(Constants.ScreenMenu)
        
        let transition = CATransition();
        transition.duration = 0.3;
        transition.type = kCATransitionFade;
        transition.subtype = kCATransitionFromTop;
        
        self.navigationController!.view.layer.addAnimation(transition, forKey: kCATransition)
        self.navigationController!.pushViewController(vk, animated: false)
    }
    
    //MARK: UIViewControllerTransitioningDelegate

    /// Control presentation animation
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return PresentTransition()
    }
    
    /// Control dismiss animation
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DismissTransition()
    }
    
    /// Custom peresnt transition object
    class PresentTransition: NSObject, UIViewControllerAnimatedTransitioning {
        
        // This is used for percent driven interactive transitions, as well as for container controllers that have companion animations that might need to
        // synchronize with the main animation.
        func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
            return 0.4
        }
        
        // This method can only  be a nop if the transition is interactive and not a percentDriven interactive transition.
        func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
            //let fromVK = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)
            let toVK = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)
            
            transitionContext.containerView()!.addSubview(toVK!.view)
            toVK!.view.alpha = 0
            UIView.animateWithDuration(0.4, animations: {
                toVK!.view.alpha = 1
                }) { complete in
                    transitionContext.completeTransition(true)
            }
        }
        
        // This is a convenience and if implemented will be invoked by the system when the transition context's completeTransition: method is invoked.
        func animationEnded(transitionCompleted: Bool) {}
        
    }
    
    /// Custom dismiss transition object
    class DismissTransition: NSObject, UIViewControllerAnimatedTransitioning {
        
        // This is used for percent driven interactive transitions, as well as for container controllers that have companion animations that might need to
        // synchronize with the main animation.
        func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
            return 0.4
        }
        
        // This method can only  be a nop if the transition is interactive and not a percentDriven interactive transition.
        func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
            let fromVK = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)
            let toVK = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)
            
            UIView.animateWithDuration(0.4, animations: {
                fromVK!.view.alpha = 0
                toVK!.view.alpha = 1
                }) { complete in
                    transitionContext.completeTransition(true)
            }
        }
        
        // This is a convenience and if implemented will be invoked by the system when the transition context's completeTransition: method is invoked.
        func animationEnded(transitionCompleted: Bool) {}
        
    }
}
