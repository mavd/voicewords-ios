//
//  MarketCtrlViewController.swift
//  VoiceWords
//
//  Created by Georg Romas on 11/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit
import StoreKit
import Appodeal
import XCGLogger

/// Market screen
class MarketCtrl: UIViewController, UITableViewDataSource, UITableViewDelegate, AppodealRewardedVideoDelegate {
        
    class PurchaseItem {
        let id: Int!
        let storeIdentifyer: String!
        let price: String
        let product: SKProduct?
        
        init(id: Int, storeIdentifyer: String, price: String, product: SKProduct?) {
            self.id = id
            self.storeIdentifyer = storeIdentifyer
            self.price = price
            self.product = product
        }
        
        class func createFromStoreIdentifier(identifier: String) -> PurchaseItem {
            return PurchaseItem(id: 0, storeIdentifyer: identifier, price: "unknown", product: nil)
        }
        
        func title() -> String {
            switch self.storeIdentifyer {
            //case Constants.PurchasePack1: return "Отключить рекламу"
            case Constants.PurchasePack2: return "Обычный набор из\n250 монет"
            case Constants.PurchasePack3: return "Рекомендуем средний\nнабор из 500 монет"
            case Constants.PurchasePack4: return "Нормальный набор\nиз 1 000 монет"
            case Constants.PurchasePack5: return "Максимальный набор\nиз 5 000 монет"
            case Constants.PurchasePack6: return "Посмотрите ролик и получите 50 монет"
            default: return ""
            }
        }

        func image() -> UIImage {
            switch self.storeIdentifyer {
            //case Constants.PurchasePack1: return UIImage(named: "ic_ad_off")!
            case Constants.PurchasePack2: return UIImage(named: "ic_coin_sympli")!
            case Constants.PurchasePack3: return UIImage(named: "ic_coin_middle")!
            case Constants.PurchasePack4: return UIImage(named: "ic_coin_norm")!
            case Constants.PurchasePack5: return UIImage(named: "ic_coin_max")!
            case Constants.PurchasePack6: return UIImage(named: "ic_ad_video")!
            default: return UIImage(named: "")!
            }
        }
        
        func coins() -> Int {
            switch self.storeIdentifyer {
            case Constants.PurchasePack2: return 250
            case Constants.PurchasePack3: return 500
            case Constants.PurchasePack4: return 1000
            case Constants.PurchasePack5: return 5000
            case Constants.PurchasePack6: return 0
            default: return 0
            }
        }
    }
    
    private static var items:[PurchaseItem] = []

    @IBOutlet weak var table: UITableView!
    
    var coinsController: CoinsController?
    var closeCallback: (()->Void)?
    
    class func cachePurchases(products: [SKProduct]) {
        MarketCtrl.items = []
        products.forEach{ product in
            
            let numberFormatter = NSNumberFormatter()
            numberFormatter.formatterBehavior = NSNumberFormatterBehavior.Behavior10_4
            numberFormatter.numberStyle = NSNumberFormatterStyle.CurrencyStyle
            numberFormatter.locale = product.priceLocale
            let priceFormatted = numberFormatter.stringFromNumber(product.price)
            
            MarketCtrl.items.append(PurchaseItem(id: 0, storeIdentifyer: product.productIdentifier, price: priceFormatted!, product: product))
        }
        MarketCtrl.items.append(PurchaseItem(id: 0, storeIdentifyer: Constants.PurchasePack6, price: "бесплатно", product: nil))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadPurchases()
        
        Appodeal.setRewardedVideoDelegate(self)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        GA.screenMarket()
    }
    
    private func loadPurchases() {
        if MarketCtrl.items.count > 0 {
            self.table.reloadData()
        } else {
            StoreHelper.sharedInstance.preLoadPurchasesInfo { [weak self] products in
                MarketCtrl.cachePurchases(products)
                self?.loadPurchases()
            }
        }
    }
    
    func purchaseError(message: String) {
        let title = NSLocalizedString("PurchaseErrorTitle")
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("AlertJustCloseBtn"), style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func disableAds() {
        //
    }

    //MARK: UITableViewDataSource, UITableViewDelegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 70
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MarketCtrl.items.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! PurchaseItemCell
        
        let current = MarketCtrl.items[indexPath.item]
        
        cell.cellTitleLabel.text = current.title()
        cell.cellImage.image = current.image()
        cell.cellPriceLabel.text = current.price
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = table.cellForRowAtIndexPath(indexPath)!

        UIView.animateWithDuration(0.2,
            animations: {
                cell.backgroundColor = UIColor(red: 245/255.0, green: 245/255.0, blue: 245/255.0, alpha: 1)
            },
            completion: { [unowned self] complete in
                UIView.animateWithDuration(0.2){ cell.backgroundColor = UIColor.whiteColor() }
                
                let current = MarketCtrl.items[indexPath.item]
                
                if current.storeIdentifyer == Constants.PurchasePack6 {
                    
                    if self.canShowAd() {
                        if soundsBoss.isMainThemePlayed() {
                            self.continueMusicAfterAd()
                        }
                        if soundsBoss.isRobotPreparedPlayed() {
                            self.continueRobotVoiceAfterAd()
                        }
                        Appodeal.showAd(.RewardedVideo, rootViewController: self)
                        GA.gaEvAddForReward()
                    } else {
                        self.noAdAlert()
                    }
                    
                } else {
                    if let product = current.product {
                        let payment = SKPayment(product: product)
                        SKPaymentQueue.defaultQueue().addPayment(payment)
                    }
                }
        })
    }
    
    private func canShowAd() -> Bool {
        return Appodeal.isReadyForShowWithStyle(AppodealShowStyle.RewardedVideo) && Settings.canShowAdForReward()
    }
    
    private func noAdAlert() {
        let alert = UIAlertController(title: "Пока рекламы нет", message: "Приходите через пару часов", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Закрыть", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    private var shouldResumeMusic = false
    private func continueMusicAfterAd() {
        soundsBoss.stopMainTheme()
        shouldResumeMusic = true
    }
    private var shouldResumeRobotVoice = false
    private func continueRobotVoiceAfterAd() {
        soundsBoss.stopRobotPrepared()
        shouldResumeRobotVoice = true
    }
    
    private func resumeMusic() {
        if shouldResumeMusic {
            shouldResumeMusic = false
            soundsBoss.playLoopMainTheme()
        }
        if shouldResumeRobotVoice {
            shouldResumeRobotVoice = false
            soundsBoss.playLoopRobotPrepared()
        }
    }
    
    //MARK: AppodealRewardedVideoDelegate
    
    func rewardedVideoDidLoadAd() {
        log.info("rewardedVideoDidLoadAd")
    }
    
    func rewardedVideoDidFailToLoadAd() {
        log.info("rewardedVideoDidFailToLoadAd")
    }
    
    func rewardedVideoDidPresent() {
        log.info("rewardedVideoDidPresent")
    }
    
    func rewardedVideoWillDismiss() {
        log.info("rewardedVideoWillDismiss")
        resumeMusic()
    }
    
    func rewardedVideoDidFinish(rewardAmount: UInt, name rewardName: String!) {
        log.info("rewardedVideoDidFinish: \(rewardAmount) \(rewardName)")
        CoinsStorage.refill(Int(rewardAmount))
        
        let alert = UIAlertController(title: "Вы получили \(rewardAmount) монет", message: "Спасибо за просмотр", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Закрыть", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func rewardedVideoDidClick() {
        log.info("rewardedVideoDidClick")
    }

    @IBAction func closeAction(sender: AnyObject?) {
        self.closeCallback?()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
