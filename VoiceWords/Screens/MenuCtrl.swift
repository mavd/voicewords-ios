//
//  MenuCtrl.swift
//  VoiceWords
//
//  Created by Georg Romas on 11/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit
import GameKit
import QuartzCore
import XCGLogger
import AVFoundation

/// Main completion screen
class MenuCtrl: UIViewController, GKGameCenterControllerDelegate {

    @IBOutlet weak var progressIndicatorView: UIView!
    @IBOutlet weak var progressView: BrainIndicator!
    @IBOutlet weak var avatarController: AvatarController!
    
    @IBOutlet weak var csBottomLabelToBrainVertical: NSLayoutConstraint!
    @IBOutlet weak var csBarToBtmLabelVertial: NSLayoutConstraint!
    @IBOutlet weak var csLogoToTop: NSLayoutConstraint!
    @IBOutlet weak var csProgressToRightSpace: NSLayoutConstraint!
    
    @IBOutlet weak var helpLebel: UILabel!
    @IBOutlet weak var coinsController: CoinsController!
    
    var overlayHUD: HUDoverlay!
    var overlayHUD_db: HUDoverlay!
    
    var reachability = try! Reachability(hostname: "google.com")
    
    var speechRecognizer = YSKRecognizer(language: "ru-RU", model: "general")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MenuCtrl.musicSettingsStateChanged(_:)), name: SettingsCtrl.kMusicToggleNotificationKey, object: nil)
        
        if self.overlayHUD == nil {
            self.overlayHUD = HUDoverlay.setupInView(self.view)
        }
        
        if self.overlayHUD_db == nil {
            self.overlayHUD_db = HUDoverlay.setupInView(self.view)
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MenuCtrl.reachabilityChanged(_:)), name: ReachabilityChangedNotification,object: reachability)
        try! reachability.startNotifier()
        
        self.screensAdoptation()
        
        self.checkTutorial()
        
        //TODO: uncomment on release. this is for screenshots
        self.syncUserScoreWithGC()
        
        //TODO: comment on release. this is for screenshots
//        ScoreStorage.setTotalScore(27075)
//        ScoreStorage.saveUserLevel(3)
//        CoinsStorage.setCoins(295)
    }
    
    private var isPaused = false
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        GA.screenMenu()

        self.isPaused = false
        
        self.coinsController.actualizeDisplayedData()
        self.progressView.updateScore()
        self.avatarController.updateAvatar()
        delayCall(1.0) { self.animateLabel() }
        
        if Settings.isMusicEnabled(){
            soundsBoss.playLoopMainTheme()
        }
        
        self.checkTutorial()
        
        self.syncWordsDB()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        reachability.stopNotifier()
        NSNotificationCenter.defaultCenter().removeObserver(self)
        
        self.isPaused = true
        self.stopAnimations()
    }
    
    private func checkTutorial() {
        if !Settings.isTutorialComplete() {
            let vk = self.storyboard!.instantiateViewControllerWithIdentifier(Constants.ScreenGame) as! MainCtrl
            vk.level = 0
            vk.isTutorial = true
            self.navigationController!.pushViewController(vk, animated: false)
            soundsBoss.stopMainTheme()
        }
    }

    private func screensAdoptation() {
        if is35Inch() {
            csBottomLabelToBrainVertical.constant = 2
            csBarToBtmLabelVertial.constant = 2
            csLogoToTop.constant = 5
        }
        
        if is47Inch() || is55Inch() {
            csBarToBtmLabelVertial.constant = 100
        }
    }

    private func animateLabel() {
        self.helpLebel.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0)
        UIView.animateWithDuration(0.5, delay: 0, options: UIViewAnimationOptions.CurveEaseOut,
            animations: {
                self.helpLebel.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1)
            },
            completion: {complete in
                
                UIView.animateWithDuration(0.5, delay: 0, options: UIViewAnimationOptions.CurveEaseOut,
                    animations: {
                        self.helpLebel.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0)
                    },
                    completion: {complete in
                        if !self.isPaused { self.animateLabel() }
                })
                
        })
    }
    
    private func stopAnimations() {
        self.helpLebel.layer.removeAllAnimations()
    }
    
    /*
    GKGameCenterControllerDelegate
    Just to close game center screen purpose
    */
    func gameCenterViewControllerDidFinish(gameCenterViewController: GKGameCenterViewController) {
        gameCenterViewController.dismissViewControllerAnimated(true, completion: nil)
    }
    
    private var gameCneterIsBlocked = false
    private func authenticateGCPlayer(localPlayer: GKLocalPlayer, success: ()->Void) {
        GKLocalPlayer.localPlayer().authenticateHandler = {controller, error in
            if let error = error {
                log.error("GC: authentication error \(error)")
                if error.domain == GKErrorDomain && error.code == 2 {
                    self.gameCneterIsBlocked = true
                }
            }
            
            if let authController = controller {
                // do not block tutorial with authentication
                if !Settings.isTutorialComplete() { return }
                
                self.presentViewController(authController, animated: true, completion: nil)
                log.info("GC: try authenticate local player")
            } else if localPlayer.authenticated {
                log.info("GC: local player is authenticated")
                localPlayer.setDefaultLeaderboardIdentifier(Constants.LeaderboardIDGeneral, completionHandler: nil)
                success()
            } else {
                log.error("GC: Unknown error for game center authentication")
            }
        }
    }
    
    private func runLevel(level: Int) {
        let vk = self.storyboard!.instantiateViewControllerWithIdentifier(Constants.ScreenLevelLoading) as! LevelLoadingCtrl
        vk.level = level
        self.navigationController!.pushViewController(vk, animated: true)
    }
    
    var dbUpdateInProgress = false
    private func syncWordsDB() {
        // do not lock db on tutorial
        if !Settings.isTutorialComplete() { return }
        
        view.setNeedsLayout()
        
        let fullProgress = view.frame.size.width
        
        csProgressToRightSpace.constant = fullProgress - 1
        progressIndicatorView.alpha = 1
        view.layoutIfNeeded()
        view.setNeedsLayout()
        
        UIView.animateWithDuration(10) { [weak self] in
            self?.csProgressToRightSpace.constant = fullProgress - 150
            self?.view.layoutIfNeeded()
        }
        
        dbUpdateInProgress = true
        SyncManager.syncWords(
            processing: { [weak self] in
                self?.overlayHUD_db.showSyncDB()
            },
            complete:{ [weak self] in
            self?.overlayHUD_db.hide()
            self?.dbUpdateInProgress = false
            self?.progressIndicatorView.layer.removeAllAnimations()
            self?.csProgressToRightSpace.constant = fullProgress - 1
            self?.view.layoutIfNeeded()
            self?.view.setNeedsLayout()
            UIView.animateWithDuration(1, delay: 0, options: [],
                animations: {
                    self?.csProgressToRightSpace.constant = 0
                    self?.view.layoutIfNeeded()
                },
                completion: { isComplete in
                    self?.view.setNeedsLayout()
                    
                    UIView.animateWithDuration(2, delay: 0, options: [],
                        animations: {
                            self?.progressIndicatorView.alpha = 0
                            self?.view.layoutIfNeeded()
                        },
                        completion: { isComplete in})
            })
        })
    }
    
    private func syncUserScoreWithGC() {
        let localPlayer = GKLocalPlayer.localPlayer()
        if localPlayer.authenticated {
            self.overlayHUD.hide()
            let leaderBoard = GKLeaderboard(players: [localPlayer])
            leaderBoard.identifier = Constants.LeaderboardIDGeneral
            leaderBoard.loadScoresWithCompletionHandler{ scores, error in
                log.info("GC score: \(scores) | error: \(error)")
                if let scores = scores {
                    if scores.count > 0 {
                        let score = Int(scores[0].value)
                        if ScoreStorage.userTotalScore() < score {
                            ScoreStorage.setTotalScore(score)
                            self.progressView.updateScore()
                            self.avatarController.updateAvatar()
                        }
                    }
                }
            }
        } else {
            if ScoreStorage.userTotalScore() == 0 {
                self.overlayHUD.showWaitGC()
                delayCall(2.2) { self.overlayHUD.hide() }
            }
            self.authenticateGCPlayer(localPlayer) {
                if localPlayer.authenticated {
                    self.syncUserScoreWithGC()
                }
            }
        }
    }
    
    @objc func musicSettingsStateChanged(isMusicEnabled: Int) {
        if (isMusicEnabled == 1) {
            soundsBoss.playLoopMainTheme()
        } else {
            soundsBoss.stopMainTheme()
        }
    }
    
    @objc func reachabilityChanged(note: NSNotification) {
        if reachability.isReachable() {
            if reachability.isReachableViaWiFi() {
                log.info("Reachable via WiFi")
                GA.gaEvReachableWifi()
            } else {
                log.info("Reachable via Cellular")
                GA.gaEvReachableGSM()
            }
            self.overlayHUD.hide()
        } else {
            self.overlayHUD.showNoInternet()
            log.error("Network not reachable")
            GA.gaEvReachableNoInternet()
        }
    }
    
    @IBAction func levelslAction(sender: AnyObject?) {
        let vk = self.storyboard!.instantiateViewControllerWithIdentifier(Constants.ScreenLevels) as! LevelsCtrl
        vk.selectedLevelCallback = { [unowned self] level in
            soundsBoss.stopMainTheme()
            self.runLevel(level)
        }
        self.presentViewController(vk, animated: true, completion: nil)
    }
    
    @IBAction func gameCenterAction(sender: AnyObject?) {
        let localPlayer = GKLocalPlayer.localPlayer()
        if localPlayer.authenticated {
            let gk = GKGameCenterViewController()
            gk.viewState = .Leaderboards
            gk.leaderboardIdentifier = Constants.LeaderboardIDGeneral
            gk.gameCenterDelegate = self
            self.presentViewController(gk, animated: true, completion: nil)
        } else {
            if self.gameCneterIsBlocked {
                let alert = UIAlertController(title: "Game center отключен", message: "Ранее вы отключили Game Center для этого приложения. Теперь его нужно включить в настройках iOS.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Закрыть", style: UIAlertActionStyle.Cancel, handler: nil))
                self.navigationController!.presentViewController(alert, animated: true, completion: nil)
                return
            }
            
            self.authenticateGCPlayer(localPlayer) {
                self.overlayHUD.showWaitGC()
                if localPlayer.authenticated {
                    self.overlayHUD.hide()
                    self.gameCenterAction(nil)
                }
            }
        }
    }
    
    @IBAction func openMarketAction(sender: AnyObject?) {
        let vk = self.storyboard!.instantiateViewControllerWithIdentifier(Constants.ScreenMarket) as! MarketCtrl
        vk.coinsController = self.coinsController
        self.presentViewController(vk, animated: true, completion: nil)
    }
    
    @IBAction func infoAction(sender: AnyObject?) {
        let vk = self.storyboard!.instantiateViewControllerWithIdentifier(Constants.ScreenInfo) as! InfoController
        vk.isAgreement = false
        vk.title = "ОБ ИГРЕ"
        self.presentViewController(vk, animated: true, completion: nil)
    }
    
    @IBAction func tapScreenAction(sender: UITapGestureRecognizer) {
        // DB is ,ocked, can not process
        if dbUpdateInProgress { return }
        
        if !reachability.isReachable() {
            overlayHUD.showNoInternet()
            return
        }
        
        soundsBoss.stopMainTheme()
        self.runLevel(LevelsCompletionStorage.lastNotCompleteLevel())
    }
    
}
