//
//  InfoBaseController.swift
//  VoiceWords
//
//  Created by Georg Romas on 12/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

/// Information controller with webview to present text with agreement or about application, formatted via html.
class InfoController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var barTitleLabel: UILabel!
    
    private var documentName = "aboutApp"
    
    /// Controller load different files depend on this property.
    ///
    /// If this is not agreement, "About app" will be used
    var isAgreement = false

    /// Setup screen
    override func viewDidLoad() {
        super.viewDidLoad()

        barTitleLabel.text = title
        
//        if isAgreement {
//            loadUrl()
//        } else {
            loadText()
//        }
    }

    private func loadText() {
        let filePath = NSBundle.mainBundle().pathForResource(documentName, ofType: "html")!
        var text = "<h1>Error to load agreement</h1><p>Contact support, please.</p>"
        do  {
            text = try NSString(contentsOfFile: filePath, encoding: NSUTF8StringEncoding) as String
        } catch { }
        self.webView.loadHTMLString(text, baseURL: nil)
    }
    
    private func loadUrl() {
        let request = NSURLRequest(URL: NSURL(string: "https://www.google.ru/intl/en/policies/privacy/?fg=1")!)
        self.webView.loadRequest(request)
    }

    @IBAction func closeAction(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
