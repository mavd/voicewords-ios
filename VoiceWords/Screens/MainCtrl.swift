//
//  ViewController.swift
//  VoiceWords
//
//  Created by Georg Romas on 08/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit
import XCGLogger
import AVFoundation

/**
 Game screen controller
 This screen used as container for game and tutorial, but not implement game logic.
 Game logic delegated to Game controller and Tutorial controller depend on current state or user interaction.
 */
class MainCtrl: UIViewController {

    /// Bottom player input deck controller
    @IBOutlet weak var deckController: InputDeckController!
    /// Bottom deck constrain
    @IBOutlet weak var csDeckToBottomVertical: NSLayoutConstraint!
    /// Player input field
    @IBOutlet weak var inputField: UITextField!
    /// Game field View
    @IBOutlet weak var gameField: GameFeildView!
    /// Coins indicator View
    @IBOutlet weak var coinsController: CoinsController!
    /// Score indicator View
    @IBOutlet weak var scoreIndicator: ScoreIndicator!
    /// Tutorial View
    @IBOutlet weak var tutorialView: TutorialView!
    
    private var gameController: GameController!
    private var tutorialController: TutorialController!
    
    /// Used in game and tutorial controller. Required to restore timer and handle task complitions.
    var closeMarketCallback: (()->Void)?
    
    /// This controller launched from main menu, which can change this parameter depend on user action or current user level.
    var level = 1
    /// Used to identify when to run tutorial. This controller launched from main menu, which can change this parameter depend on user action.
    var isTutorial = false
    
    private var reachability = try! Reachability(hostname: "google.com")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MainCtrl.reachabilityChanged(_:)), name: ReachabilityChangedNotification,object: reachability)
        try! reachability.startNotifier()

        self.setupGameController()
        
        //TODO: uncomment on release. this is for screenshots
        if isTutorial {
            GameSettings.setPauseTimerOnRecognition(true)
            self.tutorialController = TutorialController(gameController: self.gameController, screenController: self, tutorialView: self.tutorialView)
            delayCall(1.0) { self.tutorialController.start() }
        } else {
            self.tutorialView.removeFromSuperview()
            delayCall(1.0) { self.gameController.startGame() }
        }
        
        //TODO: comment on release. this is for screenshots
//        self.gameField.pushWordFromOpponent("Окоп")
//        self.gameField.pushWordFromPlayer("Порыв")
//        self.gameField.pushWordFromOpponent("Въезд")
//        self.gameField.pushWordFromPlayer("Задача")
//        self.gameField.pushWordFromOpponent("Бляшка")
//        self.gameField.pushWordFromPlayer("Амортизатор")
//        self.gameField.pushWordFromOpponent("Ролл")
//        self.gameField.pushWordFromPlayer("Лабиринт")
//        self.gameField.pushWordFromOpponent("Тяжесть")
//        self.scoreIndicator.scoreUP(1909)
//        self.coinsController.label.text = "105"
//        self.deckController.timerIndicator.updateProgress(55, totalTime: 60, level: 1)
//        delayCall(1.0) {
//            self.deckController.timerIndicator.label!.text = "957"
//            self.deckController.unlockChitButtons()
//        }
//        self.deckController.recognizerStateIndicatorLabel.text = "Слушаю"
//        self.deckController.voiceIndicator.hidden = false
//        self.deckController.voiceIndicator.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.4, 1.4)
//        self.deckController.showWord("T")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.sharedApplication().idleTimerDisabled = true
        
        if isTutorial {
            GA.screenTutorial()
        } else {
            GA.screenGame()
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
        
        UIApplication.sharedApplication().idleTimerDisabled = false
        
        if isTutorial {
            GameSettings.setPauseTimerOnRecognition(false)
        }
    }
    
    deinit {
        print("deinit debug MainCtrl")
    }
    
    private func setupGameController() {
        let gameUIGroup = GameUIGroup(screen: self, gameField: self.gameField, coinsController: self.coinsController, scoreIndicator: self.scoreIndicator)
        self.gameController = GameController(level: self.level, gameUIGroup: gameUIGroup, deckController: self.deckController, levelCompleteCallback: { [unowned self] isWin, levelScore, levelTimeInSeconds, solvedWords in
            if isWin {
                log.info("Level complete SUCCESS!")
                self.levelComplete(isWin: true, levelScore: levelScore, levelTimeInSeconds: levelTimeInSeconds, solvedWords: solvedWords)
            } else {
                log.info("Level complete FAIL!")
                self.levelComplete(isWin: false, levelScore: levelScore, levelTimeInSeconds: levelTimeInSeconds, solvedWords: solvedWords)
            }
        })
        
        self.handleKeyboardNotifications()
    }
    
    private func handleKeyboardNotifications() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MainCtrl.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MainCtrl.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
        
        self.inputField.delegate = self.gameController
        self.inputField.autocorrectionType = UITextAutocorrectionType.No
    }
    
    private func levelTargetTime() -> Int {
        return 120
    }
    
    private func levelComplete(isWin win: Bool, levelScore: Int, levelTimeInSeconds: Float, solvedWords: Int) {
        let vk = self.storyboard!.instantiateViewControllerWithIdentifier(Constants.ScreenLevelComplete) as! LevelCompleteCtrl
        vk.levelInfo = LevelCompleteCtrl.LevelInfo(level: self.level, isWin: win, levelScore: levelScore, levelTimeInSeconds: levelTimeInSeconds, solvedWords: solvedWords)
        delayCall(1.2) {
            self.navigationController!.pushViewController(vk, animated: true)
            
            self.gameController.cancelGame()
            delayCall(0.7) { self.removeFromParentViewController() }
        }
    }
    
    //MARK: move deck for keyboard
    private func moveDeckUp(height: CGFloat) {
        self.csDeckToBottomVertical.constant = height
        self.view.needsUpdateConstraints()
        
        UIView.animateWithDuration(0.5) {
            self.view.layoutIfNeeded()
        }
        self.deckController.switchToKeyboardNode()
    }
    
    private func moveDeckDown() {
        self.csDeckToBottomVertical.constant = 5
        self.view.needsUpdateConstraints()
        
        UIView.animateWithDuration(0.5) {
            self.view.layoutIfNeeded()
        }
        self.deckController.switchToAudioMode()
    }
    
    /// Keyboard shown notification handler
    @objc func keyboardWillShow(notification: NSNotification) {
        self.gameController.switchToKeyboardMode()
        
        if let userInfo = notification.userInfo {
            if let keyboardSize = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue {
                moveDeckUp(keyboardSize.CGRectValue().height)
            }
        }
    }
    
    /// Keyboard hidden notification handler
    @objc func keyboardWillHide(notification: NSNotification) {
        self.gameController.switchToVoiceMode()
        
        moveDeckDown()
    }
    
    private func canNotUseMarket() -> Bool {
        if self.isTutorial {
            return self.tutorialController.marketOpened()
        } else {
            return self.gameController.marketOpened()
        }
    }
    
    /// Internet access status changed notification handler
    @objc func reachabilityChanged(note: NSNotification) {
        if reachability.isReachable() {
            if reachability.isReachableViaWiFi() {
                log.info("Reachable via WiFi")
                GA.gaEvReachableWifi()
            } else {
                log.info("Reachable via Cellular")
                GA.gaEvReachableGSM()
            }
        } else {
            log.error("Network not reachable")
            let alert = UIAlertController(title: title, message: "Извините. Нельзя продолжить без интернет-соединения.", preferredStyle: .Alert)
            let cancel = UIAlertAction(title: NSLocalizedString("AlertJustCloseBtn"), style: .Cancel, handler: { action in
                self.closeAction(nil)
            })
            alert.addAction(cancel)
            self.presentViewController(alert, animated: true, completion: nil)
            GA.gaEvReachableNoInternet()
        }
    }

    /// Toggle input mode button handler
    @IBAction func inputTypeBtnAction(sender: AnyObject) {
        AVAudioSession.sharedInstance().requestRecordPermission{ [unowned self] granted in
            if !granted {
                
                let message = "Ранее вы запретили доступ к микрофону. Включите доступ в Настройках телефона > Микрофон > Словолюб."
                let alert = UIAlertController(title: "Нет доступа к микрофону", message: message, preferredStyle: .Alert)
                
                alert.addAction(UIAlertAction(title: "Закрыть", style: .Cancel, handler: nil))
                
                self.navigationController!.presentViewController(alert, animated: true, completion: nil)
                
                self.inputField.becomeFirstResponder()
                
                return
            }
        }
        
        if self.deckController.isKeyboardMode() {
            self.inputField.resignFirstResponder()
        } else {
            self.inputField.becomeFirstResponder()
        }
    }

    /// Open market button handler
    @IBAction func openMarketAction(sender: AnyObject?) {
        
        if self.canNotUseMarket() { return }
        
        let vk = self.storyboard!.instantiateViewControllerWithIdentifier(Constants.ScreenMarket) as! MarketCtrl
        vk.coinsController = self.coinsController
        vk.closeCallback = self.closeMarketCallback
        self.presentViewController(vk, animated: true, completion: nil)
    }
    
    /// close game screen button handler
    @IBAction func closeAction(sender: AnyObject?) {
        
        self.gameController?.cancelGame()
        self.tutorialController?.cancelTutorial()
        
        self.navigationController!.popViewControllerAnimated(true)
        delayCall(0.7) { self.removeFromParentViewController() }
    }
}

