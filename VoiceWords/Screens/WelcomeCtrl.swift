//
//  PromoCtrl.swift
//  VoiceWords
//
//  Created by Georg Romas on 09/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit
import QuartzCore
import GRDB

/// Welcome screen with paged scrollview 
class WelcomeCtrl: UIViewController, UIScrollViewDelegate, UIGestureRecognizerDelegate, UIViewControllerTransitioningDelegate {
    
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var pageinationBtn_1: UIButton!
    @IBOutlet weak var pageinationBtn_2: UIButton!
    @IBOutlet weak var pageinationBtn_3: UIButton!
    
    @IBOutlet weak var startButton: UIButton!
    
    @IBOutlet weak var pageView_1: UIView!
    @IBOutlet weak var pageView_2: UIView!
    @IBOutlet weak var pageView_3: UIView!
    
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var paginationBlock: UIView!
    
    var dismissCallback: (()->())!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        
        //TODO: comment all demo
//        demoFileRead()
//        demoSqlite()
//        demoSQL()
//        YSKVocalizer(text: "собака").start()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        GA.screenWelcome()
    }
    
    
    private func demoSQL() {
        do {
            
            try DBHelper.db().inDatabase { db in
                let c_1 = Int.fetchOne(db, "SELECT COUNT(*) FROM words WHERE word LIKE 'й%'")
                print("count start with й = \(c_1)")
                
                let c_2 = Int.fetchOne(db, "SELECT COUNT(*) FROM words WHERE word LIKE 'а%' AND word LIKE '%а' AND length(word) <= 4")
                print("count = \(c_2)")
                
                for row in Row.fetch(db, "SELECT * FROM words WHERE word LIKE 'й%'") {
                    let word = row.value(named: "word")
                    print("word: \(word)")
                }
            }
            
        } catch let e {
            print("ERROR: SQL connection \(e)")
        }
    }
    
    private func demoFileRead() {
        if let filePath = NSBundle.mainBundle().pathForResource("db", ofType: "txt") {
            do  {
                let string = try String(contentsOfFile: filePath)
                
                DBHelper.updateWordsFromText(string)
            } catch let e {
                print("ERROR: can not read database text file: \(e)")
            }
        } else {
            print("db.txt not found")
        }
    }
    
    
    func setup() {
        self.pageinationBtn_1.enabled = false
        
        self.setupStartButton()
    }
    
    func setupStartButton() {
        self.startButton.setBackgroundImage(UIImage.imagePixelFromColor(UIColor.charcoal()), forState: UIControlState.Normal)
        self.startButton.setBackgroundImage(UIImage.imagePixelFromColor(UIColor.blackColor()), forState: UIControlState.Highlighted)
        self.startButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.startButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Highlighted)
        self.startButton.layer.cornerRadius = self.startButton.frame.size.height/2
        self.startButton.clipsToBounds = true
    }
    
    //MARK: UIScrollViewDelegate
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        paginationCheck(scrollView)
    }
    
    func scrollViewDidEndScrollingAnimation(scrollView: UIScrollView) {
        paginationCheck(scrollView)
    }
    
    func currentScrollPageIndex() -> Int {
        return Int(self.scroll.bounds.origin.x/self.scroll.bounds.width)
    }
    
    func hideLogoAndPagination() {
        if self.logoImage.alpha > 0 {
            UIView.animateWithDuration(0.4) {
                self.logoImage.alpha = 0
                self.paginationBlock.alpha = 0
            }
        }
    }
    
    func showLogoAndPagination() {
        if self.logoImage.alpha == 0 {
            UIView.animateWithDuration(0.4) {
                self.logoImage.alpha = 1
                self.paginationBlock.alpha = 1
            }
        }
    }
    
    func paginationCheck(scrollView: UIScrollView) {
        self.pageinationBtn_2.enabled = true
        self.pageinationBtn_3.enabled = true
        self.pageinationBtn_1.enabled = true
        self.showLogoAndPagination()
        
        switch currentScrollPageIndex() {
        case 0: self.pageinationBtn_1.enabled = false
        case 1: self.pageinationBtn_2.enabled = false
        case 2: self.pageinationBtn_3.enabled = false
        default:
            self.hideLogoAndPagination()
            delayCall(0.5) {
                self.dismissCallback()
                self.dismissViewControllerAnimated(true, completion: nil)
            }
            break
        }
    }
    
    func switchPageWithRipple(startPoint point: CGPoint, pageIndex: Int) {
        if pageIndex < 4 {
            let rect = CGRectMake(CGFloat(pageIndex) * self.scroll.bounds.width, 0, self.scroll.bounds.width, self.scroll.bounds.height)
            self.scroll.scrollRectToVisible(rect, animated: true)
        }
    }
    
    @IBAction func pageinationBtnAction(sender: UIButton?) {
        var index = 0
        
        switch sender {
            
        case .Some(self.pageinationBtn_1): index = 0
        case .Some(self.pageinationBtn_2): index = 1
        case .Some(self.pageinationBtn_3): index = 2
        case .Some(self.startButton): index = 3
        default: break
            
        }
        
        switchPageWithRipple(startPoint: self.view.convertPoint(sender!.center, fromView: sender!.superview), pageIndex: index)
    }
    
    @IBAction func tapAction(sender: UITapGestureRecognizer) {
        let index = currentScrollPageIndex()  + 1
        switchPageWithRipple(startPoint: sender.locationInView(self.view), pageIndex: index)
    }
    
    @IBAction func startButtonAction(sender: AnyObject?) {
    }
}