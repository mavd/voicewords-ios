//
//  LevelsCtrl.swift
//  VoiceWords
//
//  Created by Georg Romas on 12/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

/// Levels screen with levels grid
class LevelsCtrl: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var collectionEdgeInserts = UIEdgeInsets(top: 10, left: 60, bottom: 40, right: 60)
    
    /// Callback
    var selectedLevelCallback:((level: Int)->Void)?
    
    private var levels = [Level]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        screensAdoptation()
        
        //TODO: this is stupid. enable your brain, please.
        let wordsLevel_1 = GameSettings.wordsCountForLevel(1)
        let wordsLevel_2 = GameSettings.wordsCountForLevel(2)
        let wordsLevel_3 = GameSettings.wordsCountForLevel(3)
        let wordsLevel_4 = GameSettings.wordsCountForLevel(4)
        let wordsLevel_5 = GameSettings.wordsCountForLevel(5)
        let wordsLevel_6 = GameSettings.wordsCountForLevel(6)
        let wordsLevel_7 = GameSettings.wordsCountForLevel(7)
        let wordsLevel_8 = GameSettings.wordsCountForLevel(8)
        
        let completeWordsLevel_1 = LevelsCompletionStorage.solvedWordsForLevel(1)
        let completeWordsLevel_2 = LevelsCompletionStorage.solvedWordsForLevel(2)
        let completeWordsLevel_3 = LevelsCompletionStorage.solvedWordsForLevel(3)
        let completeWordsLevel_4 = LevelsCompletionStorage.solvedWordsForLevel(4)
        let completeWordsLevel_5 = LevelsCompletionStorage.solvedWordsForLevel(5)
        let completeWordsLevel_6 = LevelsCompletionStorage.solvedWordsForLevel(6)
        let completeWordsLevel_7 = LevelsCompletionStorage.solvedWordsForLevel(7)
        let completeWordsLevel_8 = LevelsCompletionStorage.solvedWordsForLevel(8)
        
        levels = [
            Level(id: 1, total: wordsLevel_1, complete: completeWordsLevel_1, unlocked: true),
            Level(id: 2, total: wordsLevel_2, complete: completeWordsLevel_2, unlocked: wordsLevel_1 == completeWordsLevel_1),
            Level(id: 3, total: wordsLevel_3, complete: completeWordsLevel_3, unlocked: wordsLevel_2 == completeWordsLevel_2),
            Level(id: 4, total: wordsLevel_4, complete: completeWordsLevel_4, unlocked: wordsLevel_3 == completeWordsLevel_3),
            Level(id: 5, total: wordsLevel_5, complete: completeWordsLevel_5, unlocked: wordsLevel_4 == completeWordsLevel_4),
            Level(id: 6, total: wordsLevel_6, complete: completeWordsLevel_6, unlocked: wordsLevel_5 == completeWordsLevel_5),
            Level(id: 7, total: wordsLevel_7, complete: completeWordsLevel_7, unlocked: wordsLevel_6 == completeWordsLevel_6),
            Level(id: 8, total: wordsLevel_8, complete: completeWordsLevel_8, unlocked: wordsLevel_7 == completeWordsLevel_7)
        ]
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        GA.screenLevels()
    }
    
    private func screensAdoptation() {
        if is47Inch() {
            self.collectionEdgeInserts = UIEdgeInsets(top: 10, left: 90, bottom: 40, right: 90)
        }
        if is55Inch() {
            self.collectionEdgeInserts = UIEdgeInsets(top: 10, left: 100, bottom: 40, right: 100)
        }
    }
    
    // MARK: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return levels.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let current = self.levels[indexPath.item]
        
        if current.unlocked {
            
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! LevelActiveCell
            cell.titleLabel.text = String(current.id)
            cell.levelsLabel.text = "\(current.complete)/\(current.total)"
            cell.adjustCell()
            return cell
            
        } else {
            
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CellDisabled", forIndexPath: indexPath) as! LevelDisabledCell
            cell.titleLabel.text = String(current.id)
            cell.adjustCell()
            return cell
            
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: 70, height: 70)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return self.collectionEdgeInserts
    }
    
    func collectionView(collectionView: UICollectionView, didHighlightItemAtIndexPath indexPath: NSIndexPath) {
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! LevelCell
        cell.animateSelection()
    }
    
    func collectionView(collectionView: UICollectionView, didUnhighlightItemAtIndexPath indexPath: NSIndexPath) {
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! LevelCell
        cell.hideSelection()
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let current = self.levels[indexPath.item]
        let lastNotCompleteLevel = LevelsCompletionStorage.lastNotCompleteLevel()
        if current.unlocked && current.id == lastNotCompleteLevel {
            self.selectedLevelCallback?(level: current.id)
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    /// Close screen button handler
    @IBAction func closeAction(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
