//
//  AboutCtrl.swift
//  VoiceWords
//
//  Created by Georg Romas on 20/03/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

/// Present formatted text about application
class AboutCtrl: UIViewController {

    @IBOutlet weak var gradientView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupGradientView()
    }
    
    private func setupGradientView() {
        let gradiendLayer = CAGradientLayer()
        self.gradientView.backgroundColor = UIColor.clearColor()
        gradiendLayer.frame = self.gradientView.bounds
        gradiendLayer.colors = [UIColor.whiteColor().CGColor, UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0).CGColor]
        self.gradientView.layer.addSublayer(gradiendLayer)
    }

    /// Opane link button handler
    @IBAction func spaceShipAppsAction(sender: AnyObject) {
        UIApplication.sharedApplication().openURL(NSURL(string: "http://spaceshipapps.ru")!)
    }
    
    /// Close screen button handler
    @IBAction func closeAction(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
