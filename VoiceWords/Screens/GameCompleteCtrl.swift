//
//  LevelCompleteCtrl.swift
//  VoiceWords
//
//  Created by Georg Romas on 13/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit
import GRDB
import XCGLogger

/// Game completion screen
class GameCompleteCtrl: UIViewController {

    @IBOutlet weak var homeButton: UIButton!
    
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var avatarController: AvatarController!
    
    @IBOutlet weak var csLogoToTop: NSLayoutConstraint!
    @IBOutlet weak var bottomBottonToBottom: NSLayoutConstraint!
    @IBOutlet weak var scoreToScoreLabelVertical: NSLayoutConstraint!
    
    let confettiView = ConfettiView()
    
    class LevelInfo {
        var isWin = false
        var levelScore = 0
        var levelTimeInSeconds:Float = 0
        var level = 1
        
        init(level: Int, isWin: Bool, levelScore: Int, levelTimeInSeconds:Float) {
            self.isWin = isWin
            self.levelScore = levelScore
            self.levelTimeInSeconds = levelTimeInSeconds
            self.level = level
        }
    }
    
    var levelInfo: LevelInfo!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.screensAdoptation()
        self.avatarController.updateAvatar()
        
        self.scoreLabel.text = String(format: "%d", ScoreStorage.userTotalScore())
        
        self.resetLevels()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        GA.screenGameComplete()
        
        delayCall(1.5) { self.confetti() }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.confettiView.stop()
        
        soundsBoss.stopGameCompleteTheme()
    }
    
    private func confetti() {
        if Settings.isMusicEnabled() { soundsBoss.playLoopGameCompleteTheme() }
        self.confettiView.addConfettiInView(self.view)
    }
    
    private func screensAdoptation() {
        self.topLabel.font = UIFont(name: Constants.Font_OpenSans_ExtraBold, size: 22)
        self.scoreLabel.font = UIFont(name: Constants.Font_OpenSans_ExtraBold, size: 45)
        
        if is35Inch() {
            self.csLogoToTop.constant = 5
            self.bottomBottonToBottom.constant = 20
        }
    }
    
    private func resetLevels() {
        LevelsCompletionStorage.saveSolvedWords(0, level: 1)
        LevelsCompletionStorage.saveSolvedWords(0, level: 2)
        LevelsCompletionStorage.saveSolvedWords(0, level: 3)
        LevelsCompletionStorage.saveSolvedWords(0, level: 4)
        LevelsCompletionStorage.saveSolvedWords(0, level: 5)
        LevelsCompletionStorage.saveSolvedWords(0, level: 6)
        LevelsCompletionStorage.saveSolvedWords(0, level: 7)
        LevelsCompletionStorage.saveSolvedWords(0, level: 8)
        
        do {
            
            try DBHelper.db().inDatabase { db in
                let used = Int.fetchOne(db, "SELECT COUNT(*) FROM words WHERE ai_used_at IS NOT NULL")!
                let total = Int.fetchOne(db, "SELECT COUNT(*) FROM words")!
                
                if (Double(used)/Double(total)) >= 0.9 {
                    try db.execute("UPDATE words SET ai_used_at = NULL")
                }
            }
            
        } catch let e {
            log.error("ERROR: can not count used words in db \(e)")
        }
    }
    
    @IBAction func menuAction(sender: AnyObject?) {
        self.navigationController!.popViewControllerAnimated(true)
    }
}
