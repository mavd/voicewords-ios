//
//  LevelLoadingCtrl.swift
//  VoiceWords
//
//  Created by Georg Romas on 13/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

/// Level loading screen
class LevelLoadingCtrl: UIViewController {

    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var wordsLabel: UILabel!
    @IBOutlet weak var timePerWordLabel: UILabel!
    @IBOutlet weak var tipLabel: UILabel!
    @IBOutlet weak var loadingLabel: UILabel!
    
    @IBOutlet weak var chit_1: UIStackView!
    @IBOutlet weak var chit_2: UIStackView!
    @IBOutlet weak var chit_3: UIStackView!
    
    @IBOutlet weak var csLogoToTop: NSLayoutConstraint!
    
    var level = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.screensAdoptation()
        
        setup()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        animateLoadinLabel()
        self.animateChits()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        GA.screenLevelLoading()
    }
    
    private func screensAdoptation() {
        if is35Inch() {
            csLogoToTop.constant = 5
        }
    }

    private func setup() {
        loadingLabel.adjustsFontSizeToFitWidth = true
        
        levelLabel.text = "Уровень \(self.level)"
        wordsLabel.text = "\(GameSettings.wordsCountForLevel(self.level)) слов"
        
        self.timePerWordLabel.text = "\(GameSettings.gameIntervalForLevel(self.level)) сек на слово"
        
        if Settings.isMusicEnabled() {
            soundsBoss.playOnceStartRound()
        }
    }
    
    private func launchGame() {
        let vk = self.storyboard!.instantiateViewControllerWithIdentifier(Constants.ScreenGame) as! MainCtrl
        vk.level = self.level
        self.navigationController!.pushViewController(vk, animated: true)
        self.removeFromParentViewController()
    }
    
    private func scaleChit(chitView: UIView, complete: ()->Void) {
        let initialScale = CGFloat(1.0)
        let maxScale = CGFloat(GameSettings.chitAnimationMaxScale())
        let timeUp = Double(GameSettings.chitAnimationTimeUp())
        let timeDown = Double(GameSettings.chitAnimationTimeDown())
        
        chitView.transform = CGAffineTransformScale(CGAffineTransformIdentity, initialScale, initialScale)
        UIView.animateWithDuration(timeUp, delay: 0, options: UIViewAnimationOptions.CurveEaseOut,
            animations: {
                chitView.transform = CGAffineTransformScale(CGAffineTransformIdentity, maxScale, maxScale)
            },
            completion: {completeSuccess in
                
                UIView.animateWithDuration(timeDown, delay: 0, options: UIViewAnimationOptions.CurveEaseOut,
                    animations: {
                        chitView.transform = CGAffineTransformScale(CGAffineTransformIdentity, initialScale, initialScale)
                    },
                    completion: {completeSuccess in
                        complete()
                })
                
        })
    }
    
    private func animateChits() {
        self.scaleChit(self.chit_1) {
            self.scaleChit(self.chit_2) {
                self.scaleChit(self.chit_3) {
                    self.launchGame()
                }
            }
        }
        
    }
    
    private func animateLoadinLabel() {
        delayCall(0.5) {
            self.loadingLabel.text = ""
            delayCall(0.5) {
                self.loadingLabel.text = "."
                delayCall(0.5) {
                    self.loadingLabel.text = ".."
                    delayCall(0.5) {
                        self.loadingLabel.text = "..."
                        self.animateLoadinLabel()
                    }
                }
            }
        }
    }
}
