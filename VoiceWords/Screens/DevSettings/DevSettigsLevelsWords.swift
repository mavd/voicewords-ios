//
//  DevSettigsLevelsWords.swift
//  VoiceWords
//
//  Created by Georg Romas on 19/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

/**
 
 Developmetn settings screen for words count per level
 
 Properties starts with **l_** is UILabels
 
 Properties starts with **st_** is UISteppers
 
 Methods starts with **change_l** is UISteppers event handlers
 
 */
class DevSettigsLevelsWords: UITableViewController {
    
    @IBOutlet weak var l_seconds_1: UILabel!
    @IBOutlet weak var l_seconds_2: UILabel!
    @IBOutlet weak var l_seconds_3: UILabel!
    @IBOutlet weak var l_seconds_4: UILabel!
    @IBOutlet weak var l_seconds_5: UILabel!
    @IBOutlet weak var l_seconds_6: UILabel!
    @IBOutlet weak var l_seconds_7: UILabel!
    @IBOutlet weak var l_seconds_8: UILabel!
    
    @IBOutlet weak var st_1: UIStepper!
    @IBOutlet weak var st_2: UIStepper!
    @IBOutlet weak var st_3: UIStepper!
    @IBOutlet weak var st_4: UIStepper!
    @IBOutlet weak var st_5: UIStepper!
    @IBOutlet weak var st_6: UIStepper!
    @IBOutlet weak var st_7: UIStepper!
    @IBOutlet weak var st_8: UIStepper!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadSavedData()
    }
    
    private func loadSavedData() {
        
        self.st_1.value = Double(GameSettings.wordsCountForLevel(1))
        self.change_l_1(self.st_1)
        
        self.st_2.value = Double(GameSettings.wordsCountForLevel(2))
        self.change_l_2(self.st_2)
        
        self.st_3.value = Double(GameSettings.wordsCountForLevel(3))
        self.change_l_3(self.st_3)
        
        self.st_4.value = Double(GameSettings.wordsCountForLevel(4))
        self.change_l_4(self.st_4)
        
        self.st_5.value = Double(GameSettings.wordsCountForLevel(5))
        self.change_l_5(self.st_5)
        
        self.st_6.value = Double(GameSettings.wordsCountForLevel(6))
        self.change_l_6(self.st_6)
        
        self.st_7.value = Double(GameSettings.wordsCountForLevel(7))
        self.change_l_7(self.st_7)
        
        self.st_8.value = Double(GameSettings.wordsCountForLevel(8))
        self.change_l_8(self.st_8)
    }
    
    private func isMinimumLimit(value: Int) -> Bool {
        return value <= 1
    }
    
    @IBAction func change_l_1(sender: UIStepper) {
        let count = Int(sender.value)
        if isMinimumLimit(count) { return }
        l_seconds_1.text = String(format: "%03d", count)
        GameSettings.setWordsCountForLevel(1, wordsCount: count)
    }
    
    @IBAction func change_l_2(sender: UIStepper) {
        let count = Int(sender.value)
        if isMinimumLimit(count) { return }
        l_seconds_2.text = String(format: "%03d", count)
        GameSettings.setWordsCountForLevel(2, wordsCount: count)
    }
    
    @IBAction func change_l_3(sender: UIStepper) {
        let count = Int(sender.value)
        if isMinimumLimit(count) { return }
        l_seconds_3.text = String(format: "%03d", count)
        GameSettings.setWordsCountForLevel(3, wordsCount: count)
    }
    
    @IBAction func change_l_4(sender: UIStepper) {
        let count = Int(sender.value)
        if isMinimumLimit(count) { return }
        l_seconds_4.text = String(format: "%03d", count)
        GameSettings.setWordsCountForLevel(4, wordsCount: count)
    }
    
    @IBAction func change_l_5(sender: UIStepper) {
        let count = Int(sender.value)
        if isMinimumLimit(count) { return }
        l_seconds_5.text = String(format: "%03d", count)
        GameSettings.setWordsCountForLevel(5, wordsCount: count)
    }
    
    @IBAction func change_l_6(sender: UIStepper) {
        let count = Int(sender.value)
        if isMinimumLimit(count) { return }
        l_seconds_6.text = String(format: "%03d", count)
        GameSettings.setWordsCountForLevel(6, wordsCount: count)
    }
    
    @IBAction func change_l_7(sender: UIStepper) {
        let count = Int(sender.value)
        if isMinimumLimit(count) { return }
        l_seconds_7.text = String(format: "%03d", count)
        GameSettings.setWordsCountForLevel(7, wordsCount: count)
    }
    
    @IBAction func change_l_8(sender: UIStepper) {
        let count = Int(sender.value)
        if isMinimumLimit(count) { return }
        l_seconds_8.text = String(format: "%03d", count)
        GameSettings.setWordsCountForLevel(8, wordsCount: count)
    }
    
    
    @IBAction func closeAction(sender: AnyObject) {
        self.navigationController!.tabBarController!.dismissViewControllerAnimated(true, completion: nil)
    }
}
