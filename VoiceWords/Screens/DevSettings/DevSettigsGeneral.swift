//
//  DevSettigsGeneral.swift
//  VoiceWords
//
//  Created by Georg Romas on 19/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit
import GRDB
import XCGLogger

/// Developmetn settings screen
class DevSettigsGeneral: UITableViewController {
    
    @IBOutlet weak var chitCostLabel: UILabel!
    @IBOutlet weak var usedWordsLabel: UILabel!
    @IBOutlet weak var coinsCountLabel: UILabel!
    @IBOutlet weak var topScoreLabel: UILabel!
    @IBOutlet weak var coinsPerWordDonateLabel: UILabel!
    
    @IBOutlet weak var topScoreStepper: UIStepper!
    @IBOutlet weak var coinsStepper: UIStepper!
    @IBOutlet weak var coinsPerWordDonateStepper: UIStepper!
    
    @IBOutlet weak var pauseTimerOnRecognishinSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadSavedData()
    }
    
    private func loadSavedData() {
        self.topScoreStepper.value = Double(GameSettings.topScore())
        self.changeTopScoreAction(self.topScoreStepper)
        
        self.coinsStepper.value = Double(CoinsStorage.userCoins())
        self.changeCoinsAction(self.coinsStepper)
        
        self.coinsPerWordDonateStepper.value = Double(GameSettings.coinsPerWordDonation())
        self.changeCoinsPerWordAction(self.coinsPerWordDonateStepper)
        
        self.pauseTimerOnRecognishinSwitch.on = GameSettings.pauseTimerOnRecognition()
        
        self.updateUsedWordsMessage()
    }
    
    private func updateUsedWordsMessage() {
        do {
    
            var used = 0
            var total = 0
            
            try DBHelper.db().inDatabase { db in
                used = Int.fetchOne(db, "SELECT COUNT(*) FROM words WHERE ai_used_at IS NOT NULL")!
                total = Int.fetchOne(db, "SELECT COUNT(*) FROM words")!
            }
            
            self.usedWordsLabel.text = "\(used)/\(total)"
            
        } catch let e {
            log.error("ERROR: can not count used words in db \(e)")
            
            let error = "ERROR: can not count used words in db. contact developer."
            self.usedWordsLabel.text = error
        }
    }
    
    @IBAction func resetUsedWordsAction(sender: AnyObject) {
        do {
            
            try DBHelper.db().inDatabase { db in
                try db.execute("UPDATE words SET ai_used_at = NULL")
            }
            
            self.updateUsedWordsMessage()
            
        } catch let e {
            log.error("ERROR: can not count used words in db \(e)")
            
            let error = "ERROR: can not count reset used words in db. contact developer."
            self.usedWordsLabel.text = error
        }
    }

    @IBAction func changeCoinsAction(sender: UIStepper) {
        let value = Int(sender.value)
        CoinsStorage.setCoins(value)
        self.coinsCountLabel.text = String(format: "%04d", value)
    }
    
    @IBAction func changeTopScoreAction(sender: UIStepper) {
        let value = Int(sender.value)
        GameSettings.setTopScore(value)
        self.topScoreLabel.text = String(format: "%d", value)
    }
    
    @IBAction func pauseTimerOnRecognishinAction(sender: UISwitch!) {
        GameSettings.setPauseTimerOnRecognition(sender.on)
    }
    
    @IBAction func changeCoinsPerWordAction(sender: UIStepper) {
        let value = Int(sender.value)
        GameSettings.setCoinsPerWordDonation(value)
        self.coinsPerWordDonateLabel.text = String(format: "%d", value)
    }
    
    @IBAction func closeAction(sender: AnyObject) {
        self.navigationController!.tabBarController!.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func resetAllAction(sender: AnyObject) {
        self.resetUsedWordsAction(sender)
        
        ScoreStorage.saveUserLevel(1)
        
        LevelsCompletionStorage.saveLastNotCompleteLevel(1)
        LevelsCompletionStorage.saveSolvedWords(1, level: 0)
        LevelsCompletionStorage.saveSolvedWords(2, level: 0)
        LevelsCompletionStorage.saveSolvedWords(3, level: 0)
        LevelsCompletionStorage.saveSolvedWords(4, level: 0)
        LevelsCompletionStorage.saveSolvedWords(5, level: 0)
        LevelsCompletionStorage.saveSolvedWords(6, level: 0)
        LevelsCompletionStorage.saveSolvedWords(7, level: 0)
        LevelsCompletionStorage.saveSolvedWords(8, level: 0)
        
        CoinsStorage.setCoins(50)
        self.coinsStepper.value = Double(CoinsStorage.userCoins())
        self.changeCoinsAction(self.coinsStepper)
        
        Settings.setTutoralNotComplete()
        
        let alert = UIAlertController(title: "Готово", message: "Настройки сброшены", preferredStyle: UIAlertControllerStyle.Alert)
        let cancel = UIAlertAction(title: NSLocalizedString("AlertJustCloseBtn"), style: UIAlertActionStyle.Cancel, handler: { action in })
        alert.addAction(cancel)
        self.navigationController?.presentViewController(alert, animated: true, completion: nil)
    }
}
