//
//  DevSettigsVoice.swift
//  VoiceWords
//
//  Created by Georg Romas on 19/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

/// Developmetn settings screen for voice syntesizer/recognizer
class DevSettigsVoice: UITableViewController {

    @IBOutlet weak var swSiri: UISwitch!
    @IBOutlet weak var swYaErmil: UISwitch!
    @IBOutlet weak var swYaZahar: UISwitch!
    @IBOutlet weak var swYaJane: UISwitch!
    @IBOutlet weak var swYaAlyss: UISwitch!
    
    @IBOutlet var switches: [UISwitch]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        disableAll()
        
        applySaved()
    }
    
    private func applySaved() {
        swSiri.on = GameSettings.isVocalizerSiri()
        swYaErmil.on = GameSettings.isYandexVoiceErmil()
        swYaZahar.on = GameSettings.isYandexVoiceZahar()
        swYaJane.on = GameSettings.isYandexVoiceJane()
        swYaAlyss.on = GameSettings.isYandexVoiceAlyss()
    }
    
    private func disableAll() {
        for sw in switches {
            sw.on = false
        }
    }
    
    @IBAction func switchToSiri(sender: UISwitch) {
        disableAll()
        sender.on = true
        
        GameSettings.saveVoice(YSKVocalizerVoiceSiri)
    }
    
    @IBAction func switchToYandexErmil(sender: UISwitch) {
        disableAll()
        sender.on = true
        
        GameSettings.saveVoice(YSKVocalizerVoiceErmil)
    }
    
    @IBAction func switchToYandexZahar(sender: UISwitch) {
        disableAll()
        sender.on = true
        
        GameSettings.saveVoice(YSKVocalizerVoiceZahar)
    }
    
    @IBAction func switchToYandexJane(sender: UISwitch) {
        disableAll()
        sender.on = true
        
        GameSettings.saveVoice(YSKVocalizerVoiceJane)
    }
    
    @IBAction func switchToYandexAlyss(sender: UISwitch) {
        disableAll()
        sender.on = true
        
        GameSettings.saveVoice(YSKVocalizerVoiceAlyss)
    }
    
    
    
    @IBAction func closeAction(sender: AnyObject) {
        self.navigationController!.tabBarController!.dismissViewControllerAnimated(true, completion: nil)
    }
}
