//
//  DemoMaskGeneratorCtrl.swift
//  VoiceWords
//
//  Created by Georg Romas on 25/03/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

/// Demonstrate Apple Watchs task in development settings
class DemoMaskGeneratorCtrl: UIViewController {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var variantsLabel: UILabel!
    
    var currentQuestion: (String, [String])!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.variantsLabel.text = ""
    }

    @IBAction func generateAction(sender: AnyObject) {
        
        self.variantsLabel.text = ""
        
        let wg = WatchGame()
        
        self.currentQuestion = wg.selectNotUsedMask()
        self.label.text = self.currentQuestion.0
        
    }
    
    @IBAction func showAnswer(sender: AnyObject) {
        self.variantsLabel.text = self.currentQuestion.1.map {w in w.characters.map { ch in String(ch) }.joinWithSeparator(" ")}.joinWithSeparator("\n")
    }
}
