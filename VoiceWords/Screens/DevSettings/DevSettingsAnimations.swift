//
//  DevSettigsLevelsTime.swift
//  VoiceWords
//
//  Created by Georg Romas on 19/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit

/// Developmetn settings screen for animations
class DevSettingsAnimations: UITableViewController {
    
    @IBOutlet weak var l_chit_scale: UILabel!
    @IBOutlet weak var l_chit_time_up: UILabel!
    @IBOutlet weak var l_chit_time_down: UILabel!
    
    @IBOutlet weak var st_chit_scale: UIStepper!
    @IBOutlet weak var time_chit_time_up: UIStepper!
    @IBOutlet weak var st_chit_time_down: UIStepper!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadSavedData()
    }
    
    private func loadSavedData() {
        
        self.st_chit_scale.value = Double(GameSettings.chitAnimationMaxScale())
        self.changeChitScaleAction(self.st_chit_scale)
        
        self.time_chit_time_up.value = Double(GameSettings.chitAnimationTimeUp())
        self.changeChitTimeUpAction(self.time_chit_time_up)
        
        self.st_chit_time_down.value = Double(GameSettings.chitAnimationTimeDown())
        self.changeChitTimeDownAction(self.st_chit_time_down)

    }
    
    private func isMinimumLimit(value: Int) -> Bool {
        return value <= 1
    }
    
    @IBAction func changeChitScaleAction(sender: UIStepper) {
        let value = Float(sender.value)
        l_chit_scale.text = String(format: "%0.2f", value)
        GameSettings.setChitAnimationMaxScale(value)
    }
    
    @IBAction func changeChitTimeUpAction(sender: UIStepper) {
        let value = Float(sender.value)
        l_chit_time_up.text = String(format: "%0.2f", value)
        GameSettings.setChitAnimationTimeUp(value)
    }
    
    @IBAction func changeChitTimeDownAction(sender: UIStepper) {
        let value = Float(sender.value)
        l_chit_time_down.text = String(format: "%0.2f", value)
        GameSettings.setChitAnimationTimeDown(value)
    }
    
    
    @IBAction func closeAction(sender: AnyObject) {
        self.navigationController!.tabBarController!.dismissViewControllerAnimated(true, completion: nil)
    }
}
