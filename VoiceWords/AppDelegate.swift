//
//  AppDelegate.swift
//  VoiceWords
//
//  Created by Georg Romas on 08/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import UIKit
import StoreKit
import XCGLogger
import Appodeal
import WatchConnectivity
import SVProgressHUD
import Fabric
import Crashlytics
import AppsFlyer

/// Handy access XCGLogger singlton
let log = XCGLogger.defaultInstance()

/// **Global shortcut** for standart NSLocalizedString function without comment
///
/// Declared in AppDelegate.swift
public func NSLocalizedString(key: String) -> String {
    return NSLocalizedString(key, comment: "")
}

/**
 On launch setup external services like google Analytics and Ads providers, and configure default settings.
 
 Actualy we do:
 
   - Copy database into cashes directory if file not exist
   - Setup logging with XCGLogger
   - Setup Google Analytics
   - Setup Yandex Speech Kit
   - Setup Purchase helper
 */
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, WCSessionDelegate, SKPaymentTransactionObserver {

    
    /// Apple Watch communication session
    let session = WCSession.defaultSession()
    
    /// Application window
    var window: UIWindow?
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        Fabric.with([Crashlytics.self, Answers.self])
        
        AppsFlyerTracker.sharedTracker().appsFlyerDevKey = "XN3rURo75HCB5X8qMRZHAY"
        AppsFlyerTracker.sharedTracker().appleAppID = "1093583039"
        
        SKPaymentQueue.defaultQueue().addTransactionObserver(self)
        
        DBHelper.copyBundeledDbToCachesIfNotExist()
        
        let logFilePath = NSSearchPathForDirectoriesInDomains(.CachesDirectory, .UserDomainMask, true).first! + "/log"
        log.setup(.Debug, showThreadName: true, showLogLevel: true, showFileNames: true, showLineNumbers: true, writeToFile: logFilePath, fileLogLevel: .Debug)
        
        self.setupSpeechKit()
        self.initialSetup()
        
        let adTypes: AppodealAdType = [.RewardedVideo];
        Appodeal.initializeWithApiKey("53d45121b119c6a4315f499b36083d987cc6cba8b19cccf3", types: adTypes)
        
        session.delegate = self
        session.activateSession()
        
        // Try to preload purchases
        StoreHelper.sharedInstance.preLoadPurchasesInfo { products in
            MarketCtrl.cachePurchases(products)
        }
        
        return true
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        AppsFlyerTracker.sharedTracker().trackAppLaunch()
    }

    /**
     Setup Yandex Speech Kit with API key
     */
    func setupSpeechKit() {
        YSKSpeechKit.sharedInstance().configureWithAPIKey("8b94a540-4f94-40ea-a071-b6abb10a3644")
        // for debug
        //YSKSpeechKit.sharedInstance().setLogLevel(YSKLogLevel(YSKLogLevelDebug))
    }
    
    /**
     Setup default settings:
     - Words count for each level
     - Game timers for each level
     - Player gender
     - Vocalizer engine and voice
     - And other
     
     Method runs only once if saved settings version number is different
     */
    func initialSetup() {
        let settingsVersion = 10
        if !Settings.isInitialiSettingUp(settingsVersion) {
            Settings.setInitialSetupsComplete(settingsVersion)
            
            Settings.setGender(Settings.Gender.Male)
            Settings.allowShowAdForReward()
            
            Settings.setCurrentWordsVersion(0)
            
            CoinsStorage.refill(50)
            
            GameSettings.saveVoice(YSKVocalizerVoiceSiri)
            
            GameSettings.setGameIntervalForLevel(0, interval: 40)
            GameSettings.setGameIntervalForLevel(1, interval: 40)
            GameSettings.setGameIntervalForLevel(2, interval: 35)
            GameSettings.setGameIntervalForLevel(3, interval: 30)
            GameSettings.setGameIntervalForLevel(4, interval: 25)
            GameSettings.setGameIntervalForLevel(5, interval: 20)
            GameSettings.setGameIntervalForLevel(6, interval: 15)
            GameSettings.setGameIntervalForLevel(7, interval: 10)
            GameSettings.setGameIntervalForLevel(8, interval: 5)
            
            GameSettings.setWordsCountForLevel(0, wordsCount: 7)
            GameSettings.setWordsCountForLevel(1, wordsCount: 15)
            GameSettings.setWordsCountForLevel(2, wordsCount: 20)
            GameSettings.setWordsCountForLevel(3, wordsCount: 25)
            GameSettings.setWordsCountForLevel(4, wordsCount: 30)
            GameSettings.setWordsCountForLevel(5, wordsCount: 35)
            GameSettings.setWordsCountForLevel(6, wordsCount: 40)
            GameSettings.setWordsCountForLevel(7, wordsCount: 45)
            GameSettings.setWordsCountForLevel(8, wordsCount: 50)
            
            GameSettings.setChitCostInCoins(50, type: GameSettings.ChitID.Solve)
            GameSettings.setChitCostInCoins(35, type: GameSettings.ChitID.Change)
            GameSettings.setChitCostInCoins(25, type: GameSettings.ChitID.Time)
            GameSettings.setTopScore(180000)
            
            GameSettings.setPauseTimerOnRecognition(false)
            GameSettings.setCoinsPerWordDonation(5)
            
            GameSettings.setChitAnimationMaxScale(1.31)
            GameSettings.setChitAnimationTimeUp(0.15)
            GameSettings.setChitAnimationTimeDown(0.67)
        }
    }
    
    // MARK: WCSessionDelegate
    
    // MARK: iOS App State For Watch
    
    /// Called when any of the Watch state properties change
    /// 
    /// Used for debug information and session warm up
    /// @available(iOS 9.0, *)
    func sessionWatchStateDidChange(session: WCSession) {
        log.info("sessionWatchStateDidChange: \(session)")
        
        if session.reachable {
            session.sendMessage(["ping":"hello"], replyHandler: nil, errorHandler: { error in print(error) })
        }
    }
    
    // MARK: Interactive Messaging
    
    /// Called on the delegate of the receiver when the sender sends a message that expects a reply. Will be called on startup if the incoming message caused the receiver to launch.
    ///
    /// Delegate watch request to `WatchRequestHandler`
    /// @available(iOS 9.0, *)
    func session(session: WCSession, didReceiveMessage message: [String : AnyObject], replyHandler: ([String : AnyObject]) -> Void) {
        log.info("watch app request info: \(message)")
        WatchRequestHandler.performRequest(message, replyHandler: replyHandler)
        // for testing
        // replyHandler(["answer": "pong"])
    }
    
    // MARK: SKPaymentTransactionObserver
    func processTransactions(queue: SKPaymentQueue, transactions: [SKPaymentTransaction], isRestoreMode: Bool) {
        ui {
            SVProgressHUD.setDefaultMaskType(.Black)
            SVProgressHUD.setDefaultStyle(.Light)
            SVProgressHUD.showWithStatus("Подключение к магазину...")
            
            if isRestoreMode && transactions.count == 0 {
                //SVProgressHUD.showErrorWithStatus("Valid subscription not found")
                return
            }
            
            transactions.forEach { transaction in
                
                log.info("AppDelegate::paymentQueue transaction.error \(transaction.error)")
                
                switch transaction.transactionState {
                case .Purchased:
                    let purchaseItem = MarketCtrl.PurchaseItem.createFromStoreIdentifier(transaction.payment.productIdentifier)
                    let coins = purchaseItem.coins()
                    if coins > 0 {
                        CoinsStorage.refill(coins)
                    }
                    NSNotificationCenter.defaultCenter().postNotificationName(Constants.PurchaseCompleteEvent, object: nil)
                    SVProgressHUD.showSuccessWithStatus("Спасибо за покупку!")
                    queue.finishTransaction(transaction)
                    
                case .Failed:
                    SVProgressHUD.dismiss()
                    
                    guard let error = transaction.error else {
                        queue.finishTransaction(transaction)
                        SVProgressHUD.showErrorWithStatus("Произошла неизвестная ошибка при покупке. Повторите позже.")
                        return
                    }
                    
                    if error.code != SKErrorCode.PaymentCancelled.rawValue {
                        SVProgressHUD.showErrorWithStatus(error.localizedDescription)
                    }
                    queue.finishTransaction(transaction)
                    
                case .Deferred:
                    SVProgressHUD.showErrorWithStatus("Оплата ожидает подтверждения со стороны родителей. После подтверждения покупка будет доступна.")
                    
                case .Purchasing:
                    break
                    
                case .Restored:
                    SVProgressHUD.dismiss()
                    queue.finishTransaction(transaction)
                    break
                }
            }
        }
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(queue: SKPaymentQueue) {
        processTransactions(queue, transactions: queue.transactions, isRestoreMode: true)
    }
    
    func paymentQueue(queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: NSError) {
        SVProgressHUD.showWithStatus(error.localizedDescription)
    }
    
    func paymentQueue(queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        processTransactions(queue, transactions: transactions, isRestoreMode: true)
    }
}

