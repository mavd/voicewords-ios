//
//  ExtensionDelegate.swift
//  Watch Extension
//
//  Created by Georg Romas on 08/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import WatchKit
import WatchConnectivity

class ExtensionDelegate: NSObject, WKExtensionDelegate, WCSessionDelegate {
    
    let session = WCSession.defaultSession()
    
    func applicationDidFinishLaunching() {
        self.session.delegate = self
        self.session.activateSession()
    }

    func applicationDidBecomeActive() {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillResignActive() {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, etc.
    }
    
    //MARK: WCSessionDelegate
    
    func sessionReachabilityDidChange(session: WCSession) {
        print("Watch: sessionReachabilityDidChange")
        
        if session.reachable {
            print("Watch: session established")
            
            WCSession.defaultSession().sendMessage(["request":"userInfo"],
                replyHandler: {answer in
                    print("watch: request user info:", answer)
                },
                errorHandler: {error in})
        }
    }

}
