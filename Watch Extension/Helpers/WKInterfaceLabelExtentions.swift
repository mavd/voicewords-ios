//
//  WKInterfaceLabelExtentions.swift
//  VoiceWords
//
//  Created by Georg Romas on 10/03/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import WatchKit

extension WKInterfaceLabel {
    func changeNumberAnimated(from: Int, to: Int, steps: Int = 5, format: String = "%05d", interval: NSTimeInterval = 0.05) {
        var current = from
        let step = (to-from)/steps
        func isComplete() -> Bool {
            if (from > to) { return current > to }
            if (to > from) { return current < to }
            return false
        }
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0)) {
            while isComplete() {
                current += step
                if (!isComplete()) {current = to}
                ui{ self.setText(String(format: format, current)) }
                NSThread.sleepForTimeInterval(interval)
            }
            if (current != to) {current = to}
            ui{ self.setText(String.localizedStringWithFormat(format, current)) }

        }
    }
}