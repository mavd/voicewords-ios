//
//  ActivityIndicator.swift
//  VoiceWords
//
//  Created by Georg Romas on 05/03/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import WatchKit

extension WKInterfaceImage {
    
    func show() {
        self.setImageNamed("Activity")
        self.startAnimatingWithImagesInRange(NSMakeRange(0, 15), duration: 1.0, repeatCount: 0)
        self.setHidden(false)
    }
    
    func hide() {
        self.stopAnimating()
        self.setHidden(true)
    }
    
}
