//
//  Settings.swift
//  VoiceWords
//
//  Created by Georg Romas on 24/03/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import Foundation


class Storage {
    
    static let kLastPlayedTimeKey = "kLastPlayedTimeKey"
    
    static let kLastUserLevelKey = "kLastUserLevelKey"
    static let kLastUserCoinsKey = "kLastUserCoinsKey"
    static let kLastUserScoreKey = "kLastUserScoreKey"
    static let kLastUserAvatarNameKey = "kLastUserAvatarNameKey"
    static let kLastUserStatusNameKey = "kLastUserStatusNameKey"
    static let kGameIsActiveKey = "kGameIsActiveKey"
    
    static let kCurrentMaskKey = "kCurrentMaskKey"
    static let kCurrentAnswerVariantsKey = "kCurrentAnswerVariantsKey"
    
    //MARK: last played time
    class func lastPlayedTime() -> NSDate {
        let defaults = NSUserDefaults.standardUserDefaults()
        let interval = defaults.doubleForKey(kLastPlayedTimeKey)
        return NSDate(timeIntervalSince1970: interval)
    }
    
    class func saveLastPlayedTimeNow() {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setDouble(NSDate().timeIntervalSince1970, forKey: kLastPlayedTimeKey)
        defaults.synchronize()
    }
    
    //MARK: last known level
    class func lastKnownUserInfo() -> (userLevel: Int, coins: Int, score: Int,  avatarName: String, statusName: String) {
        let defaults = NSUserDefaults.standardUserDefaults()
        guard
            let avatarName = defaults.stringForKey(kLastUserAvatarNameKey),
            let statusName = defaults.stringForKey(kLastUserStatusNameKey)
            else {
                return (1, 0, 0, "w_img_status_male_user1", "Новичек")
        }
        
        var level = defaults.integerForKey(kLastUserLevelKey)
        if level == 0 {
            level = 1
        }
        let score = defaults.integerForKey(kLastUserScoreKey)
        let coins = defaults.integerForKey(kLastUserCoinsKey)
        
        return (level, coins, score, avatarName, statusName)
    }
    
    class func saveCurrentUserInfo(userLevel: Int, coins: Int, score: Int,  avatarName: String, statusName: String) {
        let defaults = NSUserDefaults.standardUserDefaults()
        
        defaults.setInteger(userLevel, forKey: kLastUserLevelKey)
        defaults.setInteger(coins, forKey: kLastUserCoinsKey)
        defaults.setInteger(score, forKey: kLastUserScoreKey)
        defaults.setObject(avatarName, forKey: kLastUserAvatarNameKey)
        defaults.setObject(statusName, forKey: kLastUserStatusNameKey)
        
        defaults.synchronize()
    }
    
    //MARK: current game info
    class func gameIsActive() -> Bool {
        let defaults = NSUserDefaults.standardUserDefaults()
        return defaults.boolForKey(kGameIsActiveKey)
    }
    
    class func markGameAsActive() {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setBool(true, forKey: kGameIsActiveKey)
        defaults.synchronize()
    }
    
    class func markGameAsInActive() {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setBool(false, forKey: kGameIsActiveKey)
        defaults.synchronize()
    }
    
    class func currentMask() -> String? {
        let defaults = NSUserDefaults.standardUserDefaults()
        return defaults.stringForKey(kCurrentMaskKey)
    }
    
    class func currentAnswerVariants() -> [String]? {
        let defaults = NSUserDefaults.standardUserDefaults()
        return defaults.arrayForKey(kCurrentAnswerVariantsKey) as? [String]
    }
    
    class func saveCurrentWord(mask: String?, answerVariants: [String]?) {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(mask, forKey: kCurrentMaskKey)
        defaults.setObject(answerVariants, forKey: kCurrentAnswerVariantsKey)
        defaults.synchronize()
    }
    
}