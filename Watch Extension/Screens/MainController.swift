//
//  MainController.swift
//  VoiceWords
//
//  Created by Georg Romas on 24/03/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class MainController: WKInterfaceController, WCSessionDelegate {

    @IBOutlet var menuGroup: WKInterfaceButton!
    @IBOutlet var gameGroup: WKInterfaceGroup!
    
    @IBOutlet var brainIndicatorGroup: WKInterfaceGroup!
    @IBOutlet var userScoreLabel: WKInterfaceLabel!
    @IBOutlet var avatarIndicatorGroup: WKInterfaceGroup!
    @IBOutlet var userStatusLabel: WKInterfaceLabel!
    @IBOutlet var userCoinsLabel: WKInterfaceLabel!
    @IBOutlet var menuLabel: WKInterfaceLabel!
    @IBOutlet var buttonsGroup: WKInterfaceGroup!
    
    @IBOutlet var warningGroup: WKInterfaceGroup!
    @IBOutlet var warningLabel: WKInterfaceLabel!
    @IBOutlet var titleLabel: WKInterfaceLabel!
    
    @IBOutlet var wordLabel: WKInterfaceLabel!
    @IBOutlet var scoreRewardLabel: WKInterfaceLabel!
    @IBOutlet var coinsRewardLevel: WKInterfaceLabel!
    
    let session = WCSession.defaultSession()
    
    let intervalBetweenGames = 100
    
    //var currentMask: String = "_ _ _ _"
    
    override init() {
        super.init()
        
        if WCSession.isSupported() {
            self.session.delegate = self
            self.session.activateSession()
        }
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        setTitle("Словолюб")
        
        self.updateGameStatus()
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        self.updateGameStatus()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    private func updateGameStatus() {
        self.hideWarning()
        
        self.updateUserInfo()
        
        self.checkRemainingTime()
    }
    
    //MARK: menu ------------- >
    
    private func hideMenuGroup() {
        self.menuGroup.setHidden(true)
        self.gameGroup.setHidden(false)
    }
    
    private func showMenuGroup() {
        self.menuGroup.setHidden(false)
        self.gameGroup.setHidden(true)
    }
    
    var canPlayBlink = true;
    private func blinkMenuLabelStop() {
        self.canPlayBlink = false
    }
    
    private func blinkMenuLabel() {
        self.menuLabel.setText("Играть в бонус игру")
        var hidden = false
        func blink() {
            hidden = !hidden
            self.menuLabel.setHidden(hidden)
            
            if self.canPlayBlink {
                delayCall(0.3) {
                    blink()
                }
            } else {
                self.menuLabel.setHidden(false)
                self.canPlayBlink = true
            }
        }
        blink()
    }
    
    private func showKnownUserStatus() {
        //(userLevel: Int, coins: Int, score: Int,  avatarName: String, statusName: String)
        let lastKnownUserInfo = Storage.lastKnownUserInfo()
        
        self.avatarIndicatorGroup.setBackgroundImage(UIImage(named: lastKnownUserInfo.3))
        self.userStatusLabel.setText(lastKnownUserInfo.4)
        self.userCoinsLabel.setText("\(lastKnownUserInfo.1)")
        self.userScoreLabel.setText("\(lastKnownUserInfo.2)")
        
    }
    
    private func remainingTimeInMinutes() -> Int {
        let remainTime = Storage.lastPlayedTime().dateByAddingTimeInterval(Double(intervalBetweenGames) * 60)
        let intervalInMinutes = Int(remainTime.timeIntervalSinceNow/60.0)
        
        return intervalInMinutes
    }
    
    private func checkRemainingTime() {
        let intervalInMinutes = self.remainingTimeInMinutes()

        if intervalInMinutes <= 0 {
            
            self.blinkMenuLabelStop()
            
            if Storage.gameIsActive() {
                self.setupGame()
                self.hideMenuGroup()
            } else {
                self.showMenuGroup()
                
                if Storage.currentMask() == nil {
                    self.menuLabel.setText("Подключение к iPhone...\nпожалуйста подождите")
                    self.menuLabel.setHidden(false)
                    
                    self.loadCurrrentGameInfo() {
                        self.checkRemainingTime()
                    }
                } else {
                    self.canPlayBlink = true
                    self.blinkMenuLabel()
                }
                
            }
            
        } else {
            self.showKnownUserStatus()
            self.showMenuGroup()
            self.blinkMenuLabelStop()
            self.playRemainingTimeAnimation(intervalInMinutes)
        }
    }
    
    private func playRemainingTimeAnimation(remainTime: Int) {
        let steps = intervalBetweenGames - remainTime
        let interval = 2.0/Double(steps)
        var remainingTimeFormat = "Бонус игра доступна\nчерез %02d "
        if remainTime >= 5 {
            remainingTimeFormat += "минут"
        } else if remainTime != 1 {
            remainingTimeFormat += "минуты"
        } else {
            remainingTimeFormat += "минуту"
        }
        
        self.menuLabel.changeNumberAnimated(intervalBetweenGames, to: remainTime, steps: steps, format: remainingTimeFormat, interval: interval)
    }
    
    @IBAction func startGameAction() {
        if self.remainingTimeInMinutes() <= 0 && Storage.currentMask() != nil {
            self.setupGame()
            self.blinkMenuLabelStop()
            self.hideMenuGroup()
            Storage.markGameAsActive()
        }
    }
    
    private func processUserInfoResponse(answer: [String:AnyObject]) {
        if
            let statusName = answer[WatchRequestConstants.kResponseKeyUserStatus] as? String,
            let coinsAmount = answer[WatchRequestConstants.kResponseKeyUserCoins] as? Int,
            let score = answer[WatchRequestConstants.kResponseKeyUserScore] as? Int,
            let avatarName = answer[WatchRequestConstants.kResponseKeyUserAvatarName] as? String,
            let userLevel = answer[WatchRequestConstants.kResponseKeyUserLevel] as? Int
        {
            Storage.saveCurrentUserInfo(userLevel, coins: coinsAmount, score: score, avatarName: avatarName, statusName: statusName)
        }
    }
    
    var connectionErrorsCount_1 = 0
    func updateUserInfo() {
        self.showKnownUserStatus()
        
        if session.reachable {
            self.session.sendMessage([WatchRequestConstants.kRequestKey: WatchRequestConstants.kRequestUserInfo],
                replyHandler: {answer in
                    self.processUserInfoResponse(answer)
                    
                    self.showKnownUserStatus()
                    print("watch: MainController: updateUserInfo complete")
                },
                errorHandler: {error in
                    print("watch: MainController: send message error: ", error)
                    self.connectionErrorsCount_1 += 1
                    if self.connectionErrorsCount_1 < 5 {
                        self.updateUserInfo()
                    } else {
                        //TODO: notify user about connection issue
                    }
            })
        } else {
            print("watch: MainController: session is not reachable")
            self.connectionErrorsCount_1 += 1
            if self.connectionErrorsCount_1 < 5 {
                self.updateUserInfo()
            } else {
                //TODO: notify user about connection issue
            }
        }
    }
    
    //MARK: game ------------- >
    
    private func setupGame() {
        if let mask = Storage.currentMask() {
            self.wordLabel.setText(mask.uppercaseString)
        }
        self.buttonsGroup.setHidden(false)
    }
    
    func showWarning(message: String, autoHideInterval:Double = -1.0) {
        self.warningLabel.setText(message)
        self.warningGroup.setHidden(false)
        self.titleLabel.setHidden(true)
        
        if autoHideInterval >= 0 {
            delayCall(autoHideInterval) {
                self.hideWarning()
            }
        }
    }
    
    func hideWarning() {
        self.warningGroup.setHidden(true)
        self.titleLabel.setHidden(false)
    }
    
    var connectionErrorsCount_2 = 0
    private func loadCurrrentGameInfo(complete: ()->Void) {
        if session.reachable {
            self.session.sendMessage([WatchRequestConstants.kRequestKey: WatchRequestConstants.kRequestGame],
                replyHandler: {answer in
                    if
                        let word = answer[WatchRequestConstants.kResponseKeyGameWord] as? String,
                        let answerVariants = answer[WatchRequestConstants.kResponseKeyGameVariants] as? [String]
                    {
                        Storage.saveCurrentWord(word, answerVariants: answerVariants)
                        
                        self.setupGame()
                        complete()
                        print("watch: MainController: loadCurrrentGameInfo complete")
                    } else {
                        print("watch: MainController: loadCurrrentGameInfo game info is wrong")
                    }
                },
                errorHandler: {error in
                    print("watch: MainController: send message error: ", error)
                    self.connectionErrorsCount_1 += 1
                    if self.connectionErrorsCount_1 < 5 {
                        self.loadCurrrentGameInfo(complete)
                    } else {
                        //TODO: notify user about connection issue
                    }
            })
        } else {
            print("watch: MainController: session is not reachable")
            self.connectionErrorsCount_1 += 1
            if self.connectionErrorsCount_1 < 5 {
                self.loadCurrrentGameInfo(complete)
            } else {
                //TODO: notify user about connection issue
            }
        }
    }
    
    private func processPlayerInput(word: String) {
        guard let variants = Storage.currentAnswerVariants() else {
            self.showWarning("Не верный ответ")
            return
        }
        
        if variants.contains(word.lowercaseString) {
            self.buttonsGroup.setHidden(true)
            
            //(userLevel: Int, coins: Int, score: Int,  avatarName: String, statusName: String)
            let lastKnownUserInfo = Storage.lastKnownUserInfo()
            
            let rewardCoins = 50
            let rewardScore = 10000
            
            let currentCoins = lastKnownUserInfo.1
            let currentScore = lastKnownUserInfo.2
            let newCoins = currentCoins + rewardCoins
            let newScore = currentScore + rewardScore
            
            self.saveNewScore(word.lowercaseString, mask: Storage.currentMask()!, newCoins: rewardCoins, newScore: rewardScore)
            
            let scoreInfo = CompleteCtrl.ScoreInfo(
                scoreFrom: currentScore,
                coinsFrom: currentCoins,
                scoreTo: newScore,
                coinsTo: newCoins
            )
            
            let formattedWord = word.characters.map { ch in String(ch) }.joinWithSeparator(" ").uppercaseString
            self.wordLabel.setText(formattedWord)
            
            Storage.saveCurrentWord(nil, answerVariants: nil)
            Storage.saveLastPlayedTimeNow()
            Storage.markGameAsInActive()
            
            delayCall(1.4) {
                self.presentControllerWithName("CompleteScrene", context: scoreInfo)
            }
            
        } else {
            self.showWarning("Не верный ответ", autoHideInterval: 2)
        }
    }
    
    func userWantToCancelGame() {
        Storage.saveCurrentWord(nil, answerVariants: nil)
        Storage.saveLastPlayedTimeNow()
        Storage.markGameAsInActive()
    }
    
    var connectionErrorsCount_3 = 0
    private func saveNewScore(word: String, mask: String, newCoins: Int, newScore: Int) {
        let lastKnownUserInfo = Storage.lastKnownUserInfo()
        Storage.saveCurrentUserInfo(lastKnownUserInfo.0, coins: newCoins, score: newScore, avatarName: lastKnownUserInfo.3, statusName: lastKnownUserInfo.4)
        
        let message: [String: AnyObject] = [
            WatchRequestConstants.kRequestKey: WatchRequestConstants.kRequestLevelComplete,
            WatchRequestConstants.kRequestKeyRewardScore: newScore,
            WatchRequestConstants.kRequestKeyRewardCoins: newCoins,
            WatchRequestConstants.kRequestKeySelectedWord: word,
            WatchRequestConstants.kRequestKeyPlayedMask: mask
        ]
        self.session.sendMessage(message,
            replyHandler: { answer in
                self.processUserInfoResponse(answer)
            },
            errorHandler: { error in
                self.connectionErrorsCount_3 += 1
                if self.connectionErrorsCount_3 < 3 {
                    self.saveNewScore(word, mask: mask, newCoins:newCoins, newScore: newScore)
                }
        })
    }
    
    @IBAction func playerAnswerAction() {
        //TODO: next line is for testing
//        self.processPlayerInput(Storage.currentAnswerVariants()![0])
        
        self.presentTextInputControllerWithSuggestions([], allowedInputMode: WKTextInputMode.Plain) { result in
            print("watch: dictaion result: ", result)
            if let words = result as? [String] {
                if words.count > 0 {
                    let word = words[0]
                    self.processPlayerInput(word)
                }
            }
        }
    }
    
    @IBAction func cancelGameAction() {
        presentControllerWithName("Modal", context: self)
    }

    
    //MARK: WCSessionDelegate
    
    func sessionReachabilityDidChange(session: WCSession) {
        print("watch: MainController: sessionReachabilityDidChange")
    }
    
}
