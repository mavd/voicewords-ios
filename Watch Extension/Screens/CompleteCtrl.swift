//
//  CompleteCtrl.swift
//  VoiceWords
//
//  Created by Georg Romas on 20/02/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import WatchKit
import Foundation

class CompleteCtrl: WKInterfaceController {
    
    class ScoreInfo {
        let scoreFrom: Int!
        let coinsFrom: Int!
        let scoreTo: Int!
        let coinsTo: Int!
        
        init(scoreFrom: Int, coinsFrom: Int, scoreTo: Int, coinsTo: Int!) {
            self.scoreFrom = scoreFrom
            self.coinsFrom = coinsFrom
            self.scoreTo = scoreTo
            self.coinsTo = coinsTo
        }
    }

    @IBOutlet var avatarImage: WKInterfaceImage!    
    @IBOutlet var scoreLevel: WKInterfaceLabel!
    @IBOutlet var coinsLabel: WKInterfaceLabel!
    @IBOutlet var userStatusLabel: WKInterfaceLabel!
    @IBOutlet var subtitleLabel: WKInterfaceLabel!
    @IBOutlet var messageLabel: WKInterfaceLabel!
    
    var scoreInfo: ScoreInfo!
    
    override func awakeWithContext(context: AnyObject?) {
        print("watch: CompleteCtrl: awakeWithContext")
        super.awakeWithContext(context)
        
        self.scoreInfo = context as! ScoreInfo
        
        self.setTitle("     Поздравляем!")
        
        self.updateUserLevelIndicator()
    }

    override func willActivate() {
        print("watch: CompleteCtrl: willActivate")
        super.willActivate()
        
        self.calculateScoreAnimated()
    }

    override func didDeactivate() {
        print("watch: CompleteCtrl: didDeactivate")
        super.didDeactivate()
        
        self.dismissController()
    }
    
    private func updateUserLevelIndicator() {
        //(userLevel: Int, coins: Int, score: Int,  avatarName: String, statusName: String)
        let userInfo = Storage.lastKnownUserInfo()
        self.avatarImage.setImage(UIImage(named: userInfo.3))
        self.userStatusLabel.setText(userInfo.4)
        
        self.messageLabel.setAlpha(0)
        self.subtitleLabel.setText("Награда")
        self.scoreLevel.setText("+10000")
        self.coinsLabel.setText("+50")
    }
    
    private func calculateScoreAnimated() {
        delayCall(1.4) {
            self.subtitleLabel.setText("Итого сейчас")
            self.scoreLevel.changeNumberAnimated(self.scoreInfo.scoreFrom, to: self.scoreInfo.scoreTo, steps: 10, format: "%d", interval: 0.05)
            self.coinsLabel.changeNumberAnimated(self.scoreInfo.coinsFrom, to: self.scoreInfo.coinsTo, steps: 10, format: "%d", interval: 0.05)
        }
        
        delayCall(2.4) {
            self.animateWithDuration(0.4) { self.messageLabel.setAlpha(1) }
        }
    }

}
