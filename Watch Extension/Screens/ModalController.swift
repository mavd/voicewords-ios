//
//  ModalController.swift
//  VoiceWords
//
//  Created by Georg Romas on 25/03/16.
//  Copyright © 2016 VoiceWords. All rights reserved.
//

import WatchKit
import Foundation


class ModalController: WKInterfaceController {
    
    @IBOutlet var alertGroup: WKInterfaceGroup!
    @IBOutlet var messageGroup: WKInterfaceGroup!
    
    @IBOutlet var avatarIndicatorGroup: WKInterfaceGroup!
    @IBOutlet var userStatusLabel: WKInterfaceLabel!
    
    var mainController: MainController!

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        self.mainController = context as! MainController
        
        self.setTitle(" ")
        
        self.alertGroup.setHidden(false)
        self.messageGroup.setHidden(true)
        setupStatus()
    }

    func setupStatus() {
        //(userLevel: Int, coins: Int, score: Int,  avatarName: String, statusName: String)
        let userInfo = Storage.lastKnownUserInfo()
        avatarIndicatorGroup.setBackgroundImage(UIImage(named: userInfo.3))
        userStatusLabel.setText(userInfo.4)
    }

    @IBAction func confirmAction() {
        self.mainController.userWantToCancelGame()
        self.alertGroup.setHidden(true)
        self.messageGroup.setHidden(false)
        
        delayCall(5.0) { self.dismissController() }
    }
    
    @IBAction func rejectAction() {
        self.dismissController()
    }
}
